﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(7);
			InitRadcheckEntityMappings();
			InitRadgroupcheckEntityMappings();
			InitRadgroupreplyEntityMappings();
			InitRadippoolEntityMappings();
			InitRadpostauthEntityMappings();
			InitRadreplyEntityMappings();
			InitUsergroupEntityMappings();
		}

		/// <summary>Inits RadcheckEntity's mappings</summary>
		private void InitRadcheckEntityMappings()
		{
			this.AddElementMapping("RadcheckEntity", @"radius_wtb", @"Default", "radcheck", 5, 0);
			this.AddElementFieldMapping("RadcheckEntity", "Attribute", "attribute", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadcheckEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 1);
			this.AddElementFieldMapping("RadcheckEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RadcheckEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadcheckEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits RadgroupcheckEntity's mappings</summary>
		private void InitRadgroupcheckEntityMappings()
		{
			this.AddElementMapping("RadgroupcheckEntity", @"radius_wtb", @"Default", "radgroupcheck", 5, 0);
			this.AddElementFieldMapping("RadgroupcheckEntity", "Attribute", "attribute", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadgroupcheckEntity", "Groupname", "groupname", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RadgroupcheckEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 2);
			this.AddElementFieldMapping("RadgroupcheckEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadgroupcheckEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits RadgroupreplyEntity's mappings</summary>
		private void InitRadgroupreplyEntityMappings()
		{
			this.AddElementMapping("RadgroupreplyEntity", @"radius_wtb", @"Default", "radgroupreply", 5, 0);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Attribute", "attribute", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Groupname", "groupname", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 2);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits RadippoolEntity's mappings</summary>
		private void InitRadippoolEntityMappings()
		{
			this.AddElementMapping("RadippoolEntity", @"radius_wtb", @"Default", "radippool", 9, 0);
			this.AddElementFieldMapping("RadippoolEntity", "Calledstationid", "calledstationid", false, "VarChar", 30, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadippoolEntity", "Callingstationid", "callingstationid", false, "VarChar", 30, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RadippoolEntity", "ExpiryTime", "expiry_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("RadippoolEntity", "Framedipaddress", "framedipaddress", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadippoolEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 4);
			this.AddElementFieldMapping("RadippoolEntity", "Nasipaddress", "nasipaddress", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("RadippoolEntity", "PoolKey", "pool_key", false, "VarChar", 30, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("RadippoolEntity", "PoolName", "pool_name", false, "VarChar", 30, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("RadippoolEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 8);
		}

		/// <summary>Inits RadpostauthEntity's mappings</summary>
		private void InitRadpostauthEntityMappings()
		{
			this.AddElementMapping("RadpostauthEntity", @"radius_wtb", @"Default", "radpostauth", 5, 0);
			this.AddElementFieldMapping("RadpostauthEntity", "Authdate", "authdate", false, "TimeStamp", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("RadpostauthEntity", "Id", "id", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RadpostauthEntity", "Pass", "pass", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RadpostauthEntity", "Reply", "reply", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadpostauthEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits RadreplyEntity's mappings</summary>
		private void InitRadreplyEntityMappings()
		{
			this.AddElementMapping("RadreplyEntity", @"radius_wtb", @"Default", "radreply", 5, 0);
			this.AddElementFieldMapping("RadreplyEntity", "Attribute", "attribute", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadreplyEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 1);
			this.AddElementFieldMapping("RadreplyEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RadreplyEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadreplyEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits UsergroupEntity's mappings</summary>
		private void InitUsergroupEntityMappings()
		{
			this.AddElementMapping("UsergroupEntity", @"radius_wtb", @"Default", "usergroup", 3, 0);
			this.AddElementFieldMapping("UsergroupEntity", "Groupname", "groupname", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("UsergroupEntity", "Priority", "priority", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UsergroupEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 2);
		}

	}
}
