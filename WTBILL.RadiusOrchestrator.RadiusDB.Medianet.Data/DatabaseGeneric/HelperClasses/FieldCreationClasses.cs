﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data;

namespace WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity RadcheckEntity</summary>
	public partial class RadcheckFields
	{
		/// <summary>Creates a new RadcheckEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadcheckEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Id);}
		}
		/// <summary>Creates a new RadcheckEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Op);}
		}
		/// <summary>Creates a new RadcheckEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Username);}
		}
		/// <summary>Creates a new RadcheckEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity RadgroupcheckEntity</summary>
	public partial class RadgroupcheckFields
	{
		/// <summary>Creates a new RadgroupcheckEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupcheckFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadgroupcheckEntity.Groupname field instance</summary>
		public static EntityField2 Groupname
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupcheckFieldIndex.Groupname);}
		}
		/// <summary>Creates a new RadgroupcheckEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupcheckFieldIndex.Id);}
		}
		/// <summary>Creates a new RadgroupcheckEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupcheckFieldIndex.Op);}
		}
		/// <summary>Creates a new RadgroupcheckEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupcheckFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity RadgroupreplyEntity</summary>
	public partial class RadgroupreplyFields
	{
		/// <summary>Creates a new RadgroupreplyEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Groupname field instance</summary>
		public static EntityField2 Groupname
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Groupname);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Id);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Op);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity RadippoolEntity</summary>
	public partial class RadippoolFields
	{
		/// <summary>Creates a new RadippoolEntity.Calledstationid field instance</summary>
		public static EntityField2 Calledstationid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.Calledstationid);}
		}
		/// <summary>Creates a new RadippoolEntity.Callingstationid field instance</summary>
		public static EntityField2 Callingstationid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.Callingstationid);}
		}
		/// <summary>Creates a new RadippoolEntity.ExpiryTime field instance</summary>
		public static EntityField2 ExpiryTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.ExpiryTime);}
		}
		/// <summary>Creates a new RadippoolEntity.Framedipaddress field instance</summary>
		public static EntityField2 Framedipaddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.Framedipaddress);}
		}
		/// <summary>Creates a new RadippoolEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.Id);}
		}
		/// <summary>Creates a new RadippoolEntity.Nasipaddress field instance</summary>
		public static EntityField2 Nasipaddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.Nasipaddress);}
		}
		/// <summary>Creates a new RadippoolEntity.PoolKey field instance</summary>
		public static EntityField2 PoolKey
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.PoolKey);}
		}
		/// <summary>Creates a new RadippoolEntity.PoolName field instance</summary>
		public static EntityField2 PoolName
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.PoolName);}
		}
		/// <summary>Creates a new RadippoolEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadippoolFieldIndex.Username);}
		}
	}

	/// <summary>Field Creation Class for entity RadpostauthEntity</summary>
	public partial class RadpostauthFields
	{
		/// <summary>Creates a new RadpostauthEntity.Authdate field instance</summary>
		public static EntityField2 Authdate
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Authdate);}
		}
		/// <summary>Creates a new RadpostauthEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Id);}
		}
		/// <summary>Creates a new RadpostauthEntity.Pass field instance</summary>
		public static EntityField2 Pass
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Pass);}
		}
		/// <summary>Creates a new RadpostauthEntity.Reply field instance</summary>
		public static EntityField2 Reply
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Reply);}
		}
		/// <summary>Creates a new RadpostauthEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Username);}
		}
	}

	/// <summary>Field Creation Class for entity RadreplyEntity</summary>
	public partial class RadreplyFields
	{
		/// <summary>Creates a new RadreplyEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadreplyEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Id);}
		}
		/// <summary>Creates a new RadreplyEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Op);}
		}
		/// <summary>Creates a new RadreplyEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Username);}
		}
		/// <summary>Creates a new RadreplyEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity UsergroupEntity</summary>
	public partial class UsergroupFields
	{
		/// <summary>Creates a new UsergroupEntity.Groupname field instance</summary>
		public static EntityField2 Groupname
		{
			get { return (EntityField2)EntityFieldFactory.Create(UsergroupFieldIndex.Groupname);}
		}
		/// <summary>Creates a new UsergroupEntity.Priority field instance</summary>
		public static EntityField2 Priority
		{
			get { return (EntityField2)EntityFieldFactory.Create(UsergroupFieldIndex.Priority);}
		}
		/// <summary>Creates a new UsergroupEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(UsergroupFieldIndex.Username);}
		}
	}
	

}