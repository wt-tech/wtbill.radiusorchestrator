﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (7 + 0));
			InitRadcheckEntityInfos();
			InitRadgroupcheckEntityInfos();
			InitRadgroupreplyEntityInfos();
			InitRadippoolEntityInfos();
			InitRadpostauthEntityInfos();
			InitRadreplyEntityInfos();
			InitUsergroupEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits RadcheckEntity's FieldInfo objects</summary>
		private void InitRadcheckEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadcheckFieldIndex), "RadcheckEntity");
			this.AddElementFieldInfo("RadcheckEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Attribute, 32, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadcheckFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadcheckEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Username, 64, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Value, 253, 0, 0);
		}
		/// <summary>Inits RadgroupcheckEntity's FieldInfo objects</summary>
		private void InitRadgroupcheckEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadgroupcheckFieldIndex), "RadgroupcheckEntity");
			this.AddElementFieldInfo("RadgroupcheckEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadgroupcheckFieldIndex.Attribute, 32, 0, 0);
			this.AddElementFieldInfo("RadgroupcheckEntity", "Groupname", typeof(System.String), false, false, false, false,  (int)RadgroupcheckFieldIndex.Groupname, 64, 0, 0);
			this.AddElementFieldInfo("RadgroupcheckEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadgroupcheckFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadgroupcheckEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadgroupcheckFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadgroupcheckEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadgroupcheckFieldIndex.Value, 253, 0, 0);
		}
		/// <summary>Inits RadgroupreplyEntity's FieldInfo objects</summary>
		private void InitRadgroupreplyEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadgroupreplyFieldIndex), "RadgroupreplyEntity");
			this.AddElementFieldInfo("RadgroupreplyEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Attribute, 32, 0, 0);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Groupname", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Groupname, 64, 0, 0);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadgroupreplyFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Value, 253, 0, 0);
		}
		/// <summary>Inits RadippoolEntity's FieldInfo objects</summary>
		private void InitRadippoolEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadippoolFieldIndex), "RadippoolEntity");
			this.AddElementFieldInfo("RadippoolEntity", "Calledstationid", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.Calledstationid, 30, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "Callingstationid", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.Callingstationid, 30, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "ExpiryTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RadippoolFieldIndex.ExpiryTime, 0, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "Framedipaddress", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.Framedipaddress, 15, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadippoolFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadippoolEntity", "Nasipaddress", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.Nasipaddress, 15, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "PoolKey", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.PoolKey, 30, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "PoolName", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.PoolName, 30, 0, 0);
			this.AddElementFieldInfo("RadippoolEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadippoolFieldIndex.Username, 64, 0, 0);
		}
		/// <summary>Inits RadpostauthEntity's FieldInfo objects</summary>
		private void InitRadpostauthEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadpostauthFieldIndex), "RadpostauthEntity");
			this.AddElementFieldInfo("RadpostauthEntity", "Authdate", typeof(System.DateTime), false, false, false, false,  (int)RadpostauthFieldIndex.Authdate, 0, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)RadpostauthFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadpostauthEntity", "Pass", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Pass, 64, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Reply", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Reply, 32, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Username, 64, 0, 0);
		}
		/// <summary>Inits RadreplyEntity's FieldInfo objects</summary>
		private void InitRadreplyEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadreplyFieldIndex), "RadreplyEntity");
			this.AddElementFieldInfo("RadreplyEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Attribute, 32, 0, 0);
			this.AddElementFieldInfo("RadreplyEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadreplyFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadreplyEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadreplyEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Username, 64, 0, 0);
			this.AddElementFieldInfo("RadreplyEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Value, 253, 0, 0);
		}
		/// <summary>Inits UsergroupEntity's FieldInfo objects</summary>
		private void InitUsergroupEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UsergroupFieldIndex), "UsergroupEntity");
			this.AddElementFieldInfo("UsergroupEntity", "Groupname", typeof(System.String), true, false, false, false,  (int)UsergroupFieldIndex.Groupname, 64, 0, 0);
			this.AddElementFieldInfo("UsergroupEntity", "Priority", typeof(System.Int32), false, false, false, false,  (int)UsergroupFieldIndex.Priority, 0, 0, 11);
			this.AddElementFieldInfo("UsergroupEntity", "Username", typeof(System.String), true, false, false, false,  (int)UsergroupFieldIndex.Username, 64, 0, 0);
		}
		
	}
}




