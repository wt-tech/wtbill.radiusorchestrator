﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radcheck.</summary>
	public enum RadcheckFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Username. </summary>
		Username,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radgroupcheck.</summary>
	public enum RadgroupcheckFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Groupname. </summary>
		Groupname,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radgroupreply.</summary>
	public enum RadgroupreplyFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Groupname. </summary>
		Groupname,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radippool.</summary>
	public enum RadippoolFieldIndex
	{
		///<summary>Calledstationid. </summary>
		Calledstationid,
		///<summary>Callingstationid. </summary>
		Callingstationid,
		///<summary>ExpiryTime. </summary>
		ExpiryTime,
		///<summary>Framedipaddress. </summary>
		Framedipaddress,
		///<summary>Id. </summary>
		Id,
		///<summary>Nasipaddress. </summary>
		Nasipaddress,
		///<summary>PoolKey. </summary>
		PoolKey,
		///<summary>PoolName. </summary>
		PoolName,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radpostauth.</summary>
	public enum RadpostauthFieldIndex
	{
		///<summary>Authdate. </summary>
		Authdate,
		///<summary>Id. </summary>
		Id,
		///<summary>Pass. </summary>
		Pass,
		///<summary>Reply. </summary>
		Reply,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radreply.</summary>
	public enum RadreplyFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Username. </summary>
		Username,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Usergroup.</summary>
	public enum UsergroupFieldIndex
	{
		///<summary>Groupname. </summary>
		Groupname,
		///<summary>Priority. </summary>
		Priority,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Radcheck</summary>
		RadcheckEntity,
		///<summary>Radgroupcheck</summary>
		RadgroupcheckEntity,
		///<summary>Radgroupreply</summary>
		RadgroupreplyEntity,
		///<summary>Radippool</summary>
		RadippoolEntity,
		///<summary>Radpostauth</summary>
		RadpostauthEntity,
		///<summary>Radreply</summary>
		RadreplyEntity,
		///<summary>Usergroup</summary>
		UsergroupEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	#endregion

	#region Included code

	#endregion
}

