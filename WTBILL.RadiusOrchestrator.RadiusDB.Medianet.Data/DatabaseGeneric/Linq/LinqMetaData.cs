﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Linq;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.RelationClasses;

namespace WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData: ILinqMetaData
	{
		#region Class Member Declarations
		private IDataAccessAdapter _adapterToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the IDataAccessAdapter object to use empty. To be able to execute the query, an IDataAccessAdapter instance
		/// is required, and has to be set on the LLBLGenProProvider2 object in the query to execute. </summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse) : this (adapterToUse, null)
		{
		}

		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse, FunctionMappingStore customFunctionMappings)
		{
			_adapterToUse = adapterToUse;
			_customFunctionMappings = customFunctionMappings;
		}
	
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType)typeOfEntity)
			{
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.RadcheckEntity:
					toReturn = this.Radcheck;
					break;
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.RadgroupcheckEntity:
					toReturn = this.Radgroupcheck;
					break;
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.RadgroupreplyEntity:
					toReturn = this.Radgroupreply;
					break;
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.RadippoolEntity:
					toReturn = this.Radippool;
					break;
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.RadpostauthEntity:
					toReturn = this.Radpostauth;
					break;
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.RadreplyEntity:
					toReturn = this.Radreply;
					break;
				case WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityType.UsergroupEntity:
					toReturn = this.Usergroup;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <typeparam name="TEntity">the type of the entity to get the datasource for</typeparam>
		/// <returns>the requested datasource</returns>
		public DataSource2<TEntity> GetQueryableForEntity<TEntity>()
			    where TEntity : class
		{
    		return new DataSource2<TEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse);
		}

		/// <summary>returns the datasource to use in a Linq query when targeting RadcheckEntity instances in the database.</summary>
		public DataSource2<RadcheckEntity> Radcheck
		{
			get { return new DataSource2<RadcheckEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RadgroupcheckEntity instances in the database.</summary>
		public DataSource2<RadgroupcheckEntity> Radgroupcheck
		{
			get { return new DataSource2<RadgroupcheckEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RadgroupreplyEntity instances in the database.</summary>
		public DataSource2<RadgroupreplyEntity> Radgroupreply
		{
			get { return new DataSource2<RadgroupreplyEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RadippoolEntity instances in the database.</summary>
		public DataSource2<RadippoolEntity> Radippool
		{
			get { return new DataSource2<RadippoolEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RadpostauthEntity instances in the database.</summary>
		public DataSource2<RadpostauthEntity> Radpostauth
		{
			get { return new DataSource2<RadpostauthEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RadreplyEntity instances in the database.</summary>
		public DataSource2<RadreplyEntity> Radreply
		{
			get { return new DataSource2<RadreplyEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting UsergroupEntity instances in the database.</summary>
		public DataSource2<UsergroupEntity> Usergroup
		{
			get { return new DataSource2<UsergroupEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
 
		#region Class Property Declarations
		/// <summary> Gets / sets the IDataAccessAdapter to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public IDataAccessAdapter AdapterToUse
		{
			get { return _adapterToUse;}
			set { _adapterToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}