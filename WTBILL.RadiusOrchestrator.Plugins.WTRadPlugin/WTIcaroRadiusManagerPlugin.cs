﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WTBILL.RadiusOrchestrator.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.SharedBusiness;
using OrchestrtorDBSpec = WTBILL.RadiusOrchestrator.Data.DatabaseSpecific;
using RadiusDBSpec = WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.DatabaseSpecific;


namespace WTBILL.RadiusOrchestrator.Plugins.WTRadPlugin
{
    public class WTIcaroRadiusManagerPlugin : RadiusManagerPlugin
    {
        public override string GenerateUsername(ServizioxcontrattoEntity sxc)
        {
            string nomeCliente = string.IsNullOrWhiteSpace(sxc.Contratto.Cliente.Denominazione) ? sxc.Contratto.Cliente.Cognome : sxc.Contratto.Cliente.Denominazione;
            nomeCliente = Regex.Replace(nomeCliente, "[^a-zA-Z0-9_]+", "").ToLower();
            nomeCliente = nomeCliente.Substring(0, Math.Min(nomeCliente.Length,10));
            return $"{nomeCliente}{new Random().Next(100)}{sxc.Prodotto.Categoriaprodotto.RadUserPrefisso}{DateTime.Now.Year}{sxc.Contratto.Idcontratto}{sxc.Prodotto.Categoriaprodotto.RadUserSuffisso}";
        }

        public override RadOperationResult SuspendOverdueUser(ServizioxcontrattoEntity sxc, int? nuovoIdSxc)
        {

            //IEnumerable<int> idServiziSospAuto = ConfigurationManager.AppSettings["RadiusManagerAutoSuspendProductIDS"].Split(',').Cast<int>();

            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();

                if (string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if (!sxc.Prodotto.RadiusServiceId.HasValue)
                {
                    res.Success = false;
                    res.Message = "radius service ID non valorizzato sul prodotto!";
                    return res;
                }

                if(sxc.Prodotto.RadiusTerminatedServiceId.HasValue && sxc.Prodotto.RadiusTerminatedServiceId.Value==-1)
                {
                    res.Success = true;
                    res.Message ="Servizio da sospendere manualmente";
                    return res;
                }    

                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    RmUserEntity rmUser = new RmUserEntity();
                    daRad.FetchEntityUsingUniqueConstraint(rmUser, new PredicateExpression(RmUserFields.Username == sxc.IdentificativoAccesso));

                    if (rmUser.IsNew)
                    {
                        res.Success = false;
                        res.Message = "username non trovato su tabella rm_user!";
                        return res;
                    }
                    int oldServiceId = rmUser.Srvid;

                    int idServSosp = RAD_SUSPS_SRV_ID;
                    if (sxc.Prodotto.RadiusTerminatedServiceId.HasValue)
                        idServSosp = sxc.Prodotto.RadiusTerminatedServiceId.Value;

                    rmUser.Srvid = idServSosp;
                    if (daRad.SaveEntity(rmUser))
                    {
                        sxc.Contratto.IdstatoContratto = 9; //sospeso morosità
                                                            //if(sxc.IdstatoServizioXcontratto ==3) //se è attivo lo imposta a moroso 
                                                            //    sxc.IdstatoServizioXcontratto = 5; // sospeso morosità

                        sxc.PresospRadiusServiceId = oldServiceId;

                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc.Contratto);
                        uow.AddForSave(sxc);
                        if (uow.Commit(daOrch, true) > 0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }

                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }
        }

    }
}
