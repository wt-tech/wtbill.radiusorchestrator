﻿
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.SharedBusiness;
using OrchestrtorDBSpec = WTBILL.RadiusOrchestrator.Data.DatabaseSpecific;
using RadiusDBSpec = WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.DatabaseSpecific;

namespace WTBILL.RadiusOrchestrator.Plugins
{
    public class RadiusManagerPlugin : IRadiusPlugin
    {
        protected int RAD_SUSPS_SRV_ID = int.Parse(ConfigurationManager.AppSettings["RadiusManagerSuspendedUserServiceID"]);
        public event EventHandler<UsernameCreatedEventArgs> OnUsernameCreated;
        public event EventHandler<PasswordCreatedEventArgs> OnPasswordCreated;
        public RadOperationResult CreateUser(ServizioxcontrattoEntity sxc, int? idNuovoStatoSxc)
        {
            RadOperationResult res = new RadOperationResult();

            if (!sxc.Prodotto.RadiusServiceId.HasValue)
            {
                res.Success = false;
                res.Message = "radius service ID non valorizzato sul prodotto!";
                return res;
            }

         
            UnitOfWork2 uowRad = new UnitOfWork2();

            string username = "";            
            if (!string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                username = sxc.IdentificativoAccesso;
            else
            {

                username = GenerateUsername(sxc);
                sxc.IdentificativoAccesso = username;
            }

            string password = GeneratePassword(sxc);
            if (string.IsNullOrWhiteSpace(sxc.PasswordAccesso))
                sxc.PasswordAccesso = password;
            else
                password = sxc.PasswordAccesso;

            using (var daRad = new RadiusDBSpec.DataAccessAdapter())
            {
                RadcheckEntity rdcPassword = new RadcheckEntity();
                rdcPassword.Username = username;
                rdcPassword.Attribute = "Cleartext-Password";
                rdcPassword.Op = ":=";
                rdcPassword.Value = password;

                uowRad.AddForSave(rdcPassword);

                RadcheckEntity rdcSimUse = new RadcheckEntity();
                rdcSimUse.Username = username;
                rdcSimUse.Attribute = "Simultaneous-Use";
                rdcSimUse.Op = ":=";
                rdcSimUse.Value = "1";

                uowRad.AddForSave(rdcSimUse);

                RmUserEntity rmUser = new RmUserEntity()
                {
                    Groupid = 1,
                    Enableuser = 1,
                    Username = username,
                    Password = HashPassword(password),
                    Uplimit = 0,
                    Downlimit = 0,
                    Comblimit = 0,
                    Company = sxc.Contratto.Cliente.Denominazione.Substring(0, Math.Min(sxc.Contratto.Cliente.Denominazione.Length, 48)),
                    Phone = string.Empty,
                    Mobile = string.Empty,
                    Address = string.Empty,
                    City = string.Empty,
                    Zip = string.Empty,
                    Country = string.Empty,
                    State = string.Empty,
                    Comment = string.Empty,
                    Gpslat = 0,
                    Gpslong = 0,
                    Mac = "",
                    Usemacauth = 0,
                    Expiration = new DateTime(2099, 12, 31),
                    Uptimelimit = 0,
                    Staticipcm = string.Empty,
                    Staticipcpe = string.Empty,
                    Lastname = sxc.Contratto.Cliente.Cognome.Substring(0,Math.Min(sxc.Contratto.Cliente.Cognome.Length,48)),
                    Firstname = sxc.Contratto.Cliente.Nome.Substring(0,Math.Min(sxc.Contratto.Cliente.Nome.Length,48)),
                    Srvid = sxc.Prodotto.RadiusServiceId.Value,
                    Ipmodecm = 0,
                    Ipmodecpe = 0,
                    Poolidcm = 0,
                    Poolidcpe = 0,
                    Createdon = DateTime.Now,
                    Createdby = "admin",
                    Owner = "admin",
                    Acctype = 0,
                    Credits = 0,
                    Cardfails = 0,
                    Taxid = string.Empty,
                    Email = string.Empty,
                    Maccm = string.Empty,
                    Custattr = string.Empty,
                    Warningsent = 0,
                    Verifycode = string.Empty,
                    Verified = 0,
                    Selfreg = 0,
                    Verifyfails = 0,
                    Verifysentnum = 0,
                    Verifymobile = string.Empty,
                    Contractid = $"{sxc.Contratto.Numero}-{sxc.Contratto.Idcontratto}",
                    Actcode = string.Empty,
                    Pswactsmsnum = 0,
                    Alertemail = 0,
                    Alertsms = 0,
                    Contractvalid = new DateTime(2099,12,31),
                    Lang = string.Empty
                };

                uowRad.AddForSave(rmUser);

                if (uowRad.Commit(daRad, true) == 3)
                {
                    using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
                    {

                        if (sxc.CodiceStatoRadius != 95 && idNuovoStatoSxc.HasValue)
                        {
                            //se è forzato non camnbia stato sxc 
                            sxc.IdstatoServizioXcontratto = 8; // provisioned
                        }
                        sxc.CodiceStatoRadius = 10;
                        daOrch.SaveEntity(sxc);

                        res.Message = $"user: {username}";
                        res.Success = true;
                        return res;
                    }
                }
            }

            res.Success = false;
            return res;
        }

        private string HashPassword(string password)
        {
            byte[] originalBytes;
            byte[] encodedBytes;
            MD5 md5;

            // Conver the original password to bytes; then create the hash
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(password);
            encodedBytes = md5.ComputeHash(originalBytes);

            // Bytes to string
            return Regex.Replace(BitConverter.ToString(encodedBytes), "-", "").ToLower();
        }

        protected static Random random = new Random();

      

        protected static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public virtual string GenerateUsername(ServizioxcontrattoEntity sxc)
        {
            var username = $"{sxc.Prodotto.Categoriaprodotto.RadUserPrefisso}{DateTime.Now.Year}{sxc.Contratto.Idcontratto}_{new Random().Next(999)}{sxc.Prodotto.Categoriaprodotto.RadUserSuffisso}";

            return username;
                
        }

        public virtual string GeneratePassword(ServizioxcontrattoEntity sxc)
        {
            return RandomString(8).ToUpper();
        }


        public virtual List<ServizioxcontrattoEntity> GetUsersToCreate()
        {

            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(
                    (ContrattoFields.IdstatoContratto == 6 &
                    (ServizioxcontrattoFields.CodiceStatoRadius == DBNull.Value | ServizioxcontrattoFields.CodiceStatoRadius ==0) &
                    ProdottoFields.RadiusServiceId != DBNull.Value)
                    |
                    ServizioxcontrattoFields.CodiceStatoRadius == 95 //forza creazione nuovo utente
                    );
               
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);
                

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public List<ServizioxcontrattoEntity> GetOverdueUsersToReactivate()
        {
            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 15 & ProdottoFields.RadiusServiceId != DBNull.Value);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public List<ServizioxcontrattoEntity> GetOverdueUsersToSuspend()
        {
            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();
                
                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente); ;
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                //bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 14 & ProdottoFields.RadiusServiceId != DBNull.Value);
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 14);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public virtual RadOperationResult ReactivateOverdueSuspended(ServizioxcontrattoEntity sxc)
        {
            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();

                if (string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if (!sxc.Prodotto.RadiusServiceId.HasValue && !sxc.PresospRadiusServiceId.HasValue)
                {
                    res.Success = false;
                    res.Message = "radius service ID non valorizzato sul prodotto e nemmeno su sxc in pre-sosp!";
                    return res;
                }


                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    RmUserEntity rmUser = new RmUserEntity();
                    daRad.FetchEntityUsingUniqueConstraint(rmUser, new PredicateExpression(RmUserFields.Username == sxc.IdentificativoAccesso));

                    if (rmUser.IsNew)
                    {
                        res.Success = false;
                        res.Message = "username non trovato su tabella rm_user!";
                        return res;
                    }

                    rmUser.Srvid = sxc.PresospRadiusServiceId.HasValue ? sxc.PresospRadiusServiceId.Value : sxc.Prodotto.RadiusServiceId.Value;
                    if (daRad.SaveEntity(rmUser))
                    {

                        sxc.Contratto.IdstatoContratto = 3; //attivo
                        if (sxc.IdstatoServizioXcontratto == 5) //se è moroso lo riattiva || questa cosa è stata lasciata per precauzione, ma sulla sospensione non viene più modificato lo stato di sxc
                            sxc.IdstatoServizioXcontratto = 3; // attivo

                        sxc.PresospRadiusServiceId = null;

                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc);
                        uow.AddForSave(sxc.Contratto);
                        
                        if (uow.Commit(daOrch,true)>0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }

                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }
        }

        public virtual RadOperationResult SuspendOverdueUser(ServizioxcontrattoEntity sxc, int? nuovoIdSxc)
        {
            
            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();
                   
                if(string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if(!sxc.Prodotto.RadiusServiceId.HasValue)
                {
                    res.Success = false;
                    res.Message = "radius service ID non valorizzato sul prodotto!";
                    return res;
                }


                using(var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    RmUserEntity rmUser = new RmUserEntity();
                    daRad.FetchEntityUsingUniqueConstraint(rmUser, new PredicateExpression(RmUserFields.Username == sxc.IdentificativoAccesso));

                    if(rmUser.IsNew)
                    {
                        res.Success = false;
                        res.Message = "username non trovato su tabella rm_user!";
                        return res;
                    }
                    int oldServiceId = rmUser.Srvid;

                    int idServSosp = RAD_SUSPS_SRV_ID;
                    if (sxc.Prodotto.RadiusTerminatedServiceId.HasValue)
                        idServSosp = sxc.Prodotto.RadiusTerminatedServiceId.Value;

                    rmUser.Srvid = idServSosp;
                    if (daRad.SaveEntity(rmUser))
                    {
                        sxc.Contratto.IdstatoContratto = 9; //sospeso morosità
                        //if(sxc.IdstatoServizioXcontratto ==3) //se è attivo lo imposta a moroso 
                        //    sxc.IdstatoServizioXcontratto = 5; // sospeso morosità
                        
                        sxc.PresospRadiusServiceId = oldServiceId;

                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc.Contratto);
                        uow.AddForSave(sxc);
                        if(uow.Commit(daOrch,true) >0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }
                    
                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }
        }

        public RadOperationResult SyncAttribute(AttributoradiuEntity attr)
        {
            RadOperationResult res = new RadOperationResult();

            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    if (attr.IsToInsert == 1)
                    {
                        RadreplyEntity rr = new RadreplyEntity()
                        {
                            Username = attr.Servizioxcontratto.IdentificativoAccesso,
                            Attribute = attr.NomeAttributo,
                            Op = attr.Operatore,
                            Value = attr.Valore
                        };



                        if (daRad.SaveEntity(rr))
                        {
                            attr.IsToInsert = 0;
                            if (daOrch.SaveEntity(attr))
                            {
                                res.Success = true;
                                res.Message = "CREATO";
                                return res;
                            }
                        }
                    }
                    else if (attr.IsToDelete == 1)
                    {
                        var rrs = new RadiusDB.RadiusManager.Data.HelperClasses.EntityCollection<RadreplyEntity>();
                        daRad.FetchEntityCollection(rrs, new RelationPredicateBucket(RadreplyFields.Username == attr.Servizioxcontratto.IdentificativoAccesso & RadreplyFields.Attribute == attr.NomeAttributo & RadreplyFields.Value == attr.Valore));
                        if (rrs.Count >0)
                        {
                            if (daRad.DeleteEntityCollection(rrs)>0)
                            {
                                if (daOrch.DeleteEntity(attr))
                                {
                                    res.Success = true;
                                    res.Message = "ELIMINATO";
                                    return res;
                                }
                            }
                        }
                        else if (rrs.Count == 0)
                        {
                            if (daOrch.DeleteEntity(attr))
                            {
                                res.Success = true;
                                res.Message = "Attributo eliminato solo su gestionale, non era presente in rad";
                                return res;
                            }
                        }
                        else
                        {
                            res.Success = false;
                            res.Message = "Attributi da eliminare in numero diverso da 1 o 0. Operazione fallita.";
                            return res;
                        }
                    }
                }
            }

            res.Success = false;
            res.Message = "operazione non eseguita";

            return res;
        }

        public List<AttributoradiuEntity> GetAttributesToSync()
        {
            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                var attributi = new Data.HelperClasses.EntityCollection<AttributoradiuEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.AttributoradiuEntity);
                pp2.Add(AttributoradiuEntity.PrefetchPathServizioxcontratto);

                daOrch.FetchEntityCollection(attributi, new RelationPredicateBucket(AttributoradiuFields.IsToDelete == 1 | AttributoradiuFields.IsToInsert == 1), pp2);

                return attributi.ToList();

            }
        }

        public List<RadOperationResult> SyncCreateAllUsers()
        {
            var radResults = new List<RadOperationResult>();
            RadOperationResult res;

            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);


                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ProdottoFields.RadiusServiceId != DBNull.Value);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

               

                daOrch.FetchEntityCollection(servizi, bucket, pp2);
                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    foreach (var s in servizi)
                    {
                        if (string.IsNullOrWhiteSpace(s.IdentificativoAccesso))
                            continue;

                        if (daRad.GetDbCount(new RadiusDB.RadiusManager.Data.HelperClasses.EntityCollection<RmUserEntity>(), new RelationPredicateBucket(RmUserFields.Username == s.IdentificativoAccesso)) > 0)
                            continue; //gia presente


                        res = CreateUser(s, null);
                        radResults.Add(res);
                        
                    }
                }
            }

            return radResults;
        }

        public List<ServizioxcontrattoEntity> GetSeasonalUsersToSuspend()
        {
            return new List<ServizioxcontrattoEntity>();
        }

        public List<ServizioxcontrattoEntity> GetSeasonalUsersToReactivate()
        {
            return new List<ServizioxcontrattoEntity>();
        }

        public RadOperationResult SuspendSeasonalUser(ServizioxcontrattoEntity sxc)
        {
            return new RadOperationResult() { Success = true, Message="Non implementato" };
        }

        public RadOperationResult ReactivateSeasonalSuspended(ServizioxcontrattoEntity sxc)
        {
            return new RadOperationResult() { Success = true, Message = "Non implementato" };
        }

        public List<ServizioxcontrattoEntity> GetUsersToErase()
        {
            return new List<ServizioxcontrattoEntity>();
        }

        public RadOperationResult EraseUser(ServizioxcontrattoEntity sxc)
        {
            return new RadOperationResult() { Success = true, Message = "Non implementato" };
        }
    }
}
