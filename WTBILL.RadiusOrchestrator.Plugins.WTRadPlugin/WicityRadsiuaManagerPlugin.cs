﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using OrchestrtorDBSpec = WTBILL.RadiusOrchestrator.Data.DatabaseSpecific;

namespace WTBILL.RadiusOrchestrator.Plugins.WTRadPlugin
{
    public class WicityRadsiuaManagerPlugin : RadiusManagerPlugin
    {
        public override List<ServizioxcontrattoEntity> GetUsersToCreate()
        {
            using (var daOrch = new OrchestrtorDBSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);


                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(
                    (ContrattoFields.IdstatoContratto == 10 &
                    (ServizioxcontrattoFields.CodiceStatoRadius == DBNull.Value | ServizioxcontrattoFields.CodiceStatoRadius == 0) &
                    ProdottoFields.RadiusServiceId != DBNull.Value)
                    |
                    ServizioxcontrattoFields.CodiceStatoRadius == 95 //forza creazione nuovo utente
                    );

                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);


                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }
        public override string GenerateUsername(ServizioxcontrattoEntity sxc)
        {
            string randomCharList="";
            string username="";
            var rnd = new Random();

            switch (sxc.Prodotto.Categoriaprodotto.IdcategoriaProdotto)
            {
                case 7: //adsl
                case 13: //adsl b
                case 12: //fttcab b
                case 8: //fttcab
                case 9: //ftth
                case 14:// easy ip
                    randomCharList  = "1234567890bcdfghjklmnpqrstvwxyz";
                    
                    for (int i = 1; i <8; i++)
                    {
                        username += randomCharList[rnd.Next(randomCharList.Length)];
                    }
                    username = $"{sxc.Prodotto.Categoriaprodotto.RadUserPrefisso}{username}{sxc.Prodotto.Categoriaprodotto.RadUserSuffisso}";
                    
                    break;

                case 1: //wdsl
                case 11: //wdsl b

                    string parteCognome = sxc.Contratto.Cliente.Cognome.Substring(0, Math.Min(4, sxc.Contratto.Cliente.Cognome.Length)).ToUpper();
                    string parteNome = sxc.Contratto.Cliente.Nome.ToUpper();
                    var toRemove = new string[] { "A", "E", "I", "O", "U" };
                    foreach (var c in toRemove)
                    {
                        parteNome = parteNome.Replace(c, "");
                    }
                    parteNome = parteNome.Substring(0, Math.Min(4, parteNome.Length));

                    
                    randomCharList = "bcdfghjklmnpqrstvwxyz";
                    if (parteNome.Length < 4)
                    {
                        int nrCharToInsert = 4 - parteNome.Length;
                        for (int i = 0; i < nrCharToInsert; i++)
                        {
                            parteNome = parteNome + randomCharList[rnd.Next(randomCharList.Length)];
                        }
                    }
                    parteNome = parteNome + randomCharList[rnd.Next(randomCharList.Length)];
                    parteNome = parteNome + randomCharList[rnd.Next(randomCharList.Length)];
                    parteNome = parteNome.ToUpper();

                    string nomeFileCounter = Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "contatore_username.txt");
                    int contatore = int.Parse(File.ReadAllText(@nomeFileCounter)) + 1;
                    File.WriteAllText(nomeFileCounter, contatore.ToString());                   
                    username = $"{sxc.Prodotto.Categoriaprodotto.RadUserPrefisso}{parteCognome}{parteNome}{contatore}{sxc.Prodotto.Categoriaprodotto.RadUserSuffisso}";

                    break;

                default:
                    throw new Exception("Categoria prodotto " + sxc.Prodotto.Categoriaprodotto.Descrizione + " non implementata");

            }

           
            return username;
        }

       
    }
}
