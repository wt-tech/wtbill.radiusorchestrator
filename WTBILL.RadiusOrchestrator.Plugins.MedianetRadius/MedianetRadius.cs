﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTBILL.RadiusOrchestrator.Data.EntityClasses;
using RadiusDBSpec = WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.DatabaseSpecific;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.SharedBusiness;
using OrchDbSpec = WTBILL.RadiusOrchestrator.Data.DatabaseSpecific;
using System.Xml.Schema;
using WTBILL.RadiusOrchestrator.RadiusDB.Medianet.Data.HelperClasses;
using System.Text.RegularExpressions;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;

namespace WTBILL.RadiusOrchestrator.Plugins.Medianet
{
    public class MedianetRadius : IRadiusPlugin
    {
        private const string RAD_SUSP_GROUP = "Moroso";

        //       ALTER TABLE `usergroup` ADD PRIMARY KEY(`username`, `groupname`);



        public RadOperationResult CreateUser(ServizioxcontrattoEntity sxc, int? idNuovoStatoSxc)
        {
           return  InternalCreateUser(sxc, idNuovoStatoSxc, null);
        }

        private RadOperationResult InternalCreateUser(ServizioxcontrattoEntity sxc, int? idNuovoStatoSxc, int? idNuovoStatoContratto)
        {
            RadOperationResult res = new RadOperationResult();

           if (!sxc.Prodotto.IdgruppoRadius.HasValue)
            {
                res.Success = false;
                res.Message = $"Radius Group Name non valorizzato sul prodotto ({sxc.Prodotto.Codice}){sxc.Prodotto.Descrizione}";
                return res;
            }

            if (string.IsNullOrWhiteSpace(sxc.Prodotto.Categoriaprodotto.RadUserSuffisso))
            {
                res.Success = false;
                res.Message = $"Suffisso radius non presente sulla categoria {sxc.Prodotto.Categoriaprodotto.Descrizione}";
                return res;
            }




             UnitOfWork2 uowRad = new UnitOfWork2();

            string username = "";
            if (!string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                username = sxc.IdentificativoAccesso;
            else
            {
                username = GenerateUsername(sxc);               
                sxc.IdentificativoAccesso = username;
            }

            string password = GeneratePassword(sxc);

            if (!string.IsNullOrWhiteSpace(sxc.PasswordAccesso))
                password = sxc.PasswordAccesso;
            
            sxc.PasswordAccesso = password;



            RadcheckEntity radChck = new RadcheckEntity()
            {
                Username = username,
                Attribute = "ClearText-Password",
                Op = ":=",
                Value = password

            };

            uowRad.AddForSave(radChck);

            /*  TO DO
             * 
             * in radreply ci vanno:
    a) Attualmente ci sono i pool, ma e’ un dato che non utilizziamo percio’ potrebbe anche non esserci
    b) Framed-IP-Address nel caso ci sia da assegnare un ip statico ( ad esempio:  “municipio_sanveromilis_vdsl” “Framed-IP-Address” “:=” “213.178.223.148” )
    c) Framed-Route nel caso ci sia da assegnare un’intera rete ad un cliente ( ad esempio: “cfaddacarbonia” “Framed-Route” “=” “213.178.223.135/32 213.178.223.134 1”;
    */


            UsergroupEntity userGroup = new UsergroupEntity()
            {
                Username = username,
                Groupname = sxc.Prodotto.Grupporadiu.GruppoRadius,
                Priority = 4 //come da istruzioni di fabrizo del 25/01 via whatsapp
            };

            uowRad.AddForSave(userGroup);

            using (var daRadius = new RadiusDBSpec.DataAccessAdapter())
            {
                if(daRadius.GetDbCount(new RadiusDB.Medianet.Data.HelperClasses.EntityCollection<RadcheckEntity>(), new RelationPredicateBucket(RadcheckFields.Username == username))>0)
                {
                    res.Success = false;
                    res.Message = $"user: {username} già presente in radcheck";
                    return res;
                }
                using (var daORch = new OrchDbSpec.DataAccessAdapter())
                {
                    if (uowRad.Commit(daRadius, true) > 0)
                    {
                        if (sxc.CodiceStatoRadius != 95)
                        {
                            //se è forzato non camnbia stato sxc 
                            sxc.IdstatoServizioXcontratto = 8; // provisioned
                        }

                        sxc.CodiceStatoRadius = 10;
                        
                        if(idNuovoStatoContratto.HasValue)
                        {
                            sxc.Contratto.IdstatoContratto = idNuovoStatoContratto.Value;
                            daORch.SaveEntity(sxc.Contratto);
                        }

                        if (idNuovoStatoSxc.HasValue)
                            sxc.IdstatoServizioXcontratto = idNuovoStatoSxc.Value;

                        if (daORch.SaveEntity(sxc))
                        {
                            res.Message = $"user: {username}";
                            res.Success = true;
                            return res;
                        }
                    }
                }
            }

                res.Success = false;

                return res;
        }

        protected static Random random = new Random();
        protected static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public virtual string GeneratePassword(ServizioxcontrattoEntity sxc)
        {
            return RandomString(10);
        }

        public virtual string GenerateUsername(ServizioxcontrattoEntity sxc)
        {
            string username;
            if (sxc.Contratto.Cliente.IdtipoCliente == 1)// private
            {
                username = $"{SanitizeUsername(sxc.Contratto.Cliente.Cognome)}{SanitizeUsername(sxc.Contratto.Cliente.Nome)}{new Random().Next(9999)}{sxc.Prodotto.Categoriaprodotto.RadUserSuffisso}";
            }
            else
            {
                string denominazione = sxc.Contratto.Cliente.Denominazione;
                if (!string.IsNullOrWhiteSpace(sxc.Contratto.Cliente.InsegnaCommerciale))
                    denominazione = sxc.Contratto.Cliente.InsegnaCommerciale;

                username = $"{SanitizeUsername(denominazione)}{new Random().Next(9999)}{sxc.Prodotto.Categoriaprodotto.RadUserSuffisso}";
            }

            return username;
        }
        public static string SanitizeUsername(string baseString)
        {
            string user = Regex.Replace(baseString, "[^a-zA-Z0-9_]+", "");           
            return user.ToLower();
        }

        public List<ServizioxcontrattoEntity> GetUsersToCreate()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);


                RelationPredicateBucket bucket = new RelationPredicateBucket();
                //bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 6 & (ServizioxcontrattoFields.CodiceStatoRadius == DBNull.Value | ServizioxcontrattoFields.CodiceStatoRadius == 0 | ServizioxcontrattoFields.CodiceStatoRadius == 95) & ProdottoFields.IdgruppoRadius != DBNull.Value);

                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 1 & (ServizioxcontrattoFields.CodiceStatoRadius == DBNull.Value | ServizioxcontrattoFields.CodiceStatoRadius == 0 | ServizioxcontrattoFields.CodiceStatoRadius == 95) & ProdottoFields.IdgruppoRadius != DBNull.Value);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);


                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public List<ServizioxcontrattoEntity> GetOverdueUsersToReactivate()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 15 & ProdottoFields.IdgruppoRadius != DBNull.Value);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public List<ServizioxcontrattoEntity> GetOverdueUsersToSuspend()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 14 &  ProdottoFields.IdgruppoRadius != DBNull.Value);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public RadOperationResult ReactivateOverdueSuspended(ServizioxcontrattoEntity sxc)
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();

                if (string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if (!sxc.Prodotto.IdgruppoRadius.HasValue && string.IsNullOrWhiteSpace(sxc.PresospRadiusGroupName))
                {
                    res.Success = false;
                    res.Message = "gruppo radius non valorizzato sul prodotto e nemnmeno su sxc!";
                    return res;
                }


                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    UsergroupEntity usrGroup = new UsergroupEntity();
                    daRad.FetchEntityUsingUniqueConstraint(usrGroup, new PredicateExpression(UsergroupFields.Username == sxc.IdentificativoAccesso));

                    if (usrGroup.IsNew)
                    {
                        res.Success = false;
                        res.Message = "username non trovato su tabella usergroup!";
                        return res;
                    }

                    usrGroup.Groupname = sxc.Prodotto.Grupporadiu.GruppoRadius;
                    
                    if (!string.IsNullOrWhiteSpace(sxc.PresospRadiusGroupName))
                        usrGroup.Groupname = sxc.PresospRadiusGroupName;


                    if (daRad.SaveEntity(usrGroup))
                    {
                        sxc.Contratto.IdstatoContratto = 3; //attivo
                        if(sxc.IdstatoServizioXcontratto == 5)
                            sxc.IdstatoServizioXcontratto = 3; // attivo

                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc.Contratto);
                        uow.AddForSave(sxc);
                        if (uow.Commit(daOrch, true) > 0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }

                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }
        }

        public RadOperationResult SuspendOverdueUser(ServizioxcontrattoEntity sxc, int? idNuovoStatoSxc)
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();

                if (string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if (!sxc.Prodotto.IdgruppoRadius.HasValue)
                {
                    res.Success = false;
                    res.Message = "gruppo radius non valorizzato sul prodotto!";
                    return res;
                }


                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    UsergroupEntity usrGroup = new UsergroupEntity();
                    daRad.FetchEntityUsingUniqueConstraint(usrGroup, new PredicateExpression(UsergroupFields.Username == sxc.IdentificativoAccesso));

                    if (usrGroup.IsNew)
                    {
                        res.Success = false;
                        res.Message = "username non trovato su tabella usergroup!";
                        return res;
                    }

                    sxc.PresospRadiusGroupName = usrGroup.Groupname;
                    usrGroup.Groupname = RAD_SUSP_GROUP;
                    

                    if (daRad.SaveEntity(usrGroup))
                    {
                        sxc.Contratto.IdstatoContratto = 9; //sospeso morosità
                        //if(sxc.IdstatoServizioXcontratto ==3 )
                          //  sxc.IdstatoServizioXcontratto = 5; // sospeso morosità
                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc.Contratto);
                        uow.AddForSave(sxc);
                        if (uow.Commit(daOrch, true) > 0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }

                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }
        }

        public RadOperationResult SyncAttribute(AttributoradiuEntity attr)
        {
            RadOperationResult res = new RadOperationResult();

            //if (!attr.Servizioxcontratto.CodiceStatoRadius.HasValue || (attr.Servizioxcontratto.CodiceStatoRadius.Value != 10))
            //{
            //    res.Success = false;
            //    res.Message = "stato radius <> 10";
            //    return res;
            //}

            if (string.IsNullOrEmpty(attr.Servizioxcontratto.IdentificativoAccesso))
            {
                res.Success = false;
                res.Message = "username non presente";
                return res;
            }

            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    if (attr.IsToInsert == 1)
                    {
                        RadreplyEntity rr = new RadreplyEntity()
                        {
                            Username = attr.Servizioxcontratto.IdentificativoAccesso,
                            Attribute = attr.NomeAttributo,
                            Op = attr.Operatore,
                            Value = attr.Valore
                        };

                        

                        if (daRad.SaveEntity(rr))
                        {
                            attr.IsToInsert = 0;
                            if(daOrch.SaveEntity(attr))
                            {
                                res.Success = true;
                                res.Message = "CREATO";
                                return res;
                            }
                        }
                    }
                    else if (attr.IsToDelete ==1)
                    {
                        var rrs = new RadiusDB.Medianet.Data.HelperClasses.EntityCollection<RadreplyEntity>();
                        daRad.FetchEntityCollection(rrs, new RelationPredicateBucket(RadreplyFields.Username == attr.Servizioxcontratto.IdentificativoAccesso & RadreplyFields.Attribute == attr.NomeAttributo & RadreplyFields.Value == attr.Valore));
                        if(rrs.Count==1)
                        {
                            if (daRad.DeleteEntity(rrs[0]))
                            {
                                if(daOrch.DeleteEntity(attr))
                                {
                                    res.Success = true;
                                    res.Message = "ELIMINATO";
                                    return res;
                                }
                            }
                        }
                        else
                        {
                            res.Success = false;
                            res.Message = "Non trovo attributo da eliminare";
                            return res;
                        }
                    }
                }
            }

            res.Success = false;
            res.Message = "operazione non eseguita";

            return res;
        }

        public List<AttributoradiuEntity> GetAttributesToSync()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var attributi = new Data.HelperClasses.EntityCollection<AttributoradiuEntity>();
                
                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.AttributoradiuEntity);
                pp2.Add(AttributoradiuEntity.PrefetchPathServizioxcontratto);

                daOrch.FetchEntityCollection(attributi, new RelationPredicateBucket(AttributoradiuFields.IsToDelete ==1 | AttributoradiuFields.IsToInsert ==1), pp2);

                return attributi.ToList();

            }
        }

        public List<RadOperationResult> SyncCreateAllUsers()
        {
            var radResults = new List<RadOperationResult>();
            RadOperationResult res;
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);


                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ProdottoFields.IdgruppoRadius != DBNull.Value);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);
                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    foreach (var s in servizi)
                    {
                        if (string.IsNullOrWhiteSpace(s.IdentificativoAccesso))
                            continue;

                        if (daRad.GetDbCount(new RadiusDB.Medianet.Data.HelperClasses.EntityCollection<RadcheckEntity>(), new RelationPredicateBucket(RadcheckFields.Username == s.IdentificativoAccesso)) > 0)
                            continue;
                
                        res = CreateUser(s, null);
                        radResults.Add(res);

                    }
                }
            }



                return radResults;

        }

        public List<ServizioxcontrattoEntity> GetSeasonalUsersToSuspend()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 18 & ProdottoFields.IdgruppoRadius != DBNull.Value);
                bucket.PredicateExpression.Add(ServizioxcontrattoFields.DataDecorrenzaCessazione == DBNull.Value | ServizioxcontrattoFields.DataDecorrenzaCessazione < DateTime.Now.Date);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public List<ServizioxcontrattoEntity> GetSeasonalUsersToReactivate()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == 19 & ProdottoFields.IdgruppoRadius != DBNull.Value);                
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public RadOperationResult SuspendSeasonalUser(ServizioxcontrattoEntity sxc)
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();

                if (string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if (!sxc.Prodotto.IdgruppoRadius.HasValue)
                {
                    res.Success = false;
                    res.Message = "gruppo radius non valorizzato sul prodotto!";
                    return res;
                }


                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    RadcheckEntity radChcek = new RadcheckEntity();
                    UsergroupEntity radUserGroup = new UsergroupEntity();
                    daRad.FetchEntityUsingUniqueConstraint(radChcek, new PredicateExpression(RadcheckFields.Username == sxc.IdentificativoAccesso));
                    daRad.FetchEntityUsingUniqueConstraint(radUserGroup, new PredicateExpression(UsergroupFields.Username == sxc.IdentificativoAccesso));

                    if (radChcek.IsNew)
                    {
                        res.Success = false;
                        res.Message = "username non trovato su tabella radcheck!";
                        return res;
                    }

                    

                    if (daRad.DeleteEntity(radChcek))
                    {
                        if (!radUserGroup.IsNew)
                            daRad.DeleteEntity(radUserGroup);

                        sxc.Contratto.IdstatoContratto = 7; // sospeso
                        sxc.CodiceStatoRadius = 0;
                                                          
                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc.Contratto);
                        uow.AddForSave(sxc);
                        if (uow.Commit(daOrch, true) > 0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }

                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }

        }

        public RadOperationResult ReactivateSeasonalSuspended(ServizioxcontrattoEntity sxc)
        {
            return InternalCreateUser(sxc, 3, 3);
        }

        public List<ServizioxcontrattoEntity> GetUsersToErase()
        {
            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                var servizi = new Data.HelperClasses.EntityCollection<ServizioxcontrattoEntity>();

                var statiEliminabili = new int[] { 17, 5};

                PrefetchPath2 pp2 = new PrefetchPath2(EntityType.ServizioxcontrattoEntity);
                pp2.Add(ServizioxcontrattoEntity.PrefetchPathContratto).SubPath.Add(ContrattoEntity.PrefetchPathCliente);
                IPrefetchPath2 subProdotto = pp2.Add(ServizioxcontrattoEntity.PrefetchPathProdotto).SubPath;
                subProdotto.Add(ProdottoEntity.PrefetchPathCategoriaprodotto);
                subProdotto.Add(ProdottoEntity.PrefetchPathGrupporadiu);

                RelationPredicateBucket bucket = new RelationPredicateBucket();
                bucket.PredicateExpression.Add(ContrattoFields.IdstatoContratto == statiEliminabili & ProdottoFields.IdgruppoRadius != DBNull.Value & ServizioxcontrattoFields.CodiceStatoRadius != 0);                
                bucket.PredicateExpression.Add(ServizioxcontrattoFields.DataDecorrenzaCessazione == DBNull.Value | ServizioxcontrattoFields.DataDecorrenzaCessazione < DateTime.Now.Date);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ContrattoEntityUsingIdcontratto);
                bucket.Relations.Add(ServizioxcontrattoEntity.Relations.ProdottoEntityUsingIdprodotto);

                daOrch.FetchEntityCollection(servizi, bucket, pp2);

                return servizi.ToList();
            }
        }

        public RadOperationResult EraseUser(ServizioxcontrattoEntity sxc)
        {

            using (var daOrch = new OrchDbSpec.DataAccessAdapter())
            {
                RadOperationResult res = new RadOperationResult();

                if (string.IsNullOrWhiteSpace(sxc.IdentificativoAccesso))
                {
                    res.Success = false;
                    res.Message = "identificativo accesso non presente!";
                    return res;
                }

                if (!sxc.Prodotto.IdgruppoRadius.HasValue)
                {
                    res.Success = false;
                    res.Message = "gruppo radius non valorizzato sul prodotto!";
                    return res;
                }


                using (var daRad = new RadiusDBSpec.DataAccessAdapter())
                {
                    RadcheckEntity radChcek = new RadcheckEntity();
                    UsergroupEntity radUserGroup = new UsergroupEntity();
                    
                    daRad.FetchEntityUsingUniqueConstraint(radChcek, new PredicateExpression(RadcheckFields.Username == sxc.IdentificativoAccesso));
                    daRad.FetchEntityUsingUniqueConstraint(radUserGroup, new PredicateExpression(UsergroupFields.Username == sxc.IdentificativoAccesso));                    


                    if (radChcek.IsNew)
                    {
                        res.Success = false;
                        res.Message += "username non trovato su tabella radcheck!";
                        return res;
                    }



                    if (daRad.DeleteEntity(radChcek))
                    {
                        if (!radUserGroup.IsNew)
                            daRad.DeleteEntity(radUserGroup);

                        daRad.DeleteEntitiesDirectly(typeof(RadreplyEntity), new RelationPredicateBucket(RadreplyFields.Username == sxc.IdentificativoAccesso));

                        if (sxc.Contratto.IdstatoContratto != 5) //diverso da fatt KO
                        {
                            sxc.Contratto.IdstatoContratto = 8; // disdettato
                        }

                        sxc.CodiceStatoRadius = 0;

                        UnitOfWork2 uow = new UnitOfWork2();
                        uow.AddForSave(sxc.Contratto);
                        uow.AddForSave(sxc);
                        if (uow.Commit(daOrch, true) > 0)
                        {
                            res.Success = true;
                            return res;
                        }
                    }

                }

                res.Success = false;
                res.Message = "operazione non eseguita";

                return res;
            }
        }
    }
}
