﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Servizioxcontratto. </summary>
	public partial class ServizioxcontrattoRelations
	{
		/// <summary>CTor</summary>
		public ServizioxcontrattoRelations()
		{
		}

		/// <summary>Gets all relations of the ServizioxcontrattoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributoradiuEntityUsingIdservizioXcontratto);
			toReturn.Add(this.ContrattoEntityUsingIdcontratto);
			toReturn.Add(this.GrupporadiuEntityUsingIdgruppoRadius);
			toReturn.Add(this.ProdottoEntityUsingIdprodotto);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ServizioxcontrattoEntity and AttributoradiuEntity over the 1:n relation they have, using the relation between the fields:
		/// Servizioxcontratto.IdservizioXcontratto - Attributoradiu.IdservizioXcontratto
		/// </summary>
		public virtual IEntityRelation AttributoradiuEntityUsingIdservizioXcontratto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Attributoradius" , true);
				relation.AddEntityFieldPair(ServizioxcontrattoFields.IdservizioXcontratto, AttributoradiuFields.IdservizioXcontratto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributoradiuEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ServizioxcontrattoEntity and ContrattoEntity over the m:1 relation they have, using the relation between the fields:
		/// Servizioxcontratto.Idcontratto - Contratto.Idcontratto
		/// </summary>
		public virtual IEntityRelation ContrattoEntityUsingIdcontratto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contratto", false);
				relation.AddEntityFieldPair(ContrattoFields.Idcontratto, ServizioxcontrattoFields.Idcontratto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContrattoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ServizioxcontrattoEntity and GrupporadiuEntity over the m:1 relation they have, using the relation between the fields:
		/// Servizioxcontratto.IdgruppoRadius - Grupporadiu.IdgruppoRadius
		/// </summary>
		public virtual IEntityRelation GrupporadiuEntityUsingIdgruppoRadius
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Grupporadiu", false);
				relation.AddEntityFieldPair(GrupporadiuFields.IdgruppoRadius, ServizioxcontrattoFields.IdgruppoRadius);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GrupporadiuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ServizioxcontrattoEntity and ProdottoEntity over the m:1 relation they have, using the relation between the fields:
		/// Servizioxcontratto.Idprodotto - Prodotto.Idprodotto
		/// </summary>
		public virtual IEntityRelation ProdottoEntityUsingIdprodotto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Prodotto", false);
				relation.AddEntityFieldPair(ProdottoFields.Idprodotto, ServizioxcontrattoFields.Idprodotto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProdottoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticServizioxcontrattoRelations
	{
		internal static readonly IEntityRelation AttributoradiuEntityUsingIdservizioXcontrattoStatic = new ServizioxcontrattoRelations().AttributoradiuEntityUsingIdservizioXcontratto;
		internal static readonly IEntityRelation ContrattoEntityUsingIdcontrattoStatic = new ServizioxcontrattoRelations().ContrattoEntityUsingIdcontratto;
		internal static readonly IEntityRelation GrupporadiuEntityUsingIdgruppoRadiusStatic = new ServizioxcontrattoRelations().GrupporadiuEntityUsingIdgruppoRadius;
		internal static readonly IEntityRelation ProdottoEntityUsingIdprodottoStatic = new ServizioxcontrattoRelations().ProdottoEntityUsingIdprodotto;

		/// <summary>CTor</summary>
		static StaticServizioxcontrattoRelations()
		{
		}
	}
}
