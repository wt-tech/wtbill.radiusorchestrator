﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Contratto. </summary>
	public partial class ContrattoRelations
	{
		/// <summary>CTor</summary>
		public ContrattoRelations()
		{
		}

		/// <summary>Gets all relations of the ContrattoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ServizioxcontrattoEntityUsingIdcontratto);
			toReturn.Add(this.ClienteEntityUsingIdcliente);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ContrattoEntity and ServizioxcontrattoEntity over the 1:n relation they have, using the relation between the fields:
		/// Contratto.Idcontratto - Servizioxcontratto.Idcontratto
		/// </summary>
		public virtual IEntityRelation ServizioxcontrattoEntityUsingIdcontratto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Servizioxcontrattos" , true);
				relation.AddEntityFieldPair(ContrattoFields.Idcontratto, ServizioxcontrattoFields.Idcontratto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContrattoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ContrattoEntity and ClienteEntity over the m:1 relation they have, using the relation between the fields:
		/// Contratto.Idcliente - Cliente.Idcliente
		/// </summary>
		public virtual IEntityRelation ClienteEntityUsingIdcliente
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Cliente", false);
				relation.AddEntityFieldPair(ClienteFields.Idcliente, ContrattoFields.Idcliente);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClienteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContrattoEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContrattoRelations
	{
		internal static readonly IEntityRelation ServizioxcontrattoEntityUsingIdcontrattoStatic = new ContrattoRelations().ServizioxcontrattoEntityUsingIdcontratto;
		internal static readonly IEntityRelation ClienteEntityUsingIdclienteStatic = new ContrattoRelations().ClienteEntityUsingIdcliente;

		/// <summary>CTor</summary>
		static StaticContrattoRelations()
		{
		}
	}
}
