﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Cliente. </summary>
	public partial class ClienteRelations
	{
		/// <summary>CTor</summary>
		public ClienteRelations()
		{
		}

		/// <summary>Gets all relations of the ClienteEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContrattoEntityUsingIdcliente);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ClienteEntity and ContrattoEntity over the 1:n relation they have, using the relation between the fields:
		/// Cliente.Idcliente - Contratto.Idcliente
		/// </summary>
		public virtual IEntityRelation ContrattoEntityUsingIdcliente
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Contrattos" , true);
				relation.AddEntityFieldPair(ClienteFields.Idcliente, ContrattoFields.Idcliente);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClienteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContrattoEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClienteRelations
	{
		internal static readonly IEntityRelation ContrattoEntityUsingIdclienteStatic = new ClienteRelations().ContrattoEntityUsingIdcliente;

		/// <summary>CTor</summary>
		static StaticClienteRelations()
		{
		}
	}
}
