﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Categoriaprodotto. </summary>
	public partial class CategoriaprodottoRelations
	{
		/// <summary>CTor</summary>
		public CategoriaprodottoRelations()
		{
		}

		/// <summary>Gets all relations of the CategoriaprodottoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProdottoEntityUsingIdcategoriaProdotto);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CategoriaprodottoEntity and ProdottoEntity over the 1:n relation they have, using the relation between the fields:
		/// Categoriaprodotto.IdcategoriaProdotto - Prodotto.IdcategoriaProdotto
		/// </summary>
		public virtual IEntityRelation ProdottoEntityUsingIdcategoriaProdotto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Prodottos" , true);
				relation.AddEntityFieldPair(CategoriaprodottoFields.IdcategoriaProdotto, ProdottoFields.IdcategoriaProdotto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoriaprodottoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProdottoEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCategoriaprodottoRelations
	{
		internal static readonly IEntityRelation ProdottoEntityUsingIdcategoriaProdottoStatic = new CategoriaprodottoRelations().ProdottoEntityUsingIdcategoriaProdotto;

		/// <summary>CTor</summary>
		static StaticCategoriaprodottoRelations()
		{
		}
	}
}
