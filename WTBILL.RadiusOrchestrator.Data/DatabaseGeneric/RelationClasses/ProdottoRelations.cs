﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Prodotto. </summary>
	public partial class ProdottoRelations
	{
		/// <summary>CTor</summary>
		public ProdottoRelations()
		{
		}

		/// <summary>Gets all relations of the ProdottoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ServizioxcontrattoEntityUsingIdprodotto);
			toReturn.Add(this.CategoriaprodottoEntityUsingIdcategoriaProdotto);
			toReturn.Add(this.GrupporadiuEntityUsingIdgruppoRadius);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProdottoEntity and ServizioxcontrattoEntity over the 1:n relation they have, using the relation between the fields:
		/// Prodotto.Idprodotto - Servizioxcontratto.Idprodotto
		/// </summary>
		public virtual IEntityRelation ServizioxcontrattoEntityUsingIdprodotto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Servizioxcontrattos" , true);
				relation.AddEntityFieldPair(ProdottoFields.Idprodotto, ServizioxcontrattoFields.Idprodotto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProdottoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProdottoEntity and CategoriaprodottoEntity over the m:1 relation they have, using the relation between the fields:
		/// Prodotto.IdcategoriaProdotto - Categoriaprodotto.IdcategoriaProdotto
		/// </summary>
		public virtual IEntityRelation CategoriaprodottoEntityUsingIdcategoriaProdotto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Categoriaprodotto", false);
				relation.AddEntityFieldPair(CategoriaprodottoFields.IdcategoriaProdotto, ProdottoFields.IdcategoriaProdotto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoriaprodottoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProdottoEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProdottoEntity and GrupporadiuEntity over the m:1 relation they have, using the relation between the fields:
		/// Prodotto.IdgruppoRadius - Grupporadiu.IdgruppoRadius
		/// </summary>
		public virtual IEntityRelation GrupporadiuEntityUsingIdgruppoRadius
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Grupporadiu", false);
				relation.AddEntityFieldPair(GrupporadiuFields.IdgruppoRadius, ProdottoFields.IdgruppoRadius);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GrupporadiuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProdottoEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProdottoRelations
	{
		internal static readonly IEntityRelation ServizioxcontrattoEntityUsingIdprodottoStatic = new ProdottoRelations().ServizioxcontrattoEntityUsingIdprodotto;
		internal static readonly IEntityRelation CategoriaprodottoEntityUsingIdcategoriaProdottoStatic = new ProdottoRelations().CategoriaprodottoEntityUsingIdcategoriaProdotto;
		internal static readonly IEntityRelation GrupporadiuEntityUsingIdgruppoRadiusStatic = new ProdottoRelations().GrupporadiuEntityUsingIdgruppoRadius;

		/// <summary>CTor</summary>
		static StaticProdottoRelations()
		{
		}
	}
}
