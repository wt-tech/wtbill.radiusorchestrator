﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Grupporadiu. </summary>
	public partial class GrupporadiuRelations
	{
		/// <summary>CTor</summary>
		public GrupporadiuRelations()
		{
		}

		/// <summary>Gets all relations of the GrupporadiuEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProdottoEntityUsingIdgruppoRadius);
			toReturn.Add(this.ServizioxcontrattoEntityUsingIdgruppoRadius);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GrupporadiuEntity and ProdottoEntity over the 1:n relation they have, using the relation between the fields:
		/// Grupporadiu.IdgruppoRadius - Prodotto.IdgruppoRadius
		/// </summary>
		public virtual IEntityRelation ProdottoEntityUsingIdgruppoRadius
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Prodottos" , true);
				relation.AddEntityFieldPair(GrupporadiuFields.IdgruppoRadius, ProdottoFields.IdgruppoRadius);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GrupporadiuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProdottoEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GrupporadiuEntity and ServizioxcontrattoEntity over the 1:n relation they have, using the relation between the fields:
		/// Grupporadiu.IdgruppoRadius - Servizioxcontratto.IdgruppoRadius
		/// </summary>
		public virtual IEntityRelation ServizioxcontrattoEntityUsingIdgruppoRadius
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Servizioxcontrattos" , true);
				relation.AddEntityFieldPair(GrupporadiuFields.IdgruppoRadius, ServizioxcontrattoFields.IdgruppoRadius);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GrupporadiuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGrupporadiuRelations
	{
		internal static readonly IEntityRelation ProdottoEntityUsingIdgruppoRadiusStatic = new GrupporadiuRelations().ProdottoEntityUsingIdgruppoRadius;
		internal static readonly IEntityRelation ServizioxcontrattoEntityUsingIdgruppoRadiusStatic = new GrupporadiuRelations().ServizioxcontrattoEntityUsingIdgruppoRadius;

		/// <summary>CTor</summary>
		static StaticGrupporadiuRelations()
		{
		}
	}
}
