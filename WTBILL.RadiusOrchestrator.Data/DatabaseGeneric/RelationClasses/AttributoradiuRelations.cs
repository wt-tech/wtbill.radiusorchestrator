﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Attributoradiu. </summary>
	public partial class AttributoradiuRelations
	{
		/// <summary>CTor</summary>
		public AttributoradiuRelations()
		{
		}

		/// <summary>Gets all relations of the AttributoradiuEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ServizioxcontrattoEntityUsingIdservizioXcontratto);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AttributoradiuEntity and ServizioxcontrattoEntity over the m:1 relation they have, using the relation between the fields:
		/// Attributoradiu.IdservizioXcontratto - Servizioxcontratto.IdservizioXcontratto
		/// </summary>
		public virtual IEntityRelation ServizioxcontrattoEntityUsingIdservizioXcontratto
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Servizioxcontratto", false);
				relation.AddEntityFieldPair(ServizioxcontrattoFields.IdservizioXcontratto, AttributoradiuFields.IdservizioXcontratto);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServizioxcontrattoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributoradiuEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAttributoradiuRelations
	{
		internal static readonly IEntityRelation ServizioxcontrattoEntityUsingIdservizioXcontrattoStatic = new AttributoradiuRelations().ServizioxcontrattoEntityUsingIdservizioXcontratto;

		/// <summary>CTor</summary>
		static StaticAttributoradiuRelations()
		{
		}
	}
}
