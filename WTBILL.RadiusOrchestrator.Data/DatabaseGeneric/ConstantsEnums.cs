﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace WTBILL.RadiusOrchestrator.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Attributoradiu.</summary>
	public enum AttributoradiuFieldIndex
	{
		///<summary>IdattributoRadius. </summary>
		IdattributoRadius,
		///<summary>IdservizioXcontratto. </summary>
		IdservizioXcontratto,
		///<summary>IsToDelete. </summary>
		IsToDelete,
		///<summary>IsToInsert. </summary>
		IsToInsert,
		///<summary>NomeAttributo. </summary>
		NomeAttributo,
		///<summary>Operatore. </summary>
		Operatore,
		///<summary>Valore. </summary>
		Valore,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Categoriaprodotto.</summary>
	public enum CategoriaprodottoFieldIndex
	{
		///<summary>Descrizione. </summary>
		Descrizione,
		///<summary>IdaliquotaIva. </summary>
		IdaliquotaIva,
		///<summary>IdcategoriaProdotto. </summary>
		IdcategoriaProdotto,
		///<summary>IdproceduraProvisioningPredef. </summary>
		IdproceduraProvisioningPredef,
		///<summary>IdtipoLavorazioneTecnicaAttivazione. </summary>
		IdtipoLavorazioneTecnicaAttivazione,
		///<summary>IdtipoLavorazioneTecnicaSync. </summary>
		IdtipoLavorazioneTecnicaSync,
		///<summary>IdtipoLavorazioneTecnicaVariazione. </summary>
		IdtipoLavorazioneTecnicaVariazione,
		///<summary>OrdineFatturazione. </summary>
		OrdineFatturazione,
		///<summary>RadUserPrefisso. </summary>
		RadUserPrefisso,
		///<summary>RadUserSuffisso. </summary>
		RadUserSuffisso,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Cliente.</summary>
	public enum ClienteFieldIndex
	{
		///<summary>Cap. </summary>
		Cap,
		///<summary>Cellulare. </summary>
		Cellulare,
		///<summary>Cf. </summary>
		Cf,
		///<summary>Cfamministratore. </summary>
		Cfamministratore,
		///<summary>Citta. </summary>
		Citta,
		///<summary>CodiceClienteEsterno. </summary>
		CodiceClienteEsterno,
		///<summary>CodicePaeseIva. </summary>
		CodicePaeseIva,
		///<summary>Cognome. </summary>
		Cognome,
		///<summary>DataNascita. </summary>
		DataNascita,
		///<summary>Denominazione. </summary>
		Denominazione,
		///<summary>DocIdnumero. </summary>
		DocIdnumero,
		///<summary>DocIdrilasciatoDa. </summary>
		DocIdrilasciatoDa,
		///<summary>DocIdrilasciatoIl. </summary>
		DocIdrilasciatoIl,
		///<summary>Email. </summary>
		Email,
		///<summary>EscludiSolleciti. </summary>
		EscludiSolleciti,
		///<summary>FecodiceDestinatario. </summary>
		FecodiceDestinatario,
		///<summary>FepecDestinatario. </summary>
		FepecDestinatario,
		///<summary>Iban. </summary>
		Iban,
		///<summary>IdaliquotaIva. </summary>
		IdaliquotaIva,
		///<summary>Idcliente. </summary>
		Idcliente,
		///<summary>IdgruppoCliente. </summary>
		IdgruppoCliente,
		///<summary>IdmodalitaInvioFattura. </summary>
		IdmodalitaInvioFattura,
		///<summary>IdmodalitaPagPreferita. </summary>
		IdmodalitaPagPreferita,
		///<summary>IdstatoSolvenzaCliente. </summary>
		IdstatoSolvenzaCliente,
		///<summary>IdtempiPagamento. </summary>
		IdtempiPagamento,
		///<summary>IdtipoArrotondamento. </summary>
		IdtipoArrotondamento,
		///<summary>IdtipoCliente. </summary>
		IdtipoCliente,
		///<summary>IdtipoDocumentoIdentita. </summary>
		IdtipoDocumentoIdentita,
		///<summary>Indirizzo. </summary>
		Indirizzo,
		///<summary>InsegnaCommerciale. </summary>
		InsegnaCommerciale,
		///<summary>LuogoNascita. </summary>
		LuogoNascita,
		///<summary>MediaGgpagamentoFattura. </summary>
		MediaGgpagamentoFattura,
		///<summary>NazioneIso. </summary>
		NazioneIso,
		///<summary>Nome. </summary>
		Nome,
		///<summary>Note. </summary>
		Note,
		///<summary>NrFattureEmesse. </summary>
		NrFattureEmesse,
		///<summary>NrFattureNonPagate. </summary>
		NrFattureNonPagate,
		///<summary>NumeroCivico. </summary>
		NumeroCivico,
		///<summary>NumSollecitiInviati. </summary>
		NumSollecitiInviati,
		///<summary>Password. </summary>
		Password,
		///<summary>Pec. </summary>
		Pec,
		///<summary>Piva. </summary>
		Piva,
		///<summary>Provincia. </summary>
		Provincia,
		///<summary>ProvinciaNascita. </summary>
		ProvinciaNascita,
		///<summary>RecapitoTelefonico. </summary>
		RecapitoTelefonico,
		///<summary>SaldoContabile. </summary>
		SaldoContabile,
		///<summary>UltimoSollecito. </summary>
		UltimoSollecito,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Contratto.</summary>
	public enum ContrattoFieldIndex
	{
		///<summary>CapInstallazione. </summary>
		CapInstallazione,
		///<summary>Cartaceo. </summary>
		Cartaceo,
		///<summary>CittaInstallazione. </summary>
		CittaInstallazione,
		///<summary>CoordinateInst. </summary>
		CoordinateInst,
		///<summary>DataAttivazione. </summary>
		DataAttivazione,
		///<summary>DataCessazione. </summary>
		DataCessazione,
		///<summary>DataFirma. </summary>
		DataFirma,
		///<summary>DataScadenzaDurataContrattuale. </summary>
		DataScadenzaDurataContrattuale,
		///<summary>DescrizioneAggiuntiva. </summary>
		DescrizioneAggiuntiva,
		///<summary>FecodiceCig. </summary>
		FecodiceCig,
		///<summary>FecodiceCup. </summary>
		FecodiceCup,
		///<summary>FedataContratto. </summary>
		FedataContratto,
		///<summary>FenumeroContratto. </summary>
		FenumeroContratto,
		///<summary>IdcicloPagamento. </summary>
		IdcicloPagamento,
		///<summary>Idcliente. </summary>
		Idcliente,
		///<summary>Idcontratto. </summary>
		Idcontratto,
		///<summary>IddurataContrattule. </summary>
		IddurataContrattule,
		///<summary>IdmodalitaPagamentoAttivazione. </summary>
		IdmodalitaPagamentoAttivazione,
		///<summary>Idpartner. </summary>
		Idpartner,
		///<summary>IdstatoContratto. </summary>
		IdstatoContratto,
		///<summary>IndirizzoInstallazione. </summary>
		IndirizzoInstallazione,
		///<summary>NoteCliente. </summary>
		NoteCliente,
		///<summary>NoteInterne. </summary>
		NoteInterne,
		///<summary>NotePartner. </summary>
		NotePartner,
		///<summary>Numero. </summary>
		Numero,
		///<summary>ProvinciaInstallazione. </summary>
		ProvinciaInstallazione,
		///<summary>WhsaziendaClienteFinale. </summary>
		WhsaziendaClienteFinale,
		///<summary>Whscap. </summary>
		Whscap,
		///<summary>WhscfclienteFinale. </summary>
		WhscfclienteFinale,
		///<summary>Whscitta. </summary>
		Whscitta,
		///<summary>Whscivico. </summary>
		Whscivico,
		///<summary>WhscognomeClienteFinale. </summary>
		WhscognomeClienteFinale,
		///<summary>Whsindirizzo. </summary>
		Whsindirizzo,
		///<summary>WhsnomeClienteFinale. </summary>
		WhsnomeClienteFinale,
		///<summary>Whsnote. </summary>
		Whsnote,
		///<summary>WhspartitaIvaclienteFinale. </summary>
		WhspartitaIvaclienteFinale,
		///<summary>Whsprovincia. </summary>
		Whsprovincia,
		///<summary>WhsrecapitoReferente1. </summary>
		WhsrecapitoReferente1,
		///<summary>WhsrecapitoReferente2. </summary>
		WhsrecapitoReferente2,
		///<summary>Whstoponimo. </summary>
		Whstoponimo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Grupporadiu.</summary>
	public enum GrupporadiuFieldIndex
	{
		///<summary>GruppoRadius. </summary>
		GruppoRadius,
		///<summary>IdgruppoRadius. </summary>
		IdgruppoRadius,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Prodotto.</summary>
	public enum ProdottoFieldIndex
	{
		///<summary>Acquistabile. </summary>
		Acquistabile,
		///<summary>Codice. </summary>
		Codice,
		///<summary>CodiceFatturazione. </summary>
		CodiceFatturazione,
		///<summary>Descrizione. </summary>
		Descrizione,
		///<summary>DescrizioneAggiuntivaContratto. </summary>
		DescrizioneAggiuntivaContratto,
		///<summary>EscludiProrata. </summary>
		EscludiProrata,
		///<summary>IdaliquotaIva. </summary>
		IdaliquotaIva,
		///<summary>IdcategoriaProdotto. </summary>
		IdcategoriaProdotto,
		///<summary>IdgruppoRadius. </summary>
		IdgruppoRadius,
		///<summary>IdlistinoVoip. </summary>
		IdlistinoVoip,
		///<summary>IdproceduraProvisioningPrdef. </summary>
		IdproceduraProvisioningPrdef,
		///<summary>Idprodotto. </summary>
		Idprodotto,
		///<summary>NomeFileContratto. </summary>
		NomeFileContratto,
		///<summary>NomeFileSchedaTecnica. </summary>
		NomeFileSchedaTecnica,
		///<summary>NrGgshiftPrimoAddebito. </summary>
		NrGgshiftPrimoAddebito,
		///<summary>RadiusGroupName. </summary>
		RadiusGroupName,
		///<summary>RadiusServiceId. </summary>
		RadiusServiceId,
		///<summary>RadiusTerminatedServiceId. </summary>
		RadiusTerminatedServiceId,
		///<summary>SoloAmministrativo. </summary>
		SoloAmministrativo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Servizioxcontratto.</summary>
	public enum ServizioxcontrattoFieldIndex
	{
		///<summary>CodiceMigrazione. </summary>
		CodiceMigrazione,
		///<summary>CodiceRisorsa. </summary>
		CodiceRisorsa,
		///<summary>CodiceStatoProvisioning. </summary>
		CodiceStatoProvisioning,
		///<summary>CodiceStatoRadius. </summary>
		CodiceStatoRadius,
		///<summary>Dac. </summary>
		Dac,
		///<summary>DataAttesaCessazione. </summary>
		DataAttesaCessazione,
		///<summary>DataDecorrenzaCessazione. </summary>
		DataDecorrenzaCessazione,
		///<summary>DataDecorrenzaSospensione. </summary>
		DataDecorrenzaSospensione,
		///<summary>DataInstallazioneApparati. </summary>
		DataInstallazioneApparati,
		///<summary>DataProvisioningEffettiva. </summary>
		DataProvisioningEffettiva,
		///<summary>DataRichiestaCessazione. </summary>
		DataRichiestaCessazione,
		///<summary>DataUltimoAggiornamentoSegnali. </summary>
		DataUltimoAggiornamentoSegnali,
		///<summary>DescrizioneAggiuntiva. </summary>
		DescrizioneAggiuntiva,
		///<summary>FatturatoFinoAl. </summary>
		FatturatoFinoAl,
		///<summary>FecodiceCig. </summary>
		FecodiceCig,
		///<summary>FecodiceCup. </summary>
		FecodiceCup,
		///<summary>FedataContratto. </summary>
		FedataContratto,
		///<summary>FenumeroContratto. </summary>
		FenumeroContratto,
		///<summary>FileSchedaTecnica. </summary>
		FileSchedaTecnica,
		///<summary>IdaliquotaIva. </summary>
		IdaliquotaIva,
		///<summary>Idbts. </summary>
		Idbts,
		///<summary>IdcicloImmissioneInFatturazione. </summary>
		IdcicloImmissioneInFatturazione,
		///<summary>Idcontratto. </summary>
		Idcontratto,
		///<summary>IdentificativoAccesso. </summary>
		IdentificativoAccesso,
		///<summary>Identita. </summary>
		Identita,
		///<summary>IdgruppoRadius. </summary>
		IdgruppoRadius,
		///<summary>Idinstallatore. </summary>
		Idinstallatore,
		///<summary>IdproceduraProvisioning. </summary>
		IdproceduraProvisioning,
		///<summary>Idprodotto. </summary>
		Idprodotto,
		///<summary>IdrichiestaCessazione. </summary>
		IdrichiestaCessazione,
		///<summary>IdservizioXcontratto. </summary>
		IdservizioXcontratto,
		///<summary>Idsettore. </summary>
		Idsettore,
		///<summary>IdstatoServizioXcontratto. </summary>
		IdstatoServizioXcontratto,
		///<summary>IdtipoSospensione. </summary>
		IdtipoSospensione,
		///<summary>Idzona. </summary>
		Idzona,
		///<summary>Latitudine. </summary>
		Latitudine,
		///<summary>Longitudine. </summary>
		Longitudine,
		///<summary>Macinstallatore. </summary>
		Macinstallatore,
		///<summary>MandareRidBanca. </summary>
		MandareRidBanca,
		///<summary>MultihomeAccessoAttivo. </summary>
		MultihomeAccessoAttivo,
		///<summary>NoteCliente. </summary>
		NoteCliente,
		///<summary>NoteInstallatore. </summary>
		NoteInstallatore,
		///<summary>NoteInterne. </summary>
		NoteInterne,
		///<summary>NotePartner. </summary>
		NotePartner,
		///<summary>PasswordAccesso. </summary>
		PasswordAccesso,
		///<summary>PresospRadiusGroupName. </summary>
		PresospRadiusGroupName,
		///<summary>PresospRadiusServiceId. </summary>
		PresospRadiusServiceId,
		///<summary>Prezzo. </summary>
		Prezzo,
		///<summary>SegnaleCh0Rx. </summary>
		SegnaleCh0Rx,
		///<summary>SegnaleCh0Tx. </summary>
		SegnaleCh0Tx,
		///<summary>SegnaleCh1Rx. </summary>
		SegnaleCh1Rx,
		///<summary>SegnaleCh1Tx. </summary>
		SegnaleCh1Tx,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Attributoradiu</summary>
		AttributoradiuEntity,
		///<summary>Categoriaprodotto</summary>
		CategoriaprodottoEntity,
		///<summary>Cliente</summary>
		ClienteEntity,
		///<summary>Contratto</summary>
		ContrattoEntity,
		///<summary>Grupporadiu</summary>
		GrupporadiuEntity,
		///<summary>Prodotto</summary>
		ProdottoEntity,
		///<summary>Servizioxcontratto</summary>
		ServizioxcontrattoEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

