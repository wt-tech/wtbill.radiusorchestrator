﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Cliente'.<br/><br/></summary>
	[Serializable]
	public partial class ClienteEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<ContrattoEntity> _contrattos;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Contrattos</summary>
			public static readonly string Contrattos = "Contrattos";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClienteEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public ClienteEntity():base("ClienteEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ClienteEntity(IEntityFields2 fields):base("ClienteEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ClienteEntity</param>
		public ClienteEntity(IValidator validator):base("ClienteEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="idcliente">PK value for Cliente which data should be fetched into this Cliente object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ClienteEntity(System.Int32 idcliente):base("ClienteEntity")
		{
			InitClassEmpty(null, null);
			this.Idcliente = idcliente;
		}

		/// <summary> CTor</summary>
		/// <param name="idcliente">PK value for Cliente which data should be fetched into this Cliente object</param>
		/// <param name="validator">The custom validator object for this ClienteEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ClienteEntity(System.Int32 idcliente, IValidator validator):base("ClienteEntity")
		{
			InitClassEmpty(validator, null);
			this.Idcliente = idcliente;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ClienteEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_contrattos = (EntityCollection<ContrattoEntity>)info.GetValue("_contrattos", typeof(EntityCollection<ContrattoEntity>));
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contrattos":
					this.Contrattos.Add((ContrattoEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contrattos":
					toReturn.Add(Relations.ContrattoEntityUsingIdcliente);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contrattos":
					this.Contrattos.Add((ContrattoEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contrattos":
					this.PerformRelatedEntityRemoval(this.Contrattos, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Contrattos);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_contrattos", ((_contrattos!=null) && (_contrattos.Count>0) && !this.MarkedForDeletion)?_contrattos:null);
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClienteRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Contratto' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoContrattos()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ContrattoFields.Idcliente, null, ComparisonOperator.Equal, this.Idcliente));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ClienteEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._contrattos);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._contrattos = (EntityCollection<ContrattoEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._contrattos != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ContrattoEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ContrattoEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Contrattos", _contrattos);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cap", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cellulare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cf", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cfamministratore", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Citta", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodiceClienteEsterno", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodicePaeseIva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cognome", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataNascita", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Denominazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DocIdnumero", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DocIdrilasciatoDa", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DocIdrilasciatoIl", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EscludiSolleciti", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FecodiceDestinatario", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FepecDestinatario", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Iban", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdaliquotaIva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idcliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdgruppoCliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdmodalitaInvioFattura", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdmodalitaPagPreferita", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdstatoSolvenzaCliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdtempiPagamento", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdtipoArrotondamento", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdtipoCliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdtipoDocumentoIdentita", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Indirizzo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InsegnaCommerciale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LuogoNascita", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaGgpagamentoFattura", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NazioneIso", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Nome", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Note", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NrFattureEmesse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NrFattureNonPagate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumeroCivico", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumSollecitiInviati", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Pec", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Piva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Provincia", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProvinciaNascita", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecapitoTelefonico", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaldoContabile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UltimoSollecito", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Username", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ClienteEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClienteRelations Relations
		{
			get	{ return new ClienteRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Contratto' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathContrattos
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<ContrattoEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ContrattoEntityFactory))), (IEntityRelation)GetRelationsForField("Contrattos")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ClienteEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.ContrattoEntity, 0, null, null, null, null, "Contrattos", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Cap property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."CAP"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Cap
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Cap, true); }
			set	{ SetValue((int)ClienteFieldIndex.Cap, value); }
		}

		/// <summary> The Cellulare property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Cellulare"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Cellulare
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Cellulare, true); }
			set	{ SetValue((int)ClienteFieldIndex.Cellulare, value); }
		}

		/// <summary> The Cf property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."CF"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Cf
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Cf, true); }
			set	{ SetValue((int)ClienteFieldIndex.Cf, value); }
		}

		/// <summary> The Cfamministratore property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."CFAmministratore"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Cfamministratore
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Cfamministratore, true); }
			set	{ SetValue((int)ClienteFieldIndex.Cfamministratore, value); }
		}

		/// <summary> The Citta property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Citta"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Citta
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Citta, true); }
			set	{ SetValue((int)ClienteFieldIndex.Citta, value); }
		}

		/// <summary> The CodiceClienteEsterno property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."CodiceClienteEsterno"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodiceClienteEsterno
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.CodiceClienteEsterno, true); }
			set	{ SetValue((int)ClienteFieldIndex.CodiceClienteEsterno, value); }
		}

		/// <summary> The CodicePaeseIva property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."CodicePaeseIVA"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodicePaeseIva
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.CodicePaeseIva, true); }
			set	{ SetValue((int)ClienteFieldIndex.CodicePaeseIva, value); }
		}

		/// <summary> The Cognome property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Cognome"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Cognome
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Cognome, true); }
			set	{ SetValue((int)ClienteFieldIndex.Cognome, value); }
		}

		/// <summary> The DataNascita property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."DataNascita"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataNascita
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClienteFieldIndex.DataNascita, false); }
			set	{ SetValue((int)ClienteFieldIndex.DataNascita, value); }
		}

		/// <summary> The Denominazione property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Denominazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 180<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Denominazione
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Denominazione, true); }
			set	{ SetValue((int)ClienteFieldIndex.Denominazione, value); }
		}

		/// <summary> The DocIdnumero property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."DocIDNumero"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DocIdnumero
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.DocIdnumero, true); }
			set	{ SetValue((int)ClienteFieldIndex.DocIdnumero, value); }
		}

		/// <summary> The DocIdrilasciatoDa property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."DocIDRilasciatoDa"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DocIdrilasciatoDa
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.DocIdrilasciatoDa, true); }
			set	{ SetValue((int)ClienteFieldIndex.DocIdrilasciatoDa, value); }
		}

		/// <summary> The DocIdrilasciatoIl property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."DocIDRilasciatoIl"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DocIdrilasciatoIl
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClienteFieldIndex.DocIdrilasciatoIl, false); }
			set	{ SetValue((int)ClienteFieldIndex.DocIdrilasciatoIl, value); }
		}

		/// <summary> The Email property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 180<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Email, true); }
			set	{ SetValue((int)ClienteFieldIndex.Email, value); }
		}

		/// <summary> The EscludiSolleciti property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."EscludiSolleciti"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 EscludiSolleciti
		{
			get { return (System.Int16)GetValue((int)ClienteFieldIndex.EscludiSolleciti, true); }
			set	{ SetValue((int)ClienteFieldIndex.EscludiSolleciti, value); }
		}

		/// <summary> The FecodiceDestinatario property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."FECodiceDestinatario"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FecodiceDestinatario
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.FecodiceDestinatario, true); }
			set	{ SetValue((int)ClienteFieldIndex.FecodiceDestinatario, value); }
		}

		/// <summary> The FepecDestinatario property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."FEPecDestinatario"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FepecDestinatario
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.FepecDestinatario, true); }
			set	{ SetValue((int)ClienteFieldIndex.FepecDestinatario, value); }
		}

		/// <summary> The Iban property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IBAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Iban
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Iban, true); }
			set	{ SetValue((int)ClienteFieldIndex.Iban, value); }
		}

		/// <summary> The IdaliquotaIva property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDAliquotaIva"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdaliquotaIva
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClienteFieldIndex.IdaliquotaIva, false); }
			set	{ SetValue((int)ClienteFieldIndex.IdaliquotaIva, value); }
		}

		/// <summary> The Idcliente property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Idcliente
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.Idcliente, true); }
			set	{ SetValue((int)ClienteFieldIndex.Idcliente, value); }
		}

		/// <summary> The IdgruppoCliente property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDGruppoCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdgruppoCliente
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClienteFieldIndex.IdgruppoCliente, false); }
			set	{ SetValue((int)ClienteFieldIndex.IdgruppoCliente, value); }
		}

		/// <summary> The IdmodalitaInvioFattura property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDModalitaInvioFattura"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdmodalitaInvioFattura
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClienteFieldIndex.IdmodalitaInvioFattura, false); }
			set	{ SetValue((int)ClienteFieldIndex.IdmodalitaInvioFattura, value); }
		}

		/// <summary> The IdmodalitaPagPreferita property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDModalitaPagPreferita"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdmodalitaPagPreferita
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClienteFieldIndex.IdmodalitaPagPreferita, false); }
			set	{ SetValue((int)ClienteFieldIndex.IdmodalitaPagPreferita, value); }
		}

		/// <summary> The IdstatoSolvenzaCliente property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDStatoSolvenzaCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IdstatoSolvenzaCliente
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.IdstatoSolvenzaCliente, true); }
			set	{ SetValue((int)ClienteFieldIndex.IdstatoSolvenzaCliente, value); }
		}

		/// <summary> The IdtempiPagamento property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDTempiPagamento"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdtempiPagamento
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClienteFieldIndex.IdtempiPagamento, false); }
			set	{ SetValue((int)ClienteFieldIndex.IdtempiPagamento, value); }
		}

		/// <summary> The IdtipoArrotondamento property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDTipoArrotondamento"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IdtipoArrotondamento
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.IdtipoArrotondamento, true); }
			set	{ SetValue((int)ClienteFieldIndex.IdtipoArrotondamento, value); }
		}

		/// <summary> The IdtipoCliente property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDTipoCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IdtipoCliente
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.IdtipoCliente, true); }
			set	{ SetValue((int)ClienteFieldIndex.IdtipoCliente, value); }
		}

		/// <summary> The IdtipoDocumentoIdentita property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."IDTipoDocumentoIdentita"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdtipoDocumentoIdentita
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClienteFieldIndex.IdtipoDocumentoIdentita, false); }
			set	{ SetValue((int)ClienteFieldIndex.IdtipoDocumentoIdentita, value); }
		}

		/// <summary> The Indirizzo property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Indirizzo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Indirizzo
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Indirizzo, true); }
			set	{ SetValue((int)ClienteFieldIndex.Indirizzo, value); }
		}

		/// <summary> The InsegnaCommerciale property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."InsegnaCommerciale"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 180<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InsegnaCommerciale
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.InsegnaCommerciale, true); }
			set	{ SetValue((int)ClienteFieldIndex.InsegnaCommerciale, value); }
		}

		/// <summary> The LuogoNascita property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."LuogoNascita"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LuogoNascita
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.LuogoNascita, true); }
			set	{ SetValue((int)ClienteFieldIndex.LuogoNascita, value); }
		}

		/// <summary> The MediaGgpagamentoFattura property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."MediaGGPagamentoFattura"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MediaGgpagamentoFattura
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.MediaGgpagamentoFattura, true); }
			set	{ SetValue((int)ClienteFieldIndex.MediaGgpagamentoFattura, value); }
		}

		/// <summary> The NazioneIso property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."NazioneISO"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NazioneIso
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.NazioneIso, true); }
			set	{ SetValue((int)ClienteFieldIndex.NazioneIso, value); }
		}

		/// <summary> The Nome property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Nome"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Nome
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Nome, true); }
			set	{ SetValue((int)ClienteFieldIndex.Nome, value); }
		}

		/// <summary> The Note property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Note"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Note
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Note, true); }
			set	{ SetValue((int)ClienteFieldIndex.Note, value); }
		}

		/// <summary> The NrFattureEmesse property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."NrFattureEmesse"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NrFattureEmesse
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.NrFattureEmesse, true); }
			set	{ SetValue((int)ClienteFieldIndex.NrFattureEmesse, value); }
		}

		/// <summary> The NrFattureNonPagate property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."NrFattureNonPagate"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NrFattureNonPagate
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.NrFattureNonPagate, true); }
			set	{ SetValue((int)ClienteFieldIndex.NrFattureNonPagate, value); }
		}

		/// <summary> The NumeroCivico property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."NumeroCivico"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NumeroCivico
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.NumeroCivico, true); }
			set	{ SetValue((int)ClienteFieldIndex.NumeroCivico, value); }
		}

		/// <summary> The NumSollecitiInviati property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."NumSollecitiInviati"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NumSollecitiInviati
		{
			get { return (System.Int32)GetValue((int)ClienteFieldIndex.NumSollecitiInviati, true); }
			set	{ SetValue((int)ClienteFieldIndex.NumSollecitiInviati, value); }
		}

		/// <summary> The Password property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Password"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Password, true); }
			set	{ SetValue((int)ClienteFieldIndex.Password, value); }
		}

		/// <summary> The Pec property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."PEC"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 60<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pec
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Pec, true); }
			set	{ SetValue((int)ClienteFieldIndex.Pec, value); }
		}

		/// <summary> The Piva property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."PIVA"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Piva
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Piva, true); }
			set	{ SetValue((int)ClienteFieldIndex.Piva, value); }
		}

		/// <summary> The Provincia property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Provincia"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Provincia
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Provincia, true); }
			set	{ SetValue((int)ClienteFieldIndex.Provincia, value); }
		}

		/// <summary> The ProvinciaNascita property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."ProvinciaNascita"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProvinciaNascita
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.ProvinciaNascita, true); }
			set	{ SetValue((int)ClienteFieldIndex.ProvinciaNascita, value); }
		}

		/// <summary> The RecapitoTelefonico property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."RecapitoTelefonico"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecapitoTelefonico
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.RecapitoTelefonico, true); }
			set	{ SetValue((int)ClienteFieldIndex.RecapitoTelefonico, value); }
		}

		/// <summary> The SaldoContabile property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."SaldoContabile"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal SaldoContabile
		{
			get { return (System.Decimal)GetValue((int)ClienteFieldIndex.SaldoContabile, true); }
			set	{ SetValue((int)ClienteFieldIndex.SaldoContabile, value); }
		}

		/// <summary> The UltimoSollecito property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."UltimoSollecito"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UltimoSollecito
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClienteFieldIndex.UltimoSollecito, false); }
			set	{ SetValue((int)ClienteFieldIndex.UltimoSollecito, value); }
		}

		/// <summary> The Username property of the Entity Cliente<br/><br/></summary>
		/// <remarks>Mapped on  table field: "cliente"."Username"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Username
		{
			get { return (System.String)GetValue((int)ClienteFieldIndex.Username, true); }
			set	{ SetValue((int)ClienteFieldIndex.Username, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ContrattoEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ContrattoEntity))]
		public virtual EntityCollection<ContrattoEntity> Contrattos
		{
			get { return GetOrCreateEntityCollection<ContrattoEntity, ContrattoEntityFactory>("Cliente", true, false, ref _contrattos);	}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the WTBILL.RadiusOrchestrator.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)WTBILL.RadiusOrchestrator.Data.EntityType.ClienteEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
