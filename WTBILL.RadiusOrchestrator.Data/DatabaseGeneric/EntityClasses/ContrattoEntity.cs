﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Contratto'.<br/><br/></summary>
	[Serializable]
	public partial class ContrattoEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<ServizioxcontrattoEntity> _servizioxcontrattos;
		private ClienteEntity _cliente;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Cliente</summary>
			public static readonly string Cliente = "Cliente";
			/// <summary>Member name Servizioxcontrattos</summary>
			public static readonly string Servizioxcontrattos = "Servizioxcontrattos";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ContrattoEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public ContrattoEntity():base("ContrattoEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ContrattoEntity(IEntityFields2 fields):base("ContrattoEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ContrattoEntity</param>
		public ContrattoEntity(IValidator validator):base("ContrattoEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="idcontratto">PK value for Contratto which data should be fetched into this Contratto object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ContrattoEntity(System.Int32 idcontratto):base("ContrattoEntity")
		{
			InitClassEmpty(null, null);
			this.Idcontratto = idcontratto;
		}

		/// <summary> CTor</summary>
		/// <param name="idcontratto">PK value for Contratto which data should be fetched into this Contratto object</param>
		/// <param name="validator">The custom validator object for this ContrattoEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ContrattoEntity(System.Int32 idcontratto, IValidator validator):base("ContrattoEntity")
		{
			InitClassEmpty(validator, null);
			this.Idcontratto = idcontratto;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ContrattoEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_servizioxcontrattos = (EntityCollection<ServizioxcontrattoEntity>)info.GetValue("_servizioxcontrattos", typeof(EntityCollection<ServizioxcontrattoEntity>));
				_cliente = (ClienteEntity)info.GetValue("_cliente", typeof(ClienteEntity));
				if(_cliente!=null)
				{
					_cliente.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ContrattoFieldIndex)fieldIndex)
			{
				case ContrattoFieldIndex.Idcliente:
					DesetupSyncCliente(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Cliente":
					this.Cliente = (ClienteEntity)entity;
					break;
				case "Servizioxcontrattos":
					this.Servizioxcontrattos.Add((ServizioxcontrattoEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Cliente":
					toReturn.Add(Relations.ClienteEntityUsingIdcliente);
					break;
				case "Servizioxcontrattos":
					toReturn.Add(Relations.ServizioxcontrattoEntityUsingIdcontratto);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Cliente":
					SetupSyncCliente(relatedEntity);
					break;
				case "Servizioxcontrattos":
					this.Servizioxcontrattos.Add((ServizioxcontrattoEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Cliente":
					DesetupSyncCliente(false, true);
					break;
				case "Servizioxcontrattos":
					this.PerformRelatedEntityRemoval(this.Servizioxcontrattos, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_cliente!=null)
			{
				toReturn.Add(_cliente);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Servizioxcontrattos);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_servizioxcontrattos", ((_servizioxcontrattos!=null) && (_servizioxcontrattos.Count>0) && !this.MarkedForDeletion)?_servizioxcontrattos:null);
				info.AddValue("_cliente", (!this.MarkedForDeletion?_cliente:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ContrattoRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Servizioxcontratto' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServizioxcontrattos()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ServizioxcontrattoFields.Idcontratto, null, ComparisonOperator.Equal, this.Idcontratto));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Cliente' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCliente()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClienteFields.Idcliente, null, ComparisonOperator.Equal, this.Idcliente));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ContrattoEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._servizioxcontrattos);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._servizioxcontrattos = (EntityCollection<ServizioxcontrattoEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._servizioxcontrattos != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ServizioxcontrattoEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ServizioxcontrattoEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Cliente", _cliente);
			toReturn.Add("Servizioxcontrattos", _servizioxcontrattos);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CapInstallazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cartaceo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CittaInstallazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoordinateInst", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataAttivazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataCessazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataFirma", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataScadenzaDurataContrattuale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescrizioneAggiuntiva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FecodiceCig", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FecodiceCup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FedataContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FenumeroContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdcicloPagamento", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idcliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idcontratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IddurataContrattule", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdmodalitaPagamentoAttivazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idpartner", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdstatoContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IndirizzoInstallazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoteCliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoteInterne", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotePartner", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Numero", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProvinciaInstallazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhsaziendaClienteFinale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whscap", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhscfclienteFinale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whscitta", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whscivico", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhscognomeClienteFinale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whsindirizzo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhsnomeClienteFinale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whsnote", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhspartitaIvaclienteFinale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whsprovincia", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhsrecapitoReferente1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhsrecapitoReferente2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Whstoponimo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cliente</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCliente(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cliente, new PropertyChangedEventHandler( OnClientePropertyChanged ), "Cliente", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticContrattoRelations.ClienteEntityUsingIdclienteStatic, true, signalRelatedEntity, "Contrattos", resetFKFields, new int[] { (int)ContrattoFieldIndex.Idcliente } );
			_cliente = null;
		}

		/// <summary> setups the sync logic for member _cliente</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCliente(IEntityCore relatedEntity)
		{
			if(_cliente!=relatedEntity)
			{
				DesetupSyncCliente(true, true);
				_cliente = (ClienteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cliente, new PropertyChangedEventHandler( OnClientePropertyChanged ), "Cliente", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticContrattoRelations.ClienteEntityUsingIdclienteStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ContrattoEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ContrattoRelations Relations
		{
			get	{ return new ContrattoRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Servizioxcontratto' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServizioxcontrattos
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<ServizioxcontrattoEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ServizioxcontrattoEntityFactory))), (IEntityRelation)GetRelationsForField("Servizioxcontrattos")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ContrattoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity, 0, null, null, null, null, "Servizioxcontrattos", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Cliente' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCliente
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ClienteEntityFactory))),	(IEntityRelation)GetRelationsForField("Cliente")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ContrattoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.ClienteEntity, 0, null, null, null, null, "Cliente", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CapInstallazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."CapInstallazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CapInstallazione
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.CapInstallazione, true); }
			set	{ SetValue((int)ContrattoFieldIndex.CapInstallazione, value); }
		}

		/// <summary> The Cartaceo property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."Cartaceo"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Cartaceo
		{
			get { return (System.Int16)GetValue((int)ContrattoFieldIndex.Cartaceo, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Cartaceo, value); }
		}

		/// <summary> The CittaInstallazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."CittaInstallazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CittaInstallazione
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.CittaInstallazione, true); }
			set	{ SetValue((int)ContrattoFieldIndex.CittaInstallazione, value); }
		}

		/// <summary> The CoordinateInst property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."CoordinateInst"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CoordinateInst
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.CoordinateInst, true); }
			set	{ SetValue((int)ContrattoFieldIndex.CoordinateInst, value); }
		}

		/// <summary> The DataAttivazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."DataAttivazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataAttivazione
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContrattoFieldIndex.DataAttivazione, false); }
			set	{ SetValue((int)ContrattoFieldIndex.DataAttivazione, value); }
		}

		/// <summary> The DataCessazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."DataCessazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataCessazione
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContrattoFieldIndex.DataCessazione, false); }
			set	{ SetValue((int)ContrattoFieldIndex.DataCessazione, value); }
		}

		/// <summary> The DataFirma property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."DataFirma"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataFirma
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContrattoFieldIndex.DataFirma, false); }
			set	{ SetValue((int)ContrattoFieldIndex.DataFirma, value); }
		}

		/// <summary> The DataScadenzaDurataContrattuale property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."DataScadenzaDurataContrattuale"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataScadenzaDurataContrattuale
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContrattoFieldIndex.DataScadenzaDurataContrattuale, false); }
			set	{ SetValue((int)ContrattoFieldIndex.DataScadenzaDurataContrattuale, value); }
		}

		/// <summary> The DescrizioneAggiuntiva property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."DescrizioneAggiuntiva"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DescrizioneAggiuntiva
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.DescrizioneAggiuntiva, true); }
			set	{ SetValue((int)ContrattoFieldIndex.DescrizioneAggiuntiva, value); }
		}

		/// <summary> The FecodiceCig property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."FECodiceCIG"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FecodiceCig
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.FecodiceCig, true); }
			set	{ SetValue((int)ContrattoFieldIndex.FecodiceCig, value); }
		}

		/// <summary> The FecodiceCup property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."FECodiceCUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FecodiceCup
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.FecodiceCup, true); }
			set	{ SetValue((int)ContrattoFieldIndex.FecodiceCup, value); }
		}

		/// <summary> The FedataContratto property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."FEDataContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FedataContratto
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ContrattoFieldIndex.FedataContratto, false); }
			set	{ SetValue((int)ContrattoFieldIndex.FedataContratto, value); }
		}

		/// <summary> The FenumeroContratto property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."FENumeroContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FenumeroContratto
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.FenumeroContratto, true); }
			set	{ SetValue((int)ContrattoFieldIndex.FenumeroContratto, value); }
		}

		/// <summary> The IdcicloPagamento property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDCicloPagamento"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IdcicloPagamento
		{
			get { return (System.Int32)GetValue((int)ContrattoFieldIndex.IdcicloPagamento, true); }
			set	{ SetValue((int)ContrattoFieldIndex.IdcicloPagamento, value); }
		}

		/// <summary> The Idcliente property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Idcliente
		{
			get { return (System.Int32)GetValue((int)ContrattoFieldIndex.Idcliente, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Idcliente, value); }
		}

		/// <summary> The Idcontratto property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Idcontratto
		{
			get { return (System.Int32)GetValue((int)ContrattoFieldIndex.Idcontratto, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Idcontratto, value); }
		}

		/// <summary> The IddurataContrattule property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDDurataContrattule"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IddurataContrattule
		{
			get { return (Nullable<System.Int32>)GetValue((int)ContrattoFieldIndex.IddurataContrattule, false); }
			set	{ SetValue((int)ContrattoFieldIndex.IddurataContrattule, value); }
		}

		/// <summary> The IdmodalitaPagamentoAttivazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDModalitaPagamentoAttivazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdmodalitaPagamentoAttivazione
		{
			get { return (Nullable<System.Int32>)GetValue((int)ContrattoFieldIndex.IdmodalitaPagamentoAttivazione, false); }
			set	{ SetValue((int)ContrattoFieldIndex.IdmodalitaPagamentoAttivazione, value); }
		}

		/// <summary> The Idpartner property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDPartner"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Idpartner
		{
			get { return (System.Int32)GetValue((int)ContrattoFieldIndex.Idpartner, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Idpartner, value); }
		}

		/// <summary> The IdstatoContratto property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IDStatoContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IdstatoContratto
		{
			get { return (System.Int32)GetValue((int)ContrattoFieldIndex.IdstatoContratto, true); }
			set	{ SetValue((int)ContrattoFieldIndex.IdstatoContratto, value); }
		}

		/// <summary> The IndirizzoInstallazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."IndirizzoInstallazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String IndirizzoInstallazione
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.IndirizzoInstallazione, true); }
			set	{ SetValue((int)ContrattoFieldIndex.IndirizzoInstallazione, value); }
		}

		/// <summary> The NoteCliente property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."NoteCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NoteCliente
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.NoteCliente, true); }
			set	{ SetValue((int)ContrattoFieldIndex.NoteCliente, value); }
		}

		/// <summary> The NoteInterne property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."NoteInterne"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NoteInterne
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.NoteInterne, true); }
			set	{ SetValue((int)ContrattoFieldIndex.NoteInterne, value); }
		}

		/// <summary> The NotePartner property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."NotePartner"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NotePartner
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.NotePartner, true); }
			set	{ SetValue((int)ContrattoFieldIndex.NotePartner, value); }
		}

		/// <summary> The Numero property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."Numero"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Numero
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Numero, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Numero, value); }
		}

		/// <summary> The ProvinciaInstallazione property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."ProvinciaInstallazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProvinciaInstallazione
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.ProvinciaInstallazione, true); }
			set	{ SetValue((int)ContrattoFieldIndex.ProvinciaInstallazione, value); }
		}

		/// <summary> The WhsaziendaClienteFinale property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSAziendaClienteFinale"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhsaziendaClienteFinale
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhsaziendaClienteFinale, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhsaziendaClienteFinale, value); }
		}

		/// <summary> The Whscap property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSCap"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whscap
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whscap, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whscap, value); }
		}

		/// <summary> The WhscfclienteFinale property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSCFClienteFinale"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhscfclienteFinale
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhscfclienteFinale, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhscfclienteFinale, value); }
		}

		/// <summary> The Whscitta property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSCitta"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whscitta
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whscitta, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whscitta, value); }
		}

		/// <summary> The Whscivico property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSCivico"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whscivico
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whscivico, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whscivico, value); }
		}

		/// <summary> The WhscognomeClienteFinale property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSCognomeClienteFinale"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhscognomeClienteFinale
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhscognomeClienteFinale, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhscognomeClienteFinale, value); }
		}

		/// <summary> The Whsindirizzo property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSIndirizzo"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whsindirizzo
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whsindirizzo, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whsindirizzo, value); }
		}

		/// <summary> The WhsnomeClienteFinale property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSNomeClienteFinale"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhsnomeClienteFinale
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhsnomeClienteFinale, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhsnomeClienteFinale, value); }
		}

		/// <summary> The Whsnote property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSNote"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whsnote
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whsnote, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whsnote, value); }
		}

		/// <summary> The WhspartitaIvaclienteFinale property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSPartitaIVAClienteFinale"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhspartitaIvaclienteFinale
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhspartitaIvaclienteFinale, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhspartitaIvaclienteFinale, value); }
		}

		/// <summary> The Whsprovincia property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSProvincia"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whsprovincia
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whsprovincia, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whsprovincia, value); }
		}

		/// <summary> The WhsrecapitoReferente1 property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSRecapitoReferente1"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhsrecapitoReferente1
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhsrecapitoReferente1, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhsrecapitoReferente1, value); }
		}

		/// <summary> The WhsrecapitoReferente2 property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSRecapitoReferente2"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WhsrecapitoReferente2
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.WhsrecapitoReferente2, true); }
			set	{ SetValue((int)ContrattoFieldIndex.WhsrecapitoReferente2, value); }
		}

		/// <summary> The Whstoponimo property of the Entity Contratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "contratto"."WHSToponimo"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Whstoponimo
		{
			get { return (System.String)GetValue((int)ContrattoFieldIndex.Whstoponimo, true); }
			set	{ SetValue((int)ContrattoFieldIndex.Whstoponimo, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ServizioxcontrattoEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServizioxcontrattoEntity))]
		public virtual EntityCollection<ServizioxcontrattoEntity> Servizioxcontrattos
		{
			get { return GetOrCreateEntityCollection<ServizioxcontrattoEntity, ServizioxcontrattoEntityFactory>("Contratto", true, false, ref _servizioxcontrattos);	}
		}

		/// <summary> Gets / sets related entity of type 'ClienteEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ClienteEntity Cliente
		{
			get	{ return _cliente; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCliente(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Contrattos", "Cliente", _cliente, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the WTBILL.RadiusOrchestrator.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)WTBILL.RadiusOrchestrator.Data.EntityType.ContrattoEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
