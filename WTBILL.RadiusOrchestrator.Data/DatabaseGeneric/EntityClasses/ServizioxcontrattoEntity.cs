﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Servizioxcontratto'.<br/><br/></summary>
	[Serializable]
	public partial class ServizioxcontrattoEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<AttributoradiuEntity> _attributoradius;
		private ContrattoEntity _contratto;
		private GrupporadiuEntity _grupporadiu;
		private ProdottoEntity _prodotto;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Contratto</summary>
			public static readonly string Contratto = "Contratto";
			/// <summary>Member name Grupporadiu</summary>
			public static readonly string Grupporadiu = "Grupporadiu";
			/// <summary>Member name Prodotto</summary>
			public static readonly string Prodotto = "Prodotto";
			/// <summary>Member name Attributoradius</summary>
			public static readonly string Attributoradius = "Attributoradius";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ServizioxcontrattoEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public ServizioxcontrattoEntity():base("ServizioxcontrattoEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ServizioxcontrattoEntity(IEntityFields2 fields):base("ServizioxcontrattoEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ServizioxcontrattoEntity</param>
		public ServizioxcontrattoEntity(IValidator validator):base("ServizioxcontrattoEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="idservizioXcontratto">PK value for Servizioxcontratto which data should be fetched into this Servizioxcontratto object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ServizioxcontrattoEntity(System.Int32 idservizioXcontratto):base("ServizioxcontrattoEntity")
		{
			InitClassEmpty(null, null);
			this.IdservizioXcontratto = idservizioXcontratto;
		}

		/// <summary> CTor</summary>
		/// <param name="idservizioXcontratto">PK value for Servizioxcontratto which data should be fetched into this Servizioxcontratto object</param>
		/// <param name="validator">The custom validator object for this ServizioxcontrattoEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ServizioxcontrattoEntity(System.Int32 idservizioXcontratto, IValidator validator):base("ServizioxcontrattoEntity")
		{
			InitClassEmpty(validator, null);
			this.IdservizioXcontratto = idservizioXcontratto;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ServizioxcontrattoEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_attributoradius = (EntityCollection<AttributoradiuEntity>)info.GetValue("_attributoradius", typeof(EntityCollection<AttributoradiuEntity>));
				_contratto = (ContrattoEntity)info.GetValue("_contratto", typeof(ContrattoEntity));
				if(_contratto!=null)
				{
					_contratto.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_grupporadiu = (GrupporadiuEntity)info.GetValue("_grupporadiu", typeof(GrupporadiuEntity));
				if(_grupporadiu!=null)
				{
					_grupporadiu.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_prodotto = (ProdottoEntity)info.GetValue("_prodotto", typeof(ProdottoEntity));
				if(_prodotto!=null)
				{
					_prodotto.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ServizioxcontrattoFieldIndex)fieldIndex)
			{
				case ServizioxcontrattoFieldIndex.Idcontratto:
					DesetupSyncContratto(true, false);
					break;
				case ServizioxcontrattoFieldIndex.IdgruppoRadius:
					DesetupSyncGrupporadiu(true, false);
					break;
				case ServizioxcontrattoFieldIndex.Idprodotto:
					DesetupSyncProdotto(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contratto":
					this.Contratto = (ContrattoEntity)entity;
					break;
				case "Grupporadiu":
					this.Grupporadiu = (GrupporadiuEntity)entity;
					break;
				case "Prodotto":
					this.Prodotto = (ProdottoEntity)entity;
					break;
				case "Attributoradius":
					this.Attributoradius.Add((AttributoradiuEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contratto":
					toReturn.Add(Relations.ContrattoEntityUsingIdcontratto);
					break;
				case "Grupporadiu":
					toReturn.Add(Relations.GrupporadiuEntityUsingIdgruppoRadius);
					break;
				case "Prodotto":
					toReturn.Add(Relations.ProdottoEntityUsingIdprodotto);
					break;
				case "Attributoradius":
					toReturn.Add(Relations.AttributoradiuEntityUsingIdservizioXcontratto);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contratto":
					SetupSyncContratto(relatedEntity);
					break;
				case "Grupporadiu":
					SetupSyncGrupporadiu(relatedEntity);
					break;
				case "Prodotto":
					SetupSyncProdotto(relatedEntity);
					break;
				case "Attributoradius":
					this.Attributoradius.Add((AttributoradiuEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contratto":
					DesetupSyncContratto(false, true);
					break;
				case "Grupporadiu":
					DesetupSyncGrupporadiu(false, true);
					break;
				case "Prodotto":
					DesetupSyncProdotto(false, true);
					break;
				case "Attributoradius":
					this.PerformRelatedEntityRemoval(this.Attributoradius, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_contratto!=null)
			{
				toReturn.Add(_contratto);
			}
			if(_grupporadiu!=null)
			{
				toReturn.Add(_grupporadiu);
			}
			if(_prodotto!=null)
			{
				toReturn.Add(_prodotto);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Attributoradius);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_attributoradius", ((_attributoradius!=null) && (_attributoradius.Count>0) && !this.MarkedForDeletion)?_attributoradius:null);
				info.AddValue("_contratto", (!this.MarkedForDeletion?_contratto:null));
				info.AddValue("_grupporadiu", (!this.MarkedForDeletion?_grupporadiu:null));
				info.AddValue("_prodotto", (!this.MarkedForDeletion?_prodotto:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ServizioxcontrattoRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Attributoradiu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAttributoradius()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AttributoradiuFields.IdservizioXcontratto, null, ComparisonOperator.Equal, this.IdservizioXcontratto));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Contratto' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoContratto()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ContrattoFields.Idcontratto, null, ComparisonOperator.Equal, this.Idcontratto));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Grupporadiu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoGrupporadiu()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(GrupporadiuFields.IdgruppoRadius, null, ComparisonOperator.Equal, this.IdgruppoRadius));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Prodotto' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProdotto()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ProdottoFields.Idprodotto, null, ComparisonOperator.Equal, this.Idprodotto));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ServizioxcontrattoEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._attributoradius);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._attributoradius = (EntityCollection<AttributoradiuEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._attributoradius != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<AttributoradiuEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributoradiuEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Contratto", _contratto);
			toReturn.Add("Grupporadiu", _grupporadiu);
			toReturn.Add("Prodotto", _prodotto);
			toReturn.Add("Attributoradius", _attributoradius);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodiceMigrazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodiceRisorsa", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodiceStatoProvisioning", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodiceStatoRadius", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Dac", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataAttesaCessazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataDecorrenzaCessazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataDecorrenzaSospensione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataInstallazioneApparati", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataProvisioningEffettiva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRichiestaCessazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataUltimoAggiornamentoSegnali", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescrizioneAggiuntiva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FatturatoFinoAl", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FecodiceCig", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FecodiceCup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FedataContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FenumeroContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileSchedaTecnica", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdaliquotaIva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idbts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdcicloImmissioneInFatturazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idcontratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdentificativoAccesso", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Identita", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdgruppoRadius", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idinstallatore", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdproceduraProvisioning", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idprodotto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdrichiestaCessazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdservizioXcontratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idsettore", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdstatoServizioXcontratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdtipoSospensione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idzona", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Latitudine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Longitudine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Macinstallatore", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MandareRidBanca", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MultihomeAccessoAttivo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoteCliente", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoteInstallatore", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoteInterne", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotePartner", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordAccesso", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PresospRadiusGroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PresospRadiusServiceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Prezzo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SegnaleCh0Rx", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SegnaleCh0Tx", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SegnaleCh1Rx", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SegnaleCh1Tx", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _contratto</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContratto(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contratto, new PropertyChangedEventHandler( OnContrattoPropertyChanged ), "Contratto", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticServizioxcontrattoRelations.ContrattoEntityUsingIdcontrattoStatic, true, signalRelatedEntity, "Servizioxcontrattos", resetFKFields, new int[] { (int)ServizioxcontrattoFieldIndex.Idcontratto } );
			_contratto = null;
		}

		/// <summary> setups the sync logic for member _contratto</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContratto(IEntityCore relatedEntity)
		{
			if(_contratto!=relatedEntity)
			{
				DesetupSyncContratto(true, true);
				_contratto = (ContrattoEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contratto, new PropertyChangedEventHandler( OnContrattoPropertyChanged ), "Contratto", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticServizioxcontrattoRelations.ContrattoEntityUsingIdcontrattoStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContrattoPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _grupporadiu</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGrupporadiu(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _grupporadiu, new PropertyChangedEventHandler( OnGrupporadiuPropertyChanged ), "Grupporadiu", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticServizioxcontrattoRelations.GrupporadiuEntityUsingIdgruppoRadiusStatic, true, signalRelatedEntity, "Servizioxcontrattos", resetFKFields, new int[] { (int)ServizioxcontrattoFieldIndex.IdgruppoRadius } );
			_grupporadiu = null;
		}

		/// <summary> setups the sync logic for member _grupporadiu</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGrupporadiu(IEntityCore relatedEntity)
		{
			if(_grupporadiu!=relatedEntity)
			{
				DesetupSyncGrupporadiu(true, true);
				_grupporadiu = (GrupporadiuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _grupporadiu, new PropertyChangedEventHandler( OnGrupporadiuPropertyChanged ), "Grupporadiu", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticServizioxcontrattoRelations.GrupporadiuEntityUsingIdgruppoRadiusStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGrupporadiuPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _prodotto</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProdotto(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _prodotto, new PropertyChangedEventHandler( OnProdottoPropertyChanged ), "Prodotto", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticServizioxcontrattoRelations.ProdottoEntityUsingIdprodottoStatic, true, signalRelatedEntity, "Servizioxcontrattos", resetFKFields, new int[] { (int)ServizioxcontrattoFieldIndex.Idprodotto } );
			_prodotto = null;
		}

		/// <summary> setups the sync logic for member _prodotto</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProdotto(IEntityCore relatedEntity)
		{
			if(_prodotto!=relatedEntity)
			{
				DesetupSyncProdotto(true, true);
				_prodotto = (ProdottoEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _prodotto, new PropertyChangedEventHandler( OnProdottoPropertyChanged ), "Prodotto", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticServizioxcontrattoRelations.ProdottoEntityUsingIdprodottoStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProdottoPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ServizioxcontrattoEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ServizioxcontrattoRelations Relations
		{
			get	{ return new ServizioxcontrattoRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Attributoradiu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAttributoradius
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<AttributoradiuEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributoradiuEntityFactory))), (IEntityRelation)GetRelationsForField("Attributoradius")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.AttributoradiuEntity, 0, null, null, null, null, "Attributoradius", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Contratto' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathContratto
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ContrattoEntityFactory))),	(IEntityRelation)GetRelationsForField("Contratto")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.ContrattoEntity, 0, null, null, null, null, "Contratto", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Grupporadiu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathGrupporadiu
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(GrupporadiuEntityFactory))),	(IEntityRelation)GetRelationsForField("Grupporadiu")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.GrupporadiuEntity, 0, null, null, null, null, "Grupporadiu", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Prodotto' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProdotto
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ProdottoEntityFactory))),	(IEntityRelation)GetRelationsForField("Prodotto")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.ProdottoEntity, 0, null, null, null, null, "Prodotto", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CodiceMigrazione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."CodiceMigrazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodiceMigrazione
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.CodiceMigrazione, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.CodiceMigrazione, value); }
		}

		/// <summary> The CodiceRisorsa property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."CodiceRisorsa"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodiceRisorsa
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.CodiceRisorsa, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.CodiceRisorsa, value); }
		}

		/// <summary> The CodiceStatoProvisioning property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."CodiceStatoProvisioning"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CodiceStatoProvisioning
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.CodiceStatoProvisioning, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.CodiceStatoProvisioning, value); }
		}

		/// <summary> The CodiceStatoRadius property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."CodiceStatoRadius"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CodiceStatoRadius
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.CodiceStatoRadius, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.CodiceStatoRadius, value); }
		}

		/// <summary> The Dac property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DAC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Dac
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.Dac, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Dac, value); }
		}

		/// <summary> The DataAttesaCessazione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataAttesaCessazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataAttesaCessazione
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataAttesaCessazione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataAttesaCessazione, value); }
		}

		/// <summary> The DataDecorrenzaCessazione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataDecorrenzaCessazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataDecorrenzaCessazione
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataDecorrenzaCessazione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataDecorrenzaCessazione, value); }
		}

		/// <summary> The DataDecorrenzaSospensione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataDecorrenzaSospensione"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataDecorrenzaSospensione
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataDecorrenzaSospensione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataDecorrenzaSospensione, value); }
		}

		/// <summary> The DataInstallazioneApparati property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataInstallazioneApparati"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataInstallazioneApparati
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataInstallazioneApparati, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataInstallazioneApparati, value); }
		}

		/// <summary> The DataProvisioningEffettiva property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataProvisioningEffettiva"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataProvisioningEffettiva
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataProvisioningEffettiva, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataProvisioningEffettiva, value); }
		}

		/// <summary> The DataRichiestaCessazione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataRichiestaCessazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRichiestaCessazione
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataRichiestaCessazione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataRichiestaCessazione, value); }
		}

		/// <summary> The DataUltimoAggiornamentoSegnali property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DataUltimoAggiornamentoSegnali"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataUltimoAggiornamentoSegnali
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.DataUltimoAggiornamentoSegnali, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DataUltimoAggiornamentoSegnali, value); }
		}

		/// <summary> The DescrizioneAggiuntiva property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."DescrizioneAggiuntiva"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DescrizioneAggiuntiva
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.DescrizioneAggiuntiva, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.DescrizioneAggiuntiva, value); }
		}

		/// <summary> The FatturatoFinoAl property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."FatturatoFinoAl"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FatturatoFinoAl
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.FatturatoFinoAl, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.FatturatoFinoAl, value); }
		}

		/// <summary> The FecodiceCig property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."FECodiceCIG"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FecodiceCig
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.FecodiceCig, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.FecodiceCig, value); }
		}

		/// <summary> The FecodiceCup property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."FECodiceCUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FecodiceCup
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.FecodiceCup, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.FecodiceCup, value); }
		}

		/// <summary> The FedataContratto property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."FEDataContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FedataContratto
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServizioxcontrattoFieldIndex.FedataContratto, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.FedataContratto, value); }
		}

		/// <summary> The FenumeroContratto property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."FENumeroContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FenumeroContratto
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.FenumeroContratto, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.FenumeroContratto, value); }
		}

		/// <summary> The FileSchedaTecnica property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."FileSchedaTecnica"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FileSchedaTecnica
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.FileSchedaTecnica, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.FileSchedaTecnica, value); }
		}

		/// <summary> The IdaliquotaIva property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDAliquotaIVA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdaliquotaIva
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdaliquotaIva, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdaliquotaIva, value); }
		}

		/// <summary> The Idbts property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDBts"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Idbts
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.Idbts, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Idbts, value); }
		}

		/// <summary> The IdcicloImmissioneInFatturazione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDCicloImmissioneInFatturazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdcicloImmissioneInFatturazione
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdcicloImmissioneInFatturazione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdcicloImmissioneInFatturazione, value); }
		}

		/// <summary> The Idcontratto property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Idcontratto
		{
			get { return (System.Int32)GetValue((int)ServizioxcontrattoFieldIndex.Idcontratto, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Idcontratto, value); }
		}

		/// <summary> The IdentificativoAccesso property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IdentificativoAccesso"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 60<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String IdentificativoAccesso
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.IdentificativoAccesso, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdentificativoAccesso, value); }
		}

		/// <summary> The Identita property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."Identita"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Identita
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.Identita, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Identita, value); }
		}

		/// <summary> The IdgruppoRadius property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDGruppoRadius"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdgruppoRadius
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdgruppoRadius, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdgruppoRadius, value); }
		}

		/// <summary> The Idinstallatore property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDInstallatore"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Idinstallatore
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.Idinstallatore, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Idinstallatore, value); }
		}

		/// <summary> The IdproceduraProvisioning property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDProceduraProvisioning"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdproceduraProvisioning
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdproceduraProvisioning, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdproceduraProvisioning, value); }
		}

		/// <summary> The Idprodotto property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDProdotto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Idprodotto
		{
			get { return (System.Int32)GetValue((int)ServizioxcontrattoFieldIndex.Idprodotto, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Idprodotto, value); }
		}

		/// <summary> The IdrichiestaCessazione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDRichiestaCessazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdrichiestaCessazione
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdrichiestaCessazione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdrichiestaCessazione, value); }
		}

		/// <summary> The IdservizioXcontratto property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDServizioXContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 IdservizioXcontratto
		{
			get { return (System.Int32)GetValue((int)ServizioxcontrattoFieldIndex.IdservizioXcontratto, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdservizioXcontratto, value); }
		}

		/// <summary> The Idsettore property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDSettore"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Idsettore
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.Idsettore, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Idsettore, value); }
		}

		/// <summary> The IdstatoServizioXcontratto property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDStatoServizioXContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdstatoServizioXcontratto
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdstatoServizioXcontratto, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdstatoServizioXcontratto, value); }
		}

		/// <summary> The IdtipoSospensione property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDTipoSospensione"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdtipoSospensione
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.IdtipoSospensione, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.IdtipoSospensione, value); }
		}

		/// <summary> The Idzona property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."IDZona"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Idzona
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.Idzona, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Idzona, value); }
		}

		/// <summary> The Latitudine property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."Latitudine"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 6, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> Latitudine
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ServizioxcontrattoFieldIndex.Latitudine, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Latitudine, value); }
		}

		/// <summary> The Longitudine property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."Longitudine"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 6, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> Longitudine
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ServizioxcontrattoFieldIndex.Longitudine, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Longitudine, value); }
		}

		/// <summary> The Macinstallatore property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."MACInstallatore"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Macinstallatore
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.Macinstallatore, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Macinstallatore, value); }
		}

		/// <summary> The MandareRidBanca property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."MandareRidBanca"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 MandareRidBanca
		{
			get { return (System.Int16)GetValue((int)ServizioxcontrattoFieldIndex.MandareRidBanca, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.MandareRidBanca, value); }
		}

		/// <summary> The MultihomeAccessoAttivo property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."MultihomeAccessoAttivo"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 MultihomeAccessoAttivo
		{
			get { return (System.Int16)GetValue((int)ServizioxcontrattoFieldIndex.MultihomeAccessoAttivo, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.MultihomeAccessoAttivo, value); }
		}

		/// <summary> The NoteCliente property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."NoteCliente"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NoteCliente
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.NoteCliente, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.NoteCliente, value); }
		}

		/// <summary> The NoteInstallatore property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."NoteInstallatore"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NoteInstallatore
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.NoteInstallatore, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.NoteInstallatore, value); }
		}

		/// <summary> The NoteInterne property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."NoteInterne"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NoteInterne
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.NoteInterne, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.NoteInterne, value); }
		}

		/// <summary> The NotePartner property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."NotePartner"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 65535<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NotePartner
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.NotePartner, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.NotePartner, value); }
		}

		/// <summary> The PasswordAccesso property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."PasswordAccesso"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 60<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PasswordAccesso
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.PasswordAccesso, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.PasswordAccesso, value); }
		}

		/// <summary> The PresospRadiusGroupName property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."PresospRadiusGroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PresospRadiusGroupName
		{
			get { return (System.String)GetValue((int)ServizioxcontrattoFieldIndex.PresospRadiusGroupName, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.PresospRadiusGroupName, value); }
		}

		/// <summary> The PresospRadiusServiceId property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."PresospRadiusServiceID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PresospRadiusServiceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.PresospRadiusServiceId, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.PresospRadiusServiceId, value); }
		}

		/// <summary> The Prezzo property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."Prezzo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Prezzo
		{
			get { return (System.Decimal)GetValue((int)ServizioxcontrattoFieldIndex.Prezzo, true); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.Prezzo, value); }
		}

		/// <summary> The SegnaleCh0Rx property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."SegnaleCh0Rx"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SegnaleCh0Rx
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh0Rx, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh0Rx, value); }
		}

		/// <summary> The SegnaleCh0Tx property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."SegnaleCh0Tx"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SegnaleCh0Tx
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh0Tx, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh0Tx, value); }
		}

		/// <summary> The SegnaleCh1Rx property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."SegnaleCh1Rx"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SegnaleCh1Rx
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh1Rx, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh1Rx, value); }
		}

		/// <summary> The SegnaleCh1Tx property of the Entity Servizioxcontratto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "servizioxcontratto"."SegnaleCh1Tx"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SegnaleCh1Tx
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh1Tx, false); }
			set	{ SetValue((int)ServizioxcontrattoFieldIndex.SegnaleCh1Tx, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'AttributoradiuEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AttributoradiuEntity))]
		public virtual EntityCollection<AttributoradiuEntity> Attributoradius
		{
			get { return GetOrCreateEntityCollection<AttributoradiuEntity, AttributoradiuEntityFactory>("Servizioxcontratto", true, false, ref _attributoradius);	}
		}

		/// <summary> Gets / sets related entity of type 'ContrattoEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ContrattoEntity Contratto
		{
			get	{ return _contratto; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncContratto(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Servizioxcontrattos", "Contratto", _contratto, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'GrupporadiuEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual GrupporadiuEntity Grupporadiu
		{
			get	{ return _grupporadiu; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncGrupporadiu(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Servizioxcontrattos", "Grupporadiu", _grupporadiu, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'ProdottoEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProdottoEntity Prodotto
		{
			get	{ return _prodotto; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncProdotto(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Servizioxcontrattos", "Prodotto", _prodotto, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the WTBILL.RadiusOrchestrator.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
