﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using WTBILL.RadiusOrchestrator.Data;
using WTBILL.RadiusOrchestrator.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Prodotto'.<br/><br/></summary>
	[Serializable]
	public partial class ProdottoEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<ServizioxcontrattoEntity> _servizioxcontrattos;
		private CategoriaprodottoEntity _categoriaprodotto;
		private GrupporadiuEntity _grupporadiu;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Categoriaprodotto</summary>
			public static readonly string Categoriaprodotto = "Categoriaprodotto";
			/// <summary>Member name Grupporadiu</summary>
			public static readonly string Grupporadiu = "Grupporadiu";
			/// <summary>Member name Servizioxcontrattos</summary>
			public static readonly string Servizioxcontrattos = "Servizioxcontrattos";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProdottoEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public ProdottoEntity():base("ProdottoEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProdottoEntity(IEntityFields2 fields):base("ProdottoEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProdottoEntity</param>
		public ProdottoEntity(IValidator validator):base("ProdottoEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="idprodotto">PK value for Prodotto which data should be fetched into this Prodotto object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ProdottoEntity(System.Int32 idprodotto):base("ProdottoEntity")
		{
			InitClassEmpty(null, null);
			this.Idprodotto = idprodotto;
		}

		/// <summary> CTor</summary>
		/// <param name="idprodotto">PK value for Prodotto which data should be fetched into this Prodotto object</param>
		/// <param name="validator">The custom validator object for this ProdottoEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ProdottoEntity(System.Int32 idprodotto, IValidator validator):base("ProdottoEntity")
		{
			InitClassEmpty(validator, null);
			this.Idprodotto = idprodotto;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ProdottoEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_servizioxcontrattos = (EntityCollection<ServizioxcontrattoEntity>)info.GetValue("_servizioxcontrattos", typeof(EntityCollection<ServizioxcontrattoEntity>));
				_categoriaprodotto = (CategoriaprodottoEntity)info.GetValue("_categoriaprodotto", typeof(CategoriaprodottoEntity));
				if(_categoriaprodotto!=null)
				{
					_categoriaprodotto.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_grupporadiu = (GrupporadiuEntity)info.GetValue("_grupporadiu", typeof(GrupporadiuEntity));
				if(_grupporadiu!=null)
				{
					_grupporadiu.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProdottoFieldIndex)fieldIndex)
			{
				case ProdottoFieldIndex.IdcategoriaProdotto:
					DesetupSyncCategoriaprodotto(true, false);
					break;
				case ProdottoFieldIndex.IdgruppoRadius:
					DesetupSyncGrupporadiu(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Categoriaprodotto":
					this.Categoriaprodotto = (CategoriaprodottoEntity)entity;
					break;
				case "Grupporadiu":
					this.Grupporadiu = (GrupporadiuEntity)entity;
					break;
				case "Servizioxcontrattos":
					this.Servizioxcontrattos.Add((ServizioxcontrattoEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Categoriaprodotto":
					toReturn.Add(Relations.CategoriaprodottoEntityUsingIdcategoriaProdotto);
					break;
				case "Grupporadiu":
					toReturn.Add(Relations.GrupporadiuEntityUsingIdgruppoRadius);
					break;
				case "Servizioxcontrattos":
					toReturn.Add(Relations.ServizioxcontrattoEntityUsingIdprodotto);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Categoriaprodotto":
					SetupSyncCategoriaprodotto(relatedEntity);
					break;
				case "Grupporadiu":
					SetupSyncGrupporadiu(relatedEntity);
					break;
				case "Servizioxcontrattos":
					this.Servizioxcontrattos.Add((ServizioxcontrattoEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Categoriaprodotto":
					DesetupSyncCategoriaprodotto(false, true);
					break;
				case "Grupporadiu":
					DesetupSyncGrupporadiu(false, true);
					break;
				case "Servizioxcontrattos":
					this.PerformRelatedEntityRemoval(this.Servizioxcontrattos, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_categoriaprodotto!=null)
			{
				toReturn.Add(_categoriaprodotto);
			}
			if(_grupporadiu!=null)
			{
				toReturn.Add(_grupporadiu);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Servizioxcontrattos);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_servizioxcontrattos", ((_servizioxcontrattos!=null) && (_servizioxcontrattos.Count>0) && !this.MarkedForDeletion)?_servizioxcontrattos:null);
				info.AddValue("_categoriaprodotto", (!this.MarkedForDeletion?_categoriaprodotto:null));
				info.AddValue("_grupporadiu", (!this.MarkedForDeletion?_grupporadiu:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProdottoRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Servizioxcontratto' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoServizioxcontrattos()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ServizioxcontrattoFields.Idprodotto, null, ComparisonOperator.Equal, this.Idprodotto));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Categoriaprodotto' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCategoriaprodotto()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(CategoriaprodottoFields.IdcategoriaProdotto, null, ComparisonOperator.Equal, this.IdcategoriaProdotto));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Grupporadiu' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoGrupporadiu()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(GrupporadiuFields.IdgruppoRadius, null, ComparisonOperator.Equal, this.IdgruppoRadius));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ProdottoEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._servizioxcontrattos);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._servizioxcontrattos = (EntityCollection<ServizioxcontrattoEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._servizioxcontrattos != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ServizioxcontrattoEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ServizioxcontrattoEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Categoriaprodotto", _categoriaprodotto);
			toReturn.Add("Grupporadiu", _grupporadiu);
			toReturn.Add("Servizioxcontrattos", _servizioxcontrattos);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Acquistabile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Codice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodiceFatturazione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Descrizione", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescrizioneAggiuntivaContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EscludiProrata", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdaliquotaIva", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdcategoriaProdotto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdgruppoRadius", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdlistinoVoip", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdproceduraProvisioningPrdef", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Idprodotto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NomeFileContratto", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NomeFileSchedaTecnica", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NrGgshiftPrimoAddebito", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RadiusGroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RadiusServiceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RadiusTerminatedServiceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SoloAmministrativo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoriaprodotto</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoriaprodotto(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoriaprodotto, new PropertyChangedEventHandler( OnCategoriaprodottoPropertyChanged ), "Categoriaprodotto", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticProdottoRelations.CategoriaprodottoEntityUsingIdcategoriaProdottoStatic, true, signalRelatedEntity, "Prodottos", resetFKFields, new int[] { (int)ProdottoFieldIndex.IdcategoriaProdotto } );
			_categoriaprodotto = null;
		}

		/// <summary> setups the sync logic for member _categoriaprodotto</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoriaprodotto(IEntityCore relatedEntity)
		{
			if(_categoriaprodotto!=relatedEntity)
			{
				DesetupSyncCategoriaprodotto(true, true);
				_categoriaprodotto = (CategoriaprodottoEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoriaprodotto, new PropertyChangedEventHandler( OnCategoriaprodottoPropertyChanged ), "Categoriaprodotto", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticProdottoRelations.CategoriaprodottoEntityUsingIdcategoriaProdottoStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoriaprodottoPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _grupporadiu</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGrupporadiu(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _grupporadiu, new PropertyChangedEventHandler( OnGrupporadiuPropertyChanged ), "Grupporadiu", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticProdottoRelations.GrupporadiuEntityUsingIdgruppoRadiusStatic, true, signalRelatedEntity, "Prodottos", resetFKFields, new int[] { (int)ProdottoFieldIndex.IdgruppoRadius } );
			_grupporadiu = null;
		}

		/// <summary> setups the sync logic for member _grupporadiu</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGrupporadiu(IEntityCore relatedEntity)
		{
			if(_grupporadiu!=relatedEntity)
			{
				DesetupSyncGrupporadiu(true, true);
				_grupporadiu = (GrupporadiuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _grupporadiu, new PropertyChangedEventHandler( OnGrupporadiuPropertyChanged ), "Grupporadiu", WTBILL.RadiusOrchestrator.Data.RelationClasses.StaticProdottoRelations.GrupporadiuEntityUsingIdgruppoRadiusStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGrupporadiuPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProdottoEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProdottoRelations Relations
		{
			get	{ return new ProdottoRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Servizioxcontratto' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathServizioxcontrattos
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<ServizioxcontrattoEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ServizioxcontrattoEntityFactory))), (IEntityRelation)GetRelationsForField("Servizioxcontrattos")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ProdottoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.ServizioxcontrattoEntity, 0, null, null, null, null, "Servizioxcontrattos", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Categoriaprodotto' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCategoriaprodotto
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(CategoriaprodottoEntityFactory))),	(IEntityRelation)GetRelationsForField("Categoriaprodotto")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ProdottoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.CategoriaprodottoEntity, 0, null, null, null, null, "Categoriaprodotto", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Grupporadiu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathGrupporadiu
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(GrupporadiuEntityFactory))),	(IEntityRelation)GetRelationsForField("Grupporadiu")[0], (int)WTBILL.RadiusOrchestrator.Data.EntityType.ProdottoEntity, (int)WTBILL.RadiusOrchestrator.Data.EntityType.GrupporadiuEntity, 0, null, null, null, null, "Grupporadiu", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Acquistabile property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."Acquistabile"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Acquistabile
		{
			get { return (System.Int16)GetValue((int)ProdottoFieldIndex.Acquistabile, true); }
			set	{ SetValue((int)ProdottoFieldIndex.Acquistabile, value); }
		}

		/// <summary> The Codice property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."Codice"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Codice
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.Codice, true); }
			set	{ SetValue((int)ProdottoFieldIndex.Codice, value); }
		}

		/// <summary> The CodiceFatturazione property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."CodiceFatturazione"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodiceFatturazione
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.CodiceFatturazione, true); }
			set	{ SetValue((int)ProdottoFieldIndex.CodiceFatturazione, value); }
		}

		/// <summary> The Descrizione property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."Descrizione"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Descrizione
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.Descrizione, true); }
			set	{ SetValue((int)ProdottoFieldIndex.Descrizione, value); }
		}

		/// <summary> The DescrizioneAggiuntivaContratto property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."DescrizioneAggiuntivaContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DescrizioneAggiuntivaContratto
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.DescrizioneAggiuntivaContratto, true); }
			set	{ SetValue((int)ProdottoFieldIndex.DescrizioneAggiuntivaContratto, value); }
		}

		/// <summary> The EscludiProrata property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."EscludiProrata"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 EscludiProrata
		{
			get { return (System.Int16)GetValue((int)ProdottoFieldIndex.EscludiProrata, true); }
			set	{ SetValue((int)ProdottoFieldIndex.EscludiProrata, value); }
		}

		/// <summary> The IdaliquotaIva property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."IDAliquotaIva"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdaliquotaIva
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProdottoFieldIndex.IdaliquotaIva, false); }
			set	{ SetValue((int)ProdottoFieldIndex.IdaliquotaIva, value); }
		}

		/// <summary> The IdcategoriaProdotto property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."IDCategoriaProdotto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IdcategoriaProdotto
		{
			get { return (System.Int32)GetValue((int)ProdottoFieldIndex.IdcategoriaProdotto, true); }
			set	{ SetValue((int)ProdottoFieldIndex.IdcategoriaProdotto, value); }
		}

		/// <summary> The IdgruppoRadius property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."IDGruppoRadius"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdgruppoRadius
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProdottoFieldIndex.IdgruppoRadius, false); }
			set	{ SetValue((int)ProdottoFieldIndex.IdgruppoRadius, value); }
		}

		/// <summary> The IdlistinoVoip property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."IDListinoVoip"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdlistinoVoip
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProdottoFieldIndex.IdlistinoVoip, false); }
			set	{ SetValue((int)ProdottoFieldIndex.IdlistinoVoip, value); }
		}

		/// <summary> The IdproceduraProvisioningPrdef property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."IDProceduraProvisioningPrdef"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IdproceduraProvisioningPrdef
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProdottoFieldIndex.IdproceduraProvisioningPrdef, false); }
			set	{ SetValue((int)ProdottoFieldIndex.IdproceduraProvisioningPrdef, value); }
		}

		/// <summary> The Idprodotto property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."IDProdotto"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Idprodotto
		{
			get { return (System.Int32)GetValue((int)ProdottoFieldIndex.Idprodotto, true); }
			set	{ SetValue((int)ProdottoFieldIndex.Idprodotto, value); }
		}

		/// <summary> The NomeFileContratto property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."NomeFileContratto"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NomeFileContratto
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.NomeFileContratto, true); }
			set	{ SetValue((int)ProdottoFieldIndex.NomeFileContratto, value); }
		}

		/// <summary> The NomeFileSchedaTecnica property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."NomeFileSchedaTecnica"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NomeFileSchedaTecnica
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.NomeFileSchedaTecnica, true); }
			set	{ SetValue((int)ProdottoFieldIndex.NomeFileSchedaTecnica, value); }
		}

		/// <summary> The NrGgshiftPrimoAddebito property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."NrGGShiftPrimoAddebito"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NrGgshiftPrimoAddebito
		{
			get { return (System.Int32)GetValue((int)ProdottoFieldIndex.NrGgshiftPrimoAddebito, true); }
			set	{ SetValue((int)ProdottoFieldIndex.NrGgshiftPrimoAddebito, value); }
		}

		/// <summary> The RadiusGroupName property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."RadiusGroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RadiusGroupName
		{
			get { return (System.String)GetValue((int)ProdottoFieldIndex.RadiusGroupName, true); }
			set	{ SetValue((int)ProdottoFieldIndex.RadiusGroupName, value); }
		}

		/// <summary> The RadiusServiceId property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."RadiusServiceID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RadiusServiceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProdottoFieldIndex.RadiusServiceId, false); }
			set	{ SetValue((int)ProdottoFieldIndex.RadiusServiceId, value); }
		}

		/// <summary> The RadiusTerminatedServiceId property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."RadiusTerminatedServiceID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RadiusTerminatedServiceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProdottoFieldIndex.RadiusTerminatedServiceId, false); }
			set	{ SetValue((int)ProdottoFieldIndex.RadiusTerminatedServiceId, value); }
		}

		/// <summary> The SoloAmministrativo property of the Entity Prodotto<br/><br/></summary>
		/// <remarks>Mapped on  table field: "prodotto"."SoloAmministrativo"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> SoloAmministrativo
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProdottoFieldIndex.SoloAmministrativo, false); }
			set	{ SetValue((int)ProdottoFieldIndex.SoloAmministrativo, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ServizioxcontrattoEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ServizioxcontrattoEntity))]
		public virtual EntityCollection<ServizioxcontrattoEntity> Servizioxcontrattos
		{
			get { return GetOrCreateEntityCollection<ServizioxcontrattoEntity, ServizioxcontrattoEntityFactory>("Prodotto", true, false, ref _servizioxcontrattos);	}
		}

		/// <summary> Gets / sets related entity of type 'CategoriaprodottoEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CategoriaprodottoEntity Categoriaprodotto
		{
			get	{ return _categoriaprodotto; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCategoriaprodotto(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Prodottos", "Categoriaprodotto", _categoriaprodotto, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'GrupporadiuEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual GrupporadiuEntity Grupporadiu
		{
			get	{ return _grupporadiu; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncGrupporadiu(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Prodottos", "Grupporadiu", _grupporadiu, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the WTBILL.RadiusOrchestrator.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)WTBILL.RadiusOrchestrator.Data.EntityType.ProdottoEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
