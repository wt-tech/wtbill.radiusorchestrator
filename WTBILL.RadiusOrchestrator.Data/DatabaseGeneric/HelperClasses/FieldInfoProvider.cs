﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (7 + 0));
			InitAttributoradiuEntityInfos();
			InitCategoriaprodottoEntityInfos();
			InitClienteEntityInfos();
			InitContrattoEntityInfos();
			InitGrupporadiuEntityInfos();
			InitProdottoEntityInfos();
			InitServizioxcontrattoEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits AttributoradiuEntity's FieldInfo objects</summary>
		private void InitAttributoradiuEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(AttributoradiuFieldIndex), "AttributoradiuEntity");
			this.AddElementFieldInfo("AttributoradiuEntity", "IdattributoRadius", typeof(System.Int32), true, false, false, false,  (int)AttributoradiuFieldIndex.IdattributoRadius, 0, 0, 11);
			this.AddElementFieldInfo("AttributoradiuEntity", "IdservizioXcontratto", typeof(System.Int32), false, true, false, false,  (int)AttributoradiuFieldIndex.IdservizioXcontratto, 0, 0, 11);
			this.AddElementFieldInfo("AttributoradiuEntity", "IsToDelete", typeof(System.Int16), false, false, false, false,  (int)AttributoradiuFieldIndex.IsToDelete, 0, 0, 1);
			this.AddElementFieldInfo("AttributoradiuEntity", "IsToInsert", typeof(System.Int16), false, false, false, false,  (int)AttributoradiuFieldIndex.IsToInsert, 0, 0, 1);
			this.AddElementFieldInfo("AttributoradiuEntity", "NomeAttributo", typeof(System.String), false, false, false, false,  (int)AttributoradiuFieldIndex.NomeAttributo, 50, 0, 0);
			this.AddElementFieldInfo("AttributoradiuEntity", "Operatore", typeof(System.String), false, false, false, false,  (int)AttributoradiuFieldIndex.Operatore, 10, 0, 0);
			this.AddElementFieldInfo("AttributoradiuEntity", "Valore", typeof(System.String), false, false, false, false,  (int)AttributoradiuFieldIndex.Valore, 80, 0, 0);
		}
		/// <summary>Inits CategoriaprodottoEntity's FieldInfo objects</summary>
		private void InitCategoriaprodottoEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CategoriaprodottoFieldIndex), "CategoriaprodottoEntity");
			this.AddElementFieldInfo("CategoriaprodottoEntity", "Descrizione", typeof(System.String), false, false, false, false,  (int)CategoriaprodottoFieldIndex.Descrizione, 100, 0, 0);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "IdaliquotaIva", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CategoriaprodottoFieldIndex.IdaliquotaIva, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "IdcategoriaProdotto", typeof(System.Int32), true, false, true, false,  (int)CategoriaprodottoFieldIndex.IdcategoriaProdotto, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "IdproceduraProvisioningPredef", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CategoriaprodottoFieldIndex.IdproceduraProvisioningPredef, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "IdtipoLavorazioneTecnicaAttivazione", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CategoriaprodottoFieldIndex.IdtipoLavorazioneTecnicaAttivazione, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "IdtipoLavorazioneTecnicaSync", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CategoriaprodottoFieldIndex.IdtipoLavorazioneTecnicaSync, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "IdtipoLavorazioneTecnicaVariazione", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CategoriaprodottoFieldIndex.IdtipoLavorazioneTecnicaVariazione, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "OrdineFatturazione", typeof(System.Int32), false, false, false, false,  (int)CategoriaprodottoFieldIndex.OrdineFatturazione, 0, 0, 11);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "RadUserPrefisso", typeof(System.String), false, false, false, true,  (int)CategoriaprodottoFieldIndex.RadUserPrefisso, 25, 0, 0);
			this.AddElementFieldInfo("CategoriaprodottoEntity", "RadUserSuffisso", typeof(System.String), false, false, false, true,  (int)CategoriaprodottoFieldIndex.RadUserSuffisso, 25, 0, 0);
		}
		/// <summary>Inits ClienteEntity's FieldInfo objects</summary>
		private void InitClienteEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ClienteFieldIndex), "ClienteEntity");
			this.AddElementFieldInfo("ClienteEntity", "Cap", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Cap, 10, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Cellulare", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Cellulare, 65535, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Cf", typeof(System.String), false, false, false, false,  (int)ClienteFieldIndex.Cf, 20, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Cfamministratore", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Cfamministratore, 20, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Citta", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Citta, 100, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "CodiceClienteEsterno", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.CodiceClienteEsterno, 50, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "CodicePaeseIva", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.CodicePaeseIva, 4, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Cognome", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Cognome, 100, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "DataNascita", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ClienteFieldIndex.DataNascita, 0, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Denominazione", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Denominazione, 180, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "DocIdnumero", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.DocIdnumero, 80, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "DocIdrilasciatoDa", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.DocIdrilasciatoDa, 200, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "DocIdrilasciatoIl", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ClienteFieldIndex.DocIdrilasciatoIl, 0, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Email", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Email, 180, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "EscludiSolleciti", typeof(System.Int16), false, false, false, false,  (int)ClienteFieldIndex.EscludiSolleciti, 0, 0, 1);
			this.AddElementFieldInfo("ClienteEntity", "FecodiceDestinatario", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.FecodiceDestinatario, 20, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "FepecDestinatario", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.FepecDestinatario, 50, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Iban", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Iban, 50, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "IdaliquotaIva", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ClienteFieldIndex.IdaliquotaIva, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "Idcliente", typeof(System.Int32), true, false, true, false,  (int)ClienteFieldIndex.Idcliente, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdgruppoCliente", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ClienteFieldIndex.IdgruppoCliente, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdmodalitaInvioFattura", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ClienteFieldIndex.IdmodalitaInvioFattura, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdmodalitaPagPreferita", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ClienteFieldIndex.IdmodalitaPagPreferita, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdstatoSolvenzaCliente", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.IdstatoSolvenzaCliente, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdtempiPagamento", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ClienteFieldIndex.IdtempiPagamento, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdtipoArrotondamento", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.IdtipoArrotondamento, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdtipoCliente", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.IdtipoCliente, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "IdtipoDocumentoIdentita", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ClienteFieldIndex.IdtipoDocumentoIdentita, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "Indirizzo", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Indirizzo, 65535, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "InsegnaCommerciale", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.InsegnaCommerciale, 180, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "LuogoNascita", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.LuogoNascita, 150, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "MediaGgpagamentoFattura", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.MediaGgpagamentoFattura, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "NazioneIso", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.NazioneIso, 4, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Nome", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Nome, 100, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Note", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Note, 65535, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "NrFattureEmesse", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.NrFattureEmesse, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "NrFattureNonPagate", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.NrFattureNonPagate, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "NumeroCivico", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.NumeroCivico, 10, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "NumSollecitiInviati", typeof(System.Int32), false, false, false, false,  (int)ClienteFieldIndex.NumSollecitiInviati, 0, 0, 11);
			this.AddElementFieldInfo("ClienteEntity", "Password", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Password, 20, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Pec", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Pec, 60, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Piva", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Piva, 20, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Provincia", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Provincia, 2, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "ProvinciaNascita", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.ProvinciaNascita, 2, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "RecapitoTelefonico", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.RecapitoTelefonico, 65535, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "SaldoContabile", typeof(System.Decimal), false, false, false, false,  (int)ClienteFieldIndex.SaldoContabile, 0, 2, 10);
			this.AddElementFieldInfo("ClienteEntity", "UltimoSollecito", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ClienteFieldIndex.UltimoSollecito, 0, 0, 0);
			this.AddElementFieldInfo("ClienteEntity", "Username", typeof(System.String), false, false, false, true,  (int)ClienteFieldIndex.Username, 20, 0, 0);
		}
		/// <summary>Inits ContrattoEntity's FieldInfo objects</summary>
		private void InitContrattoEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ContrattoFieldIndex), "ContrattoEntity");
			this.AddElementFieldInfo("ContrattoEntity", "CapInstallazione", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.CapInstallazione, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Cartaceo", typeof(System.Int16), false, false, false, false,  (int)ContrattoFieldIndex.Cartaceo, 0, 0, 1);
			this.AddElementFieldInfo("ContrattoEntity", "CittaInstallazione", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.CittaInstallazione, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "CoordinateInst", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.CoordinateInst, 2147483647, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "DataAttivazione", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ContrattoFieldIndex.DataAttivazione, 0, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "DataCessazione", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ContrattoFieldIndex.DataCessazione, 0, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "DataFirma", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ContrattoFieldIndex.DataFirma, 0, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "DataScadenzaDurataContrattuale", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ContrattoFieldIndex.DataScadenzaDurataContrattuale, 0, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "DescrizioneAggiuntiva", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.DescrizioneAggiuntiva, 100, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "FecodiceCig", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.FecodiceCig, 15, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "FecodiceCup", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.FecodiceCup, 15, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "FedataContratto", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ContrattoFieldIndex.FedataContratto, 0, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "FenumeroContratto", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.FenumeroContratto, 15, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "IdcicloPagamento", typeof(System.Int32), false, false, false, false,  (int)ContrattoFieldIndex.IdcicloPagamento, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "Idcliente", typeof(System.Int32), false, true, false, false,  (int)ContrattoFieldIndex.Idcliente, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "Idcontratto", typeof(System.Int32), true, false, true, false,  (int)ContrattoFieldIndex.Idcontratto, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "IddurataContrattule", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ContrattoFieldIndex.IddurataContrattule, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "IdmodalitaPagamentoAttivazione", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ContrattoFieldIndex.IdmodalitaPagamentoAttivazione, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "Idpartner", typeof(System.Int32), false, false, false, false,  (int)ContrattoFieldIndex.Idpartner, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "IdstatoContratto", typeof(System.Int32), false, false, false, false,  (int)ContrattoFieldIndex.IdstatoContratto, 0, 0, 11);
			this.AddElementFieldInfo("ContrattoEntity", "IndirizzoInstallazione", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.IndirizzoInstallazione, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "NoteCliente", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.NoteCliente, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "NoteInterne", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.NoteInterne, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "NotePartner", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.NotePartner, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Numero", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Numero, 20, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "ProvinciaInstallazione", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.ProvinciaInstallazione, 65535, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhsaziendaClienteFinale", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhsaziendaClienteFinale, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whscap", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whscap, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhscfclienteFinale", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhscfclienteFinale, 25, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whscitta", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whscitta, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whscivico", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whscivico, 255, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhscognomeClienteFinale", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhscognomeClienteFinale, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whsindirizzo", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whsindirizzo, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhsnomeClienteFinale", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhsnomeClienteFinale, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whsnote", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whsnote, 255, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhspartitaIvaclienteFinale", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhspartitaIvaclienteFinale, 25, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whsprovincia", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whsprovincia, 2, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhsrecapitoReferente1", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhsrecapitoReferente1, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "WhsrecapitoReferente2", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.WhsrecapitoReferente2, 80, 0, 0);
			this.AddElementFieldInfo("ContrattoEntity", "Whstoponimo", typeof(System.String), false, false, false, true,  (int)ContrattoFieldIndex.Whstoponimo, 80, 0, 0);
		}
		/// <summary>Inits GrupporadiuEntity's FieldInfo objects</summary>
		private void InitGrupporadiuEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GrupporadiuFieldIndex), "GrupporadiuEntity");
			this.AddElementFieldInfo("GrupporadiuEntity", "GruppoRadius", typeof(System.String), false, false, false, false,  (int)GrupporadiuFieldIndex.GruppoRadius, 150, 0, 0);
			this.AddElementFieldInfo("GrupporadiuEntity", "IdgruppoRadius", typeof(System.Int32), true, false, true, false,  (int)GrupporadiuFieldIndex.IdgruppoRadius, 0, 0, 11);
		}
		/// <summary>Inits ProdottoEntity's FieldInfo objects</summary>
		private void InitProdottoEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProdottoFieldIndex), "ProdottoEntity");
			this.AddElementFieldInfo("ProdottoEntity", "Acquistabile", typeof(System.Int16), false, false, false, false,  (int)ProdottoFieldIndex.Acquistabile, 0, 0, 1);
			this.AddElementFieldInfo("ProdottoEntity", "Codice", typeof(System.String), false, false, false, false,  (int)ProdottoFieldIndex.Codice, 25, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "CodiceFatturazione", typeof(System.String), false, false, false, true,  (int)ProdottoFieldIndex.CodiceFatturazione, 25, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "Descrizione", typeof(System.String), false, false, false, false,  (int)ProdottoFieldIndex.Descrizione, 200, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "DescrizioneAggiuntivaContratto", typeof(System.String), false, false, false, true,  (int)ProdottoFieldIndex.DescrizioneAggiuntivaContratto, 200, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "EscludiProrata", typeof(System.Int16), false, false, false, false,  (int)ProdottoFieldIndex.EscludiProrata, 0, 0, 1);
			this.AddElementFieldInfo("ProdottoEntity", "IdaliquotaIva", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProdottoFieldIndex.IdaliquotaIva, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "IdcategoriaProdotto", typeof(System.Int32), false, true, false, false,  (int)ProdottoFieldIndex.IdcategoriaProdotto, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "IdgruppoRadius", typeof(Nullable<System.Int32>), false, true, false, true,  (int)ProdottoFieldIndex.IdgruppoRadius, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "IdlistinoVoip", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProdottoFieldIndex.IdlistinoVoip, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "IdproceduraProvisioningPrdef", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProdottoFieldIndex.IdproceduraProvisioningPrdef, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "Idprodotto", typeof(System.Int32), true, false, true, false,  (int)ProdottoFieldIndex.Idprodotto, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "NomeFileContratto", typeof(System.String), false, false, false, true,  (int)ProdottoFieldIndex.NomeFileContratto, 200, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "NomeFileSchedaTecnica", typeof(System.String), false, false, false, true,  (int)ProdottoFieldIndex.NomeFileSchedaTecnica, 150, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "NrGgshiftPrimoAddebito", typeof(System.Int32), false, false, false, false,  (int)ProdottoFieldIndex.NrGgshiftPrimoAddebito, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "RadiusGroupName", typeof(System.String), false, false, false, true,  (int)ProdottoFieldIndex.RadiusGroupName, 150, 0, 0);
			this.AddElementFieldInfo("ProdottoEntity", "RadiusServiceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProdottoFieldIndex.RadiusServiceId, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "RadiusTerminatedServiceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProdottoFieldIndex.RadiusTerminatedServiceId, 0, 0, 11);
			this.AddElementFieldInfo("ProdottoEntity", "SoloAmministrativo", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ProdottoFieldIndex.SoloAmministrativo, 0, 0, 1);
		}
		/// <summary>Inits ServizioxcontrattoEntity's FieldInfo objects</summary>
		private void InitServizioxcontrattoEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ServizioxcontrattoFieldIndex), "ServizioxcontrattoEntity");
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "CodiceMigrazione", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.CodiceMigrazione, 50, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "CodiceRisorsa", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.CodiceRisorsa, 100, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "CodiceStatoProvisioning", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.CodiceStatoProvisioning, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "CodiceStatoRadius", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.CodiceStatoRadius, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Dac", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Dac, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataAttesaCessazione", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataAttesaCessazione, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataDecorrenzaCessazione", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataDecorrenzaCessazione, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataDecorrenzaSospensione", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataDecorrenzaSospensione, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataInstallazioneApparati", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataInstallazioneApparati, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataProvisioningEffettiva", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataProvisioningEffettiva, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataRichiestaCessazione", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataRichiestaCessazione, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DataUltimoAggiornamentoSegnali", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DataUltimoAggiornamentoSegnali, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "DescrizioneAggiuntiva", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.DescrizioneAggiuntiva, 200, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "FatturatoFinoAl", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.FatturatoFinoAl, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "FecodiceCig", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.FecodiceCig, 15, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "FecodiceCup", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.FecodiceCup, 15, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "FedataContratto", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.FedataContratto, 0, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "FenumeroContratto", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.FenumeroContratto, 15, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "FileSchedaTecnica", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.FileSchedaTecnica, 150, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdaliquotaIva", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdaliquotaIva, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Idbts", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Idbts, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdcicloImmissioneInFatturazione", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdcicloImmissioneInFatturazione, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Idcontratto", typeof(System.Int32), false, true, false, false,  (int)ServizioxcontrattoFieldIndex.Idcontratto, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdentificativoAccesso", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdentificativoAccesso, 60, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Identita", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Identita, 255, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdgruppoRadius", typeof(Nullable<System.Int32>), false, true, false, true,  (int)ServizioxcontrattoFieldIndex.IdgruppoRadius, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Idinstallatore", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Idinstallatore, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdproceduraProvisioning", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdproceduraProvisioning, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Idprodotto", typeof(System.Int32), false, true, false, false,  (int)ServizioxcontrattoFieldIndex.Idprodotto, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdrichiestaCessazione", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdrichiestaCessazione, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdservizioXcontratto", typeof(System.Int32), true, false, true, false,  (int)ServizioxcontrattoFieldIndex.IdservizioXcontratto, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Idsettore", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Idsettore, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdstatoServizioXcontratto", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdstatoServizioXcontratto, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "IdtipoSospensione", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.IdtipoSospensione, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Idzona", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Idzona, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Latitudine", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Latitudine, 0, 6, 10);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Longitudine", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Longitudine, 0, 6, 10);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Macinstallatore", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.Macinstallatore, 50, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "MandareRidBanca", typeof(System.Int16), false, false, false, false,  (int)ServizioxcontrattoFieldIndex.MandareRidBanca, 0, 0, 1);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "MultihomeAccessoAttivo", typeof(System.Int16), false, false, false, false,  (int)ServizioxcontrattoFieldIndex.MultihomeAccessoAttivo, 0, 0, 1);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "NoteCliente", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.NoteCliente, 65535, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "NoteInstallatore", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.NoteInstallatore, 65535, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "NoteInterne", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.NoteInterne, 65535, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "NotePartner", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.NotePartner, 65535, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "PasswordAccesso", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.PasswordAccesso, 60, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "PresospRadiusGroupName", typeof(System.String), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.PresospRadiusGroupName, 80, 0, 0);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "PresospRadiusServiceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.PresospRadiusServiceId, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "Prezzo", typeof(System.Decimal), false, false, false, false,  (int)ServizioxcontrattoFieldIndex.Prezzo, 0, 2, 10);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "SegnaleCh0Rx", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.SegnaleCh0Rx, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "SegnaleCh0Tx", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.SegnaleCh0Tx, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "SegnaleCh1Rx", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.SegnaleCh1Rx, 0, 0, 11);
			this.AddElementFieldInfo("ServizioxcontrattoEntity", "SegnaleCh1Tx", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ServizioxcontrattoFieldIndex.SegnaleCh1Tx, 0, 0, 11);
		}
		
	}
}




