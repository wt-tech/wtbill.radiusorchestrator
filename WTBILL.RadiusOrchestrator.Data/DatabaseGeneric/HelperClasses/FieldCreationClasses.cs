﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using WTBILL.RadiusOrchestrator.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.Data;

namespace WTBILL.RadiusOrchestrator.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity AttributoradiuEntity</summary>
	public partial class AttributoradiuFields
	{
		/// <summary>Creates a new AttributoradiuEntity.IdattributoRadius field instance</summary>
		public static EntityField2 IdattributoRadius
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.IdattributoRadius);}
		}
		/// <summary>Creates a new AttributoradiuEntity.IdservizioXcontratto field instance</summary>
		public static EntityField2 IdservizioXcontratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.IdservizioXcontratto);}
		}
		/// <summary>Creates a new AttributoradiuEntity.IsToDelete field instance</summary>
		public static EntityField2 IsToDelete
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.IsToDelete);}
		}
		/// <summary>Creates a new AttributoradiuEntity.IsToInsert field instance</summary>
		public static EntityField2 IsToInsert
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.IsToInsert);}
		}
		/// <summary>Creates a new AttributoradiuEntity.NomeAttributo field instance</summary>
		public static EntityField2 NomeAttributo
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.NomeAttributo);}
		}
		/// <summary>Creates a new AttributoradiuEntity.Operatore field instance</summary>
		public static EntityField2 Operatore
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.Operatore);}
		}
		/// <summary>Creates a new AttributoradiuEntity.Valore field instance</summary>
		public static EntityField2 Valore
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributoradiuFieldIndex.Valore);}
		}
	}

	/// <summary>Field Creation Class for entity CategoriaprodottoEntity</summary>
	public partial class CategoriaprodottoFields
	{
		/// <summary>Creates a new CategoriaprodottoEntity.Descrizione field instance</summary>
		public static EntityField2 Descrizione
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.Descrizione);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.IdaliquotaIva field instance</summary>
		public static EntityField2 IdaliquotaIva
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.IdaliquotaIva);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.IdcategoriaProdotto field instance</summary>
		public static EntityField2 IdcategoriaProdotto
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.IdcategoriaProdotto);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.IdproceduraProvisioningPredef field instance</summary>
		public static EntityField2 IdproceduraProvisioningPredef
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.IdproceduraProvisioningPredef);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.IdtipoLavorazioneTecnicaAttivazione field instance</summary>
		public static EntityField2 IdtipoLavorazioneTecnicaAttivazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.IdtipoLavorazioneTecnicaAttivazione);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.IdtipoLavorazioneTecnicaSync field instance</summary>
		public static EntityField2 IdtipoLavorazioneTecnicaSync
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.IdtipoLavorazioneTecnicaSync);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.IdtipoLavorazioneTecnicaVariazione field instance</summary>
		public static EntityField2 IdtipoLavorazioneTecnicaVariazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.IdtipoLavorazioneTecnicaVariazione);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.OrdineFatturazione field instance</summary>
		public static EntityField2 OrdineFatturazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.OrdineFatturazione);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.RadUserPrefisso field instance</summary>
		public static EntityField2 RadUserPrefisso
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.RadUserPrefisso);}
		}
		/// <summary>Creates a new CategoriaprodottoEntity.RadUserSuffisso field instance</summary>
		public static EntityField2 RadUserSuffisso
		{
			get { return (EntityField2)EntityFieldFactory.Create(CategoriaprodottoFieldIndex.RadUserSuffisso);}
		}
	}

	/// <summary>Field Creation Class for entity ClienteEntity</summary>
	public partial class ClienteFields
	{
		/// <summary>Creates a new ClienteEntity.Cap field instance</summary>
		public static EntityField2 Cap
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Cap);}
		}
		/// <summary>Creates a new ClienteEntity.Cellulare field instance</summary>
		public static EntityField2 Cellulare
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Cellulare);}
		}
		/// <summary>Creates a new ClienteEntity.Cf field instance</summary>
		public static EntityField2 Cf
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Cf);}
		}
		/// <summary>Creates a new ClienteEntity.Cfamministratore field instance</summary>
		public static EntityField2 Cfamministratore
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Cfamministratore);}
		}
		/// <summary>Creates a new ClienteEntity.Citta field instance</summary>
		public static EntityField2 Citta
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Citta);}
		}
		/// <summary>Creates a new ClienteEntity.CodiceClienteEsterno field instance</summary>
		public static EntityField2 CodiceClienteEsterno
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.CodiceClienteEsterno);}
		}
		/// <summary>Creates a new ClienteEntity.CodicePaeseIva field instance</summary>
		public static EntityField2 CodicePaeseIva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.CodicePaeseIva);}
		}
		/// <summary>Creates a new ClienteEntity.Cognome field instance</summary>
		public static EntityField2 Cognome
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Cognome);}
		}
		/// <summary>Creates a new ClienteEntity.DataNascita field instance</summary>
		public static EntityField2 DataNascita
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.DataNascita);}
		}
		/// <summary>Creates a new ClienteEntity.Denominazione field instance</summary>
		public static EntityField2 Denominazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Denominazione);}
		}
		/// <summary>Creates a new ClienteEntity.DocIdnumero field instance</summary>
		public static EntityField2 DocIdnumero
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.DocIdnumero);}
		}
		/// <summary>Creates a new ClienteEntity.DocIdrilasciatoDa field instance</summary>
		public static EntityField2 DocIdrilasciatoDa
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.DocIdrilasciatoDa);}
		}
		/// <summary>Creates a new ClienteEntity.DocIdrilasciatoIl field instance</summary>
		public static EntityField2 DocIdrilasciatoIl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.DocIdrilasciatoIl);}
		}
		/// <summary>Creates a new ClienteEntity.Email field instance</summary>
		public static EntityField2 Email
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Email);}
		}
		/// <summary>Creates a new ClienteEntity.EscludiSolleciti field instance</summary>
		public static EntityField2 EscludiSolleciti
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.EscludiSolleciti);}
		}
		/// <summary>Creates a new ClienteEntity.FecodiceDestinatario field instance</summary>
		public static EntityField2 FecodiceDestinatario
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.FecodiceDestinatario);}
		}
		/// <summary>Creates a new ClienteEntity.FepecDestinatario field instance</summary>
		public static EntityField2 FepecDestinatario
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.FepecDestinatario);}
		}
		/// <summary>Creates a new ClienteEntity.Iban field instance</summary>
		public static EntityField2 Iban
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Iban);}
		}
		/// <summary>Creates a new ClienteEntity.IdaliquotaIva field instance</summary>
		public static EntityField2 IdaliquotaIva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdaliquotaIva);}
		}
		/// <summary>Creates a new ClienteEntity.Idcliente field instance</summary>
		public static EntityField2 Idcliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Idcliente);}
		}
		/// <summary>Creates a new ClienteEntity.IdgruppoCliente field instance</summary>
		public static EntityField2 IdgruppoCliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdgruppoCliente);}
		}
		/// <summary>Creates a new ClienteEntity.IdmodalitaInvioFattura field instance</summary>
		public static EntityField2 IdmodalitaInvioFattura
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdmodalitaInvioFattura);}
		}
		/// <summary>Creates a new ClienteEntity.IdmodalitaPagPreferita field instance</summary>
		public static EntityField2 IdmodalitaPagPreferita
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdmodalitaPagPreferita);}
		}
		/// <summary>Creates a new ClienteEntity.IdstatoSolvenzaCliente field instance</summary>
		public static EntityField2 IdstatoSolvenzaCliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdstatoSolvenzaCliente);}
		}
		/// <summary>Creates a new ClienteEntity.IdtempiPagamento field instance</summary>
		public static EntityField2 IdtempiPagamento
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdtempiPagamento);}
		}
		/// <summary>Creates a new ClienteEntity.IdtipoArrotondamento field instance</summary>
		public static EntityField2 IdtipoArrotondamento
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdtipoArrotondamento);}
		}
		/// <summary>Creates a new ClienteEntity.IdtipoCliente field instance</summary>
		public static EntityField2 IdtipoCliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdtipoCliente);}
		}
		/// <summary>Creates a new ClienteEntity.IdtipoDocumentoIdentita field instance</summary>
		public static EntityField2 IdtipoDocumentoIdentita
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.IdtipoDocumentoIdentita);}
		}
		/// <summary>Creates a new ClienteEntity.Indirizzo field instance</summary>
		public static EntityField2 Indirizzo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Indirizzo);}
		}
		/// <summary>Creates a new ClienteEntity.InsegnaCommerciale field instance</summary>
		public static EntityField2 InsegnaCommerciale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.InsegnaCommerciale);}
		}
		/// <summary>Creates a new ClienteEntity.LuogoNascita field instance</summary>
		public static EntityField2 LuogoNascita
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.LuogoNascita);}
		}
		/// <summary>Creates a new ClienteEntity.MediaGgpagamentoFattura field instance</summary>
		public static EntityField2 MediaGgpagamentoFattura
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.MediaGgpagamentoFattura);}
		}
		/// <summary>Creates a new ClienteEntity.NazioneIso field instance</summary>
		public static EntityField2 NazioneIso
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.NazioneIso);}
		}
		/// <summary>Creates a new ClienteEntity.Nome field instance</summary>
		public static EntityField2 Nome
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Nome);}
		}
		/// <summary>Creates a new ClienteEntity.Note field instance</summary>
		public static EntityField2 Note
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Note);}
		}
		/// <summary>Creates a new ClienteEntity.NrFattureEmesse field instance</summary>
		public static EntityField2 NrFattureEmesse
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.NrFattureEmesse);}
		}
		/// <summary>Creates a new ClienteEntity.NrFattureNonPagate field instance</summary>
		public static EntityField2 NrFattureNonPagate
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.NrFattureNonPagate);}
		}
		/// <summary>Creates a new ClienteEntity.NumeroCivico field instance</summary>
		public static EntityField2 NumeroCivico
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.NumeroCivico);}
		}
		/// <summary>Creates a new ClienteEntity.NumSollecitiInviati field instance</summary>
		public static EntityField2 NumSollecitiInviati
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.NumSollecitiInviati);}
		}
		/// <summary>Creates a new ClienteEntity.Password field instance</summary>
		public static EntityField2 Password
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Password);}
		}
		/// <summary>Creates a new ClienteEntity.Pec field instance</summary>
		public static EntityField2 Pec
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Pec);}
		}
		/// <summary>Creates a new ClienteEntity.Piva field instance</summary>
		public static EntityField2 Piva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Piva);}
		}
		/// <summary>Creates a new ClienteEntity.Provincia field instance</summary>
		public static EntityField2 Provincia
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Provincia);}
		}
		/// <summary>Creates a new ClienteEntity.ProvinciaNascita field instance</summary>
		public static EntityField2 ProvinciaNascita
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.ProvinciaNascita);}
		}
		/// <summary>Creates a new ClienteEntity.RecapitoTelefonico field instance</summary>
		public static EntityField2 RecapitoTelefonico
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.RecapitoTelefonico);}
		}
		/// <summary>Creates a new ClienteEntity.SaldoContabile field instance</summary>
		public static EntityField2 SaldoContabile
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.SaldoContabile);}
		}
		/// <summary>Creates a new ClienteEntity.UltimoSollecito field instance</summary>
		public static EntityField2 UltimoSollecito
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.UltimoSollecito);}
		}
		/// <summary>Creates a new ClienteEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClienteFieldIndex.Username);}
		}
	}

	/// <summary>Field Creation Class for entity ContrattoEntity</summary>
	public partial class ContrattoFields
	{
		/// <summary>Creates a new ContrattoEntity.CapInstallazione field instance</summary>
		public static EntityField2 CapInstallazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.CapInstallazione);}
		}
		/// <summary>Creates a new ContrattoEntity.Cartaceo field instance</summary>
		public static EntityField2 Cartaceo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Cartaceo);}
		}
		/// <summary>Creates a new ContrattoEntity.CittaInstallazione field instance</summary>
		public static EntityField2 CittaInstallazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.CittaInstallazione);}
		}
		/// <summary>Creates a new ContrattoEntity.CoordinateInst field instance</summary>
		public static EntityField2 CoordinateInst
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.CoordinateInst);}
		}
		/// <summary>Creates a new ContrattoEntity.DataAttivazione field instance</summary>
		public static EntityField2 DataAttivazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.DataAttivazione);}
		}
		/// <summary>Creates a new ContrattoEntity.DataCessazione field instance</summary>
		public static EntityField2 DataCessazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.DataCessazione);}
		}
		/// <summary>Creates a new ContrattoEntity.DataFirma field instance</summary>
		public static EntityField2 DataFirma
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.DataFirma);}
		}
		/// <summary>Creates a new ContrattoEntity.DataScadenzaDurataContrattuale field instance</summary>
		public static EntityField2 DataScadenzaDurataContrattuale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.DataScadenzaDurataContrattuale);}
		}
		/// <summary>Creates a new ContrattoEntity.DescrizioneAggiuntiva field instance</summary>
		public static EntityField2 DescrizioneAggiuntiva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.DescrizioneAggiuntiva);}
		}
		/// <summary>Creates a new ContrattoEntity.FecodiceCig field instance</summary>
		public static EntityField2 FecodiceCig
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.FecodiceCig);}
		}
		/// <summary>Creates a new ContrattoEntity.FecodiceCup field instance</summary>
		public static EntityField2 FecodiceCup
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.FecodiceCup);}
		}
		/// <summary>Creates a new ContrattoEntity.FedataContratto field instance</summary>
		public static EntityField2 FedataContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.FedataContratto);}
		}
		/// <summary>Creates a new ContrattoEntity.FenumeroContratto field instance</summary>
		public static EntityField2 FenumeroContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.FenumeroContratto);}
		}
		/// <summary>Creates a new ContrattoEntity.IdcicloPagamento field instance</summary>
		public static EntityField2 IdcicloPagamento
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.IdcicloPagamento);}
		}
		/// <summary>Creates a new ContrattoEntity.Idcliente field instance</summary>
		public static EntityField2 Idcliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Idcliente);}
		}
		/// <summary>Creates a new ContrattoEntity.Idcontratto field instance</summary>
		public static EntityField2 Idcontratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Idcontratto);}
		}
		/// <summary>Creates a new ContrattoEntity.IddurataContrattule field instance</summary>
		public static EntityField2 IddurataContrattule
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.IddurataContrattule);}
		}
		/// <summary>Creates a new ContrattoEntity.IdmodalitaPagamentoAttivazione field instance</summary>
		public static EntityField2 IdmodalitaPagamentoAttivazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.IdmodalitaPagamentoAttivazione);}
		}
		/// <summary>Creates a new ContrattoEntity.Idpartner field instance</summary>
		public static EntityField2 Idpartner
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Idpartner);}
		}
		/// <summary>Creates a new ContrattoEntity.IdstatoContratto field instance</summary>
		public static EntityField2 IdstatoContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.IdstatoContratto);}
		}
		/// <summary>Creates a new ContrattoEntity.IndirizzoInstallazione field instance</summary>
		public static EntityField2 IndirizzoInstallazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.IndirizzoInstallazione);}
		}
		/// <summary>Creates a new ContrattoEntity.NoteCliente field instance</summary>
		public static EntityField2 NoteCliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.NoteCliente);}
		}
		/// <summary>Creates a new ContrattoEntity.NoteInterne field instance</summary>
		public static EntityField2 NoteInterne
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.NoteInterne);}
		}
		/// <summary>Creates a new ContrattoEntity.NotePartner field instance</summary>
		public static EntityField2 NotePartner
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.NotePartner);}
		}
		/// <summary>Creates a new ContrattoEntity.Numero field instance</summary>
		public static EntityField2 Numero
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Numero);}
		}
		/// <summary>Creates a new ContrattoEntity.ProvinciaInstallazione field instance</summary>
		public static EntityField2 ProvinciaInstallazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.ProvinciaInstallazione);}
		}
		/// <summary>Creates a new ContrattoEntity.WhsaziendaClienteFinale field instance</summary>
		public static EntityField2 WhsaziendaClienteFinale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhsaziendaClienteFinale);}
		}
		/// <summary>Creates a new ContrattoEntity.Whscap field instance</summary>
		public static EntityField2 Whscap
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whscap);}
		}
		/// <summary>Creates a new ContrattoEntity.WhscfclienteFinale field instance</summary>
		public static EntityField2 WhscfclienteFinale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhscfclienteFinale);}
		}
		/// <summary>Creates a new ContrattoEntity.Whscitta field instance</summary>
		public static EntityField2 Whscitta
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whscitta);}
		}
		/// <summary>Creates a new ContrattoEntity.Whscivico field instance</summary>
		public static EntityField2 Whscivico
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whscivico);}
		}
		/// <summary>Creates a new ContrattoEntity.WhscognomeClienteFinale field instance</summary>
		public static EntityField2 WhscognomeClienteFinale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhscognomeClienteFinale);}
		}
		/// <summary>Creates a new ContrattoEntity.Whsindirizzo field instance</summary>
		public static EntityField2 Whsindirizzo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whsindirizzo);}
		}
		/// <summary>Creates a new ContrattoEntity.WhsnomeClienteFinale field instance</summary>
		public static EntityField2 WhsnomeClienteFinale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhsnomeClienteFinale);}
		}
		/// <summary>Creates a new ContrattoEntity.Whsnote field instance</summary>
		public static EntityField2 Whsnote
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whsnote);}
		}
		/// <summary>Creates a new ContrattoEntity.WhspartitaIvaclienteFinale field instance</summary>
		public static EntityField2 WhspartitaIvaclienteFinale
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhspartitaIvaclienteFinale);}
		}
		/// <summary>Creates a new ContrattoEntity.Whsprovincia field instance</summary>
		public static EntityField2 Whsprovincia
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whsprovincia);}
		}
		/// <summary>Creates a new ContrattoEntity.WhsrecapitoReferente1 field instance</summary>
		public static EntityField2 WhsrecapitoReferente1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhsrecapitoReferente1);}
		}
		/// <summary>Creates a new ContrattoEntity.WhsrecapitoReferente2 field instance</summary>
		public static EntityField2 WhsrecapitoReferente2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.WhsrecapitoReferente2);}
		}
		/// <summary>Creates a new ContrattoEntity.Whstoponimo field instance</summary>
		public static EntityField2 Whstoponimo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContrattoFieldIndex.Whstoponimo);}
		}
	}

	/// <summary>Field Creation Class for entity GrupporadiuEntity</summary>
	public partial class GrupporadiuFields
	{
		/// <summary>Creates a new GrupporadiuEntity.GruppoRadius field instance</summary>
		public static EntityField2 GruppoRadius
		{
			get { return (EntityField2)EntityFieldFactory.Create(GrupporadiuFieldIndex.GruppoRadius);}
		}
		/// <summary>Creates a new GrupporadiuEntity.IdgruppoRadius field instance</summary>
		public static EntityField2 IdgruppoRadius
		{
			get { return (EntityField2)EntityFieldFactory.Create(GrupporadiuFieldIndex.IdgruppoRadius);}
		}
	}

	/// <summary>Field Creation Class for entity ProdottoEntity</summary>
	public partial class ProdottoFields
	{
		/// <summary>Creates a new ProdottoEntity.Acquistabile field instance</summary>
		public static EntityField2 Acquistabile
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.Acquistabile);}
		}
		/// <summary>Creates a new ProdottoEntity.Codice field instance</summary>
		public static EntityField2 Codice
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.Codice);}
		}
		/// <summary>Creates a new ProdottoEntity.CodiceFatturazione field instance</summary>
		public static EntityField2 CodiceFatturazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.CodiceFatturazione);}
		}
		/// <summary>Creates a new ProdottoEntity.Descrizione field instance</summary>
		public static EntityField2 Descrizione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.Descrizione);}
		}
		/// <summary>Creates a new ProdottoEntity.DescrizioneAggiuntivaContratto field instance</summary>
		public static EntityField2 DescrizioneAggiuntivaContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.DescrizioneAggiuntivaContratto);}
		}
		/// <summary>Creates a new ProdottoEntity.EscludiProrata field instance</summary>
		public static EntityField2 EscludiProrata
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.EscludiProrata);}
		}
		/// <summary>Creates a new ProdottoEntity.IdaliquotaIva field instance</summary>
		public static EntityField2 IdaliquotaIva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.IdaliquotaIva);}
		}
		/// <summary>Creates a new ProdottoEntity.IdcategoriaProdotto field instance</summary>
		public static EntityField2 IdcategoriaProdotto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.IdcategoriaProdotto);}
		}
		/// <summary>Creates a new ProdottoEntity.IdgruppoRadius field instance</summary>
		public static EntityField2 IdgruppoRadius
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.IdgruppoRadius);}
		}
		/// <summary>Creates a new ProdottoEntity.IdlistinoVoip field instance</summary>
		public static EntityField2 IdlistinoVoip
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.IdlistinoVoip);}
		}
		/// <summary>Creates a new ProdottoEntity.IdproceduraProvisioningPrdef field instance</summary>
		public static EntityField2 IdproceduraProvisioningPrdef
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.IdproceduraProvisioningPrdef);}
		}
		/// <summary>Creates a new ProdottoEntity.Idprodotto field instance</summary>
		public static EntityField2 Idprodotto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.Idprodotto);}
		}
		/// <summary>Creates a new ProdottoEntity.NomeFileContratto field instance</summary>
		public static EntityField2 NomeFileContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.NomeFileContratto);}
		}
		/// <summary>Creates a new ProdottoEntity.NomeFileSchedaTecnica field instance</summary>
		public static EntityField2 NomeFileSchedaTecnica
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.NomeFileSchedaTecnica);}
		}
		/// <summary>Creates a new ProdottoEntity.NrGgshiftPrimoAddebito field instance</summary>
		public static EntityField2 NrGgshiftPrimoAddebito
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.NrGgshiftPrimoAddebito);}
		}
		/// <summary>Creates a new ProdottoEntity.RadiusGroupName field instance</summary>
		public static EntityField2 RadiusGroupName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.RadiusGroupName);}
		}
		/// <summary>Creates a new ProdottoEntity.RadiusServiceId field instance</summary>
		public static EntityField2 RadiusServiceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.RadiusServiceId);}
		}
		/// <summary>Creates a new ProdottoEntity.RadiusTerminatedServiceId field instance</summary>
		public static EntityField2 RadiusTerminatedServiceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.RadiusTerminatedServiceId);}
		}
		/// <summary>Creates a new ProdottoEntity.SoloAmministrativo field instance</summary>
		public static EntityField2 SoloAmministrativo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProdottoFieldIndex.SoloAmministrativo);}
		}
	}

	/// <summary>Field Creation Class for entity ServizioxcontrattoEntity</summary>
	public partial class ServizioxcontrattoFields
	{
		/// <summary>Creates a new ServizioxcontrattoEntity.CodiceMigrazione field instance</summary>
		public static EntityField2 CodiceMigrazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.CodiceMigrazione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.CodiceRisorsa field instance</summary>
		public static EntityField2 CodiceRisorsa
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.CodiceRisorsa);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.CodiceStatoProvisioning field instance</summary>
		public static EntityField2 CodiceStatoProvisioning
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.CodiceStatoProvisioning);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.CodiceStatoRadius field instance</summary>
		public static EntityField2 CodiceStatoRadius
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.CodiceStatoRadius);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Dac field instance</summary>
		public static EntityField2 Dac
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Dac);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataAttesaCessazione field instance</summary>
		public static EntityField2 DataAttesaCessazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataAttesaCessazione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataDecorrenzaCessazione field instance</summary>
		public static EntityField2 DataDecorrenzaCessazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataDecorrenzaCessazione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataDecorrenzaSospensione field instance</summary>
		public static EntityField2 DataDecorrenzaSospensione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataDecorrenzaSospensione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataInstallazioneApparati field instance</summary>
		public static EntityField2 DataInstallazioneApparati
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataInstallazioneApparati);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataProvisioningEffettiva field instance</summary>
		public static EntityField2 DataProvisioningEffettiva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataProvisioningEffettiva);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataRichiestaCessazione field instance</summary>
		public static EntityField2 DataRichiestaCessazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataRichiestaCessazione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DataUltimoAggiornamentoSegnali field instance</summary>
		public static EntityField2 DataUltimoAggiornamentoSegnali
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DataUltimoAggiornamentoSegnali);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.DescrizioneAggiuntiva field instance</summary>
		public static EntityField2 DescrizioneAggiuntiva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.DescrizioneAggiuntiva);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.FatturatoFinoAl field instance</summary>
		public static EntityField2 FatturatoFinoAl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.FatturatoFinoAl);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.FecodiceCig field instance</summary>
		public static EntityField2 FecodiceCig
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.FecodiceCig);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.FecodiceCup field instance</summary>
		public static EntityField2 FecodiceCup
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.FecodiceCup);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.FedataContratto field instance</summary>
		public static EntityField2 FedataContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.FedataContratto);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.FenumeroContratto field instance</summary>
		public static EntityField2 FenumeroContratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.FenumeroContratto);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.FileSchedaTecnica field instance</summary>
		public static EntityField2 FileSchedaTecnica
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.FileSchedaTecnica);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdaliquotaIva field instance</summary>
		public static EntityField2 IdaliquotaIva
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdaliquotaIva);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Idbts field instance</summary>
		public static EntityField2 Idbts
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Idbts);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdcicloImmissioneInFatturazione field instance</summary>
		public static EntityField2 IdcicloImmissioneInFatturazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdcicloImmissioneInFatturazione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Idcontratto field instance</summary>
		public static EntityField2 Idcontratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Idcontratto);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdentificativoAccesso field instance</summary>
		public static EntityField2 IdentificativoAccesso
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdentificativoAccesso);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Identita field instance</summary>
		public static EntityField2 Identita
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Identita);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdgruppoRadius field instance</summary>
		public static EntityField2 IdgruppoRadius
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdgruppoRadius);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Idinstallatore field instance</summary>
		public static EntityField2 Idinstallatore
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Idinstallatore);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdproceduraProvisioning field instance</summary>
		public static EntityField2 IdproceduraProvisioning
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdproceduraProvisioning);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Idprodotto field instance</summary>
		public static EntityField2 Idprodotto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Idprodotto);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdrichiestaCessazione field instance</summary>
		public static EntityField2 IdrichiestaCessazione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdrichiestaCessazione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdservizioXcontratto field instance</summary>
		public static EntityField2 IdservizioXcontratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdservizioXcontratto);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Idsettore field instance</summary>
		public static EntityField2 Idsettore
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Idsettore);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdstatoServizioXcontratto field instance</summary>
		public static EntityField2 IdstatoServizioXcontratto
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdstatoServizioXcontratto);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.IdtipoSospensione field instance</summary>
		public static EntityField2 IdtipoSospensione
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.IdtipoSospensione);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Idzona field instance</summary>
		public static EntityField2 Idzona
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Idzona);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Latitudine field instance</summary>
		public static EntityField2 Latitudine
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Latitudine);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Longitudine field instance</summary>
		public static EntityField2 Longitudine
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Longitudine);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Macinstallatore field instance</summary>
		public static EntityField2 Macinstallatore
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Macinstallatore);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.MandareRidBanca field instance</summary>
		public static EntityField2 MandareRidBanca
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.MandareRidBanca);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.MultihomeAccessoAttivo field instance</summary>
		public static EntityField2 MultihomeAccessoAttivo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.MultihomeAccessoAttivo);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.NoteCliente field instance</summary>
		public static EntityField2 NoteCliente
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.NoteCliente);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.NoteInstallatore field instance</summary>
		public static EntityField2 NoteInstallatore
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.NoteInstallatore);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.NoteInterne field instance</summary>
		public static EntityField2 NoteInterne
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.NoteInterne);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.NotePartner field instance</summary>
		public static EntityField2 NotePartner
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.NotePartner);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.PasswordAccesso field instance</summary>
		public static EntityField2 PasswordAccesso
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.PasswordAccesso);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.PresospRadiusGroupName field instance</summary>
		public static EntityField2 PresospRadiusGroupName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.PresospRadiusGroupName);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.PresospRadiusServiceId field instance</summary>
		public static EntityField2 PresospRadiusServiceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.PresospRadiusServiceId);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.Prezzo field instance</summary>
		public static EntityField2 Prezzo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.Prezzo);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.SegnaleCh0Rx field instance</summary>
		public static EntityField2 SegnaleCh0Rx
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.SegnaleCh0Rx);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.SegnaleCh0Tx field instance</summary>
		public static EntityField2 SegnaleCh0Tx
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.SegnaleCh0Tx);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.SegnaleCh1Rx field instance</summary>
		public static EntityField2 SegnaleCh1Rx
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.SegnaleCh1Rx);}
		}
		/// <summary>Creates a new ServizioxcontrattoEntity.SegnaleCh1Tx field instance</summary>
		public static EntityField2 SegnaleCh1Tx
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServizioxcontrattoFieldIndex.SegnaleCh1Tx);}
		}
	}
	

}