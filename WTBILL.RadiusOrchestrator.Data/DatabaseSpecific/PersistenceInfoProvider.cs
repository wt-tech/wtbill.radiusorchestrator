﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.Data.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(7);
			InitAttributoradiuEntityMappings();
			InitCategoriaprodottoEntityMappings();
			InitClienteEntityMappings();
			InitContrattoEntityMappings();
			InitGrupporadiuEntityMappings();
			InitProdottoEntityMappings();
			InitServizioxcontrattoEntityMappings();
		}

		/// <summary>Inits AttributoradiuEntity's mappings</summary>
		private void InitAttributoradiuEntityMappings()
		{
			this.AddElementMapping("AttributoradiuEntity", @"wtbilling", @"Default", "attributoradius", 7, 0);
			this.AddElementFieldMapping("AttributoradiuEntity", "IdattributoRadius", "IDAttributoRadius", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AttributoradiuEntity", "IdservizioXcontratto", "IDServizioXContratto", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AttributoradiuEntity", "IsToDelete", "IsToDelete", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("AttributoradiuEntity", "IsToInsert", "IsToInsert", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("AttributoradiuEntity", "NomeAttributo", "NomeAttributo", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AttributoradiuEntity", "Operatore", "Operatore", false, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("AttributoradiuEntity", "Valore", "Valore", false, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 6);
		}

		/// <summary>Inits CategoriaprodottoEntity's mappings</summary>
		private void InitCategoriaprodottoEntityMappings()
		{
			this.AddElementMapping("CategoriaprodottoEntity", @"wtbilling", @"Default", "categoriaprodotto", 10, 0);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "Descrizione", "Descrizione", false, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "IdaliquotaIva", "IDAliquotaIva", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "IdcategoriaProdotto", "IDCategoriaProdotto", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "IdproceduraProvisioningPredef", "IDProceduraProvisioningPredef", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "IdtipoLavorazioneTecnicaAttivazione", "IDTipoLavorazioneTecnicaAttivazione", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "IdtipoLavorazioneTecnicaSync", "IDTipoLavorazioneTecnicaSync", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "IdtipoLavorazioneTecnicaVariazione", "IDTipoLavorazioneTecnicaVariazione", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "OrdineFatturazione", "OrdineFatturazione", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "RadUserPrefisso", "RadUserPrefisso", true, "VarChar", 25, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("CategoriaprodottoEntity", "RadUserSuffisso", "RadUserSuffisso", true, "VarChar", 25, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits ClienteEntity's mappings</summary>
		private void InitClienteEntityMappings()
		{
			this.AddElementMapping("ClienteEntity", @"wtbilling", @"Default", "cliente", 48, 0);
			this.AddElementFieldMapping("ClienteEntity", "Cap", "CAP", true, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ClienteEntity", "Cellulare", "Cellulare", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ClienteEntity", "Cf", "CF", false, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ClienteEntity", "Cfamministratore", "CFAmministratore", true, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ClienteEntity", "Citta", "Citta", true, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ClienteEntity", "CodiceClienteEsterno", "CodiceClienteEsterno", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ClienteEntity", "CodicePaeseIva", "CodicePaeseIVA", true, "VarChar", 4, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ClienteEntity", "Cognome", "Cognome", true, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ClienteEntity", "DataNascita", "DataNascita", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ClienteEntity", "Denominazione", "Denominazione", true, "VarChar", 180, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ClienteEntity", "DocIdnumero", "DocIDNumero", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ClienteEntity", "DocIdrilasciatoDa", "DocIDRilasciatoDa", true, "VarChar", 200, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ClienteEntity", "DocIdrilasciatoIl", "DocIDRilasciatoIl", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ClienteEntity", "Email", "Email", true, "VarChar", 180, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ClienteEntity", "EscludiSolleciti", "EscludiSolleciti", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 14);
			this.AddElementFieldMapping("ClienteEntity", "FecodiceDestinatario", "FECodiceDestinatario", true, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ClienteEntity", "FepecDestinatario", "FEPecDestinatario", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ClienteEntity", "Iban", "IBAN", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("ClienteEntity", "IdaliquotaIva", "IDAliquotaIva", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("ClienteEntity", "Idcliente", "IDCliente", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("ClienteEntity", "IdgruppoCliente", "IDGruppoCliente", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("ClienteEntity", "IdmodalitaInvioFattura", "IDModalitaInvioFattura", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("ClienteEntity", "IdmodalitaPagPreferita", "IDModalitaPagPreferita", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("ClienteEntity", "IdstatoSolvenzaCliente", "IDStatoSolvenzaCliente", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("ClienteEntity", "IdtempiPagamento", "IDTempiPagamento", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("ClienteEntity", "IdtipoArrotondamento", "IDTipoArrotondamento", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("ClienteEntity", "IdtipoCliente", "IDTipoCliente", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("ClienteEntity", "IdtipoDocumentoIdentita", "IDTipoDocumentoIdentita", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("ClienteEntity", "Indirizzo", "Indirizzo", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("ClienteEntity", "InsegnaCommerciale", "InsegnaCommerciale", true, "VarChar", 180, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("ClienteEntity", "LuogoNascita", "LuogoNascita", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("ClienteEntity", "MediaGgpagamentoFattura", "MediaGGPagamentoFattura", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("ClienteEntity", "NazioneIso", "NazioneISO", true, "VarChar", 4, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("ClienteEntity", "Nome", "Nome", true, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("ClienteEntity", "Note", "Note", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("ClienteEntity", "NrFattureEmesse", "NrFattureEmesse", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("ClienteEntity", "NrFattureNonPagate", "NrFattureNonPagate", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("ClienteEntity", "NumeroCivico", "NumeroCivico", true, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("ClienteEntity", "NumSollecitiInviati", "NumSollecitiInviati", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("ClienteEntity", "Password", "Password", true, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("ClienteEntity", "Pec", "PEC", true, "VarChar", 60, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("ClienteEntity", "Piva", "PIVA", true, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("ClienteEntity", "Provincia", "Provincia", true, "VarChar", 2, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("ClienteEntity", "ProvinciaNascita", "ProvinciaNascita", true, "VarChar", 2, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("ClienteEntity", "RecapitoTelefonico", "RecapitoTelefonico", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("ClienteEntity", "SaldoContabile", "SaldoContabile", false, "Decimal", 0, 10, 2, false, "", null, typeof(System.Decimal), 45);
			this.AddElementFieldMapping("ClienteEntity", "UltimoSollecito", "UltimoSollecito", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 46);
			this.AddElementFieldMapping("ClienteEntity", "Username", "Username", true, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 47);
		}

		/// <summary>Inits ContrattoEntity's mappings</summary>
		private void InitContrattoEntityMappings()
		{
			this.AddElementMapping("ContrattoEntity", @"wtbilling", @"Default", "contratto", 40, 0);
			this.AddElementFieldMapping("ContrattoEntity", "CapInstallazione", "CapInstallazione", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ContrattoEntity", "Cartaceo", "Cartaceo", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("ContrattoEntity", "CittaInstallazione", "CittaInstallazione", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ContrattoEntity", "CoordinateInst", "CoordinateInst", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ContrattoEntity", "DataAttivazione", "DataAttivazione", true, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ContrattoEntity", "DataCessazione", "DataCessazione", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ContrattoEntity", "DataFirma", "DataFirma", true, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ContrattoEntity", "DataScadenzaDurataContrattuale", "DataScadenzaDurataContrattuale", true, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ContrattoEntity", "DescrizioneAggiuntiva", "DescrizioneAggiuntiva", true, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ContrattoEntity", "FecodiceCig", "FECodiceCIG", true, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ContrattoEntity", "FecodiceCup", "FECodiceCUP", true, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ContrattoEntity", "FedataContratto", "FEDataContratto", true, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ContrattoEntity", "FenumeroContratto", "FENumeroContratto", true, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ContrattoEntity", "IdcicloPagamento", "IDCicloPagamento", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ContrattoEntity", "Idcliente", "IDCliente", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ContrattoEntity", "Idcontratto", "IDContratto", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ContrattoEntity", "IddurataContrattule", "IDDurataContrattule", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ContrattoEntity", "IdmodalitaPagamentoAttivazione", "IDModalitaPagamentoAttivazione", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ContrattoEntity", "Idpartner", "IDPartner", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("ContrattoEntity", "IdstatoContratto", "IDStatoContratto", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("ContrattoEntity", "IndirizzoInstallazione", "IndirizzoInstallazione", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("ContrattoEntity", "NoteCliente", "NoteCliente", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("ContrattoEntity", "NoteInterne", "NoteInterne", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("ContrattoEntity", "NotePartner", "NotePartner", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("ContrattoEntity", "Numero", "Numero", true, "VarChar", 20, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("ContrattoEntity", "ProvinciaInstallazione", "ProvinciaInstallazione", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("ContrattoEntity", "WhsaziendaClienteFinale", "WHSAziendaClienteFinale", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("ContrattoEntity", "Whscap", "WHSCap", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("ContrattoEntity", "WhscfclienteFinale", "WHSCFClienteFinale", true, "VarChar", 25, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("ContrattoEntity", "Whscitta", "WHSCitta", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("ContrattoEntity", "Whscivico", "WHSCivico", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("ContrattoEntity", "WhscognomeClienteFinale", "WHSCognomeClienteFinale", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("ContrattoEntity", "Whsindirizzo", "WHSIndirizzo", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("ContrattoEntity", "WhsnomeClienteFinale", "WHSNomeClienteFinale", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("ContrattoEntity", "Whsnote", "WHSNote", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("ContrattoEntity", "WhspartitaIvaclienteFinale", "WHSPartitaIVAClienteFinale", true, "VarChar", 25, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("ContrattoEntity", "Whsprovincia", "WHSProvincia", true, "VarChar", 2, 0, 0, false, "", null, typeof(System.String), 36);
			this.AddElementFieldMapping("ContrattoEntity", "WhsrecapitoReferente1", "WHSRecapitoReferente1", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("ContrattoEntity", "WhsrecapitoReferente2", "WHSRecapitoReferente2", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("ContrattoEntity", "Whstoponimo", "WHSToponimo", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 39);
		}

		/// <summary>Inits GrupporadiuEntity's mappings</summary>
		private void InitGrupporadiuEntityMappings()
		{
			this.AddElementMapping("GrupporadiuEntity", @"wtbilling", @"Default", "grupporadius", 2, 0);
			this.AddElementFieldMapping("GrupporadiuEntity", "GruppoRadius", "GruppoRadius", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("GrupporadiuEntity", "IdgruppoRadius", "IDGruppoRadius", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 1);
		}

		/// <summary>Inits ProdottoEntity's mappings</summary>
		private void InitProdottoEntityMappings()
		{
			this.AddElementMapping("ProdottoEntity", @"wtbilling", @"Default", "prodotto", 19, 0);
			this.AddElementFieldMapping("ProdottoEntity", "Acquistabile", "Acquistabile", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("ProdottoEntity", "Codice", "Codice", false, "VarChar", 25, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ProdottoEntity", "CodiceFatturazione", "CodiceFatturazione", true, "VarChar", 25, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ProdottoEntity", "Descrizione", "Descrizione", false, "VarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ProdottoEntity", "DescrizioneAggiuntivaContratto", "DescrizioneAggiuntivaContratto", true, "VarChar", 200, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ProdottoEntity", "EscludiProrata", "EscludiProrata", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("ProdottoEntity", "IdaliquotaIva", "IDAliquotaIva", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProdottoEntity", "IdcategoriaProdotto", "IDCategoriaProdotto", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ProdottoEntity", "IdgruppoRadius", "IDGruppoRadius", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ProdottoEntity", "IdlistinoVoip", "IDListinoVoip", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ProdottoEntity", "IdproceduraProvisioningPrdef", "IDProceduraProvisioningPrdef", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ProdottoEntity", "Idprodotto", "IDProdotto", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ProdottoEntity", "NomeFileContratto", "NomeFileContratto", true, "VarChar", 200, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ProdottoEntity", "NomeFileSchedaTecnica", "NomeFileSchedaTecnica", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ProdottoEntity", "NrGgshiftPrimoAddebito", "NrGGShiftPrimoAddebito", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ProdottoEntity", "RadiusGroupName", "RadiusGroupName", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ProdottoEntity", "RadiusServiceId", "RadiusServiceID", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ProdottoEntity", "RadiusTerminatedServiceId", "RadiusTerminatedServiceID", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ProdottoEntity", "SoloAmministrativo", "SoloAmministrativo", true, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 18);
		}

		/// <summary>Inits ServizioxcontrattoEntity's mappings</summary>
		private void InitServizioxcontrattoEntityMappings()
		{
			this.AddElementMapping("ServizioxcontrattoEntity", @"wtbilling", @"Default", "servizioxcontratto", 52, 0);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "CodiceMigrazione", "CodiceMigrazione", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "CodiceRisorsa", "CodiceRisorsa", true, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "CodiceStatoProvisioning", "CodiceStatoProvisioning", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "CodiceStatoRadius", "CodiceStatoRadius", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Dac", "DAC", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataAttesaCessazione", "DataAttesaCessazione", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataDecorrenzaCessazione", "DataDecorrenzaCessazione", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataDecorrenzaSospensione", "DataDecorrenzaSospensione", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataInstallazioneApparati", "DataInstallazioneApparati", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataProvisioningEffettiva", "DataProvisioningEffettiva", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataRichiestaCessazione", "DataRichiestaCessazione", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DataUltimoAggiornamentoSegnali", "DataUltimoAggiornamentoSegnali", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "DescrizioneAggiuntiva", "DescrizioneAggiuntiva", true, "VarChar", 200, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "FatturatoFinoAl", "FatturatoFinoAl", true, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "FecodiceCig", "FECodiceCIG", true, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "FecodiceCup", "FECodiceCUP", true, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "FedataContratto", "FEDataContratto", true, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "FenumeroContratto", "FENumeroContratto", true, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "FileSchedaTecnica", "FileSchedaTecnica", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdaliquotaIva", "IDAliquotaIVA", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Idbts", "IDBts", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdcicloImmissioneInFatturazione", "IDCicloImmissioneInFatturazione", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Idcontratto", "IDContratto", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdentificativoAccesso", "IdentificativoAccesso", true, "VarChar", 60, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Identita", "Identita", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdgruppoRadius", "IDGruppoRadius", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Idinstallatore", "IDInstallatore", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdproceduraProvisioning", "IDProceduraProvisioning", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Idprodotto", "IDProdotto", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdrichiestaCessazione", "IDRichiestaCessazione", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdservizioXcontratto", "IDServizioXContratto", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Idsettore", "IDSettore", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdstatoServizioXcontratto", "IDStatoServizioXContratto", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "IdtipoSospensione", "IDTipoSospensione", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Idzona", "IDZona", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Latitudine", "Latitudine", true, "Decimal", 0, 10, 6, false, "", null, typeof(System.Decimal), 35);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Longitudine", "Longitudine", true, "Decimal", 0, 10, 6, false, "", null, typeof(System.Decimal), 36);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Macinstallatore", "MACInstallatore", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "MandareRidBanca", "MandareRidBanca", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 38);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "MultihomeAccessoAttivo", "MultihomeAccessoAttivo", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 39);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "NoteCliente", "NoteCliente", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "NoteInstallatore", "NoteInstallatore", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "NoteInterne", "NoteInterne", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "NotePartner", "NotePartner", true, "Text", 65535, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "PasswordAccesso", "PasswordAccesso", true, "VarChar", 60, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "PresospRadiusGroupName", "PresospRadiusGroupName", true, "VarChar", 80, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "PresospRadiusServiceId", "PresospRadiusServiceID", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "Prezzo", "Prezzo", false, "Decimal", 0, 10, 2, false, "", null, typeof(System.Decimal), 47);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "SegnaleCh0Rx", "SegnaleCh0Rx", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "SegnaleCh0Tx", "SegnaleCh0Tx", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "SegnaleCh1Rx", "SegnaleCh1Rx", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 50);
			this.AddElementFieldMapping("ServizioxcontrattoEntity", "SegnaleCh1Tx", "SegnaleCh1Tx", true, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 51);
		}

	}
}
