﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.HelperClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'RmUser'.<br/><br/></summary>
	[Serializable]
	public partial class RmUserEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RmUserEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public RmUserEntity():base("RmUserEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RmUserEntity(IEntityFields2 fields):base("RmUserEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RmUserEntity</param>
		public RmUserEntity(IValidator validator):base("RmUserEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="username">PK value for RmUser which data should be fetched into this RmUser object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RmUserEntity(System.String username):base("RmUserEntity")
		{
			InitClassEmpty(null, null);
			this.Username = username;
		}

		/// <summary> CTor</summary>
		/// <param name="username">PK value for RmUser which data should be fetched into this RmUser object</param>
		/// <param name="validator">The custom validator object for this RmUserEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RmUserEntity(System.String username, IValidator validator):base("RmUserEntity")
		{
			InitClassEmpty(validator, null);
			this.Username = username;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected RmUserEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RmUserRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(RmUserEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Acctype", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Actcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Address", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Alertemail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Alertsms", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cardfails", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Comblimit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Comment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Company", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Contractid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Contractvalid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Country", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Createdby", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Createdon", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Credits", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Custattr", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Downlimit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Enableuser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Expiration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Firstname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Gpslat", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Gpslong", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Groupid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ipmodecm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ipmodecpe", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lang", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lastlogoff", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lastname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Mac", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Maccm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Mobile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Owner", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phone", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Poolidcm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Poolidcpe", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Pswactsmsnum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Selfreg", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Srvid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Staticipcm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Staticipcpe", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Taxid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Uplimit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Uptimelimit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Usemacauth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Username", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Verified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Verifycode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Verifyfails", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Verifymobile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Verifysentnum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Warningsent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Zip", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RmUserEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RmUserRelations Relations
		{
			get	{ return new RmUserRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Acctype property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."acctype"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Acctype
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Acctype, true); }
			set	{ SetValue((int)RmUserFieldIndex.Acctype, value); }
		}

		/// <summary> The Actcode property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."actcode"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 60<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Actcode
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Actcode, true); }
			set	{ SetValue((int)RmUserFieldIndex.Actcode, value); }
		}

		/// <summary> The Address property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."address"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Address
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Address, true); }
			set	{ SetValue((int)RmUserFieldIndex.Address, value); }
		}

		/// <summary> The Alertemail property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."alertemail"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Alertemail
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Alertemail, true); }
			set	{ SetValue((int)RmUserFieldIndex.Alertemail, value); }
		}

		/// <summary> The Alertsms property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."alertsms"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Alertsms
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Alertsms, true); }
			set	{ SetValue((int)RmUserFieldIndex.Alertsms, value); }
		}

		/// <summary> The Cardfails property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."cardfails"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Cardfails
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Cardfails, true); }
			set	{ SetValue((int)RmUserFieldIndex.Cardfails, value); }
		}

		/// <summary> The City property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."city"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.City, true); }
			set	{ SetValue((int)RmUserFieldIndex.City, value); }
		}

		/// <summary> The Comblimit property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."comblimit"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Comblimit
		{
			get { return (System.Int64)GetValue((int)RmUserFieldIndex.Comblimit, true); }
			set	{ SetValue((int)RmUserFieldIndex.Comblimit, value); }
		}

		/// <summary> The Comment property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."comment"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Comment
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Comment, true); }
			set	{ SetValue((int)RmUserFieldIndex.Comment, value); }
		}

		/// <summary> The Company property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."company"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Company
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Company, true); }
			set	{ SetValue((int)RmUserFieldIndex.Company, value); }
		}

		/// <summary> The Contractid property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."contractid"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Contractid
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Contractid, true); }
			set	{ SetValue((int)RmUserFieldIndex.Contractid, value); }
		}

		/// <summary> The Contractvalid property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."contractvalid"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Contractvalid
		{
			get { return (System.DateTime)GetValue((int)RmUserFieldIndex.Contractvalid, true); }
			set	{ SetValue((int)RmUserFieldIndex.Contractvalid, value); }
		}

		/// <summary> The Country property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."country"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Country
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Country, true); }
			set	{ SetValue((int)RmUserFieldIndex.Country, value); }
		}

		/// <summary> The Createdby property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."createdby"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 64<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Createdby
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Createdby, true); }
			set	{ SetValue((int)RmUserFieldIndex.Createdby, value); }
		}

		/// <summary> The Createdon property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."createdon"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Createdon
		{
			get { return (System.DateTime)GetValue((int)RmUserFieldIndex.Createdon, true); }
			set	{ SetValue((int)RmUserFieldIndex.Createdon, value); }
		}

		/// <summary> The Credits property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."credits"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Credits
		{
			get { return (System.Decimal)GetValue((int)RmUserFieldIndex.Credits, true); }
			set	{ SetValue((int)RmUserFieldIndex.Credits, value); }
		}

		/// <summary> The Custattr property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."custattr"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Custattr
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Custattr, true); }
			set	{ SetValue((int)RmUserFieldIndex.Custattr, value); }
		}

		/// <summary> The Downlimit property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."downlimit"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Downlimit
		{
			get { return (System.Int64)GetValue((int)RmUserFieldIndex.Downlimit, true); }
			set	{ SetValue((int)RmUserFieldIndex.Downlimit, value); }
		}

		/// <summary> The Email property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."email"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Email, true); }
			set	{ SetValue((int)RmUserFieldIndex.Email, value); }
		}

		/// <summary> The Enableuser property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."enableuser"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Enableuser
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Enableuser, true); }
			set	{ SetValue((int)RmUserFieldIndex.Enableuser, value); }
		}

		/// <summary> The Expiration property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."expiration"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Expiration
		{
			get { return (System.DateTime)GetValue((int)RmUserFieldIndex.Expiration, true); }
			set	{ SetValue((int)RmUserFieldIndex.Expiration, value); }
		}

		/// <summary> The Firstname property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."firstname"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Firstname
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Firstname, true); }
			set	{ SetValue((int)RmUserFieldIndex.Firstname, value); }
		}

		/// <summary> The Gpslat property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."gpslat"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 17, 14, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Gpslat
		{
			get { return (System.Decimal)GetValue((int)RmUserFieldIndex.Gpslat, true); }
			set	{ SetValue((int)RmUserFieldIndex.Gpslat, value); }
		}

		/// <summary> The Gpslong property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."gpslong"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 17, 14, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Gpslong
		{
			get { return (System.Decimal)GetValue((int)RmUserFieldIndex.Gpslong, true); }
			set	{ SetValue((int)RmUserFieldIndex.Gpslong, value); }
		}

		/// <summary> The Groupid property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."groupid"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Groupid
		{
			get { return (System.Int32)GetValue((int)RmUserFieldIndex.Groupid, true); }
			set	{ SetValue((int)RmUserFieldIndex.Groupid, value); }
		}

		/// <summary> The Ipmodecm property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."ipmodecm"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Ipmodecm
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Ipmodecm, true); }
			set	{ SetValue((int)RmUserFieldIndex.Ipmodecm, value); }
		}

		/// <summary> The Ipmodecpe property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."ipmodecpe"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Ipmodecpe
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Ipmodecpe, true); }
			set	{ SetValue((int)RmUserFieldIndex.Ipmodecpe, value); }
		}

		/// <summary> The Lang property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."lang"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Lang
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Lang, true); }
			set	{ SetValue((int)RmUserFieldIndex.Lang, value); }
		}

		/// <summary> The Lastlogoff property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."lastlogoff"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Lastlogoff
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RmUserFieldIndex.Lastlogoff, false); }
			set	{ SetValue((int)RmUserFieldIndex.Lastlogoff, value); }
		}

		/// <summary> The Lastname property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."lastname"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Lastname
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Lastname, true); }
			set	{ SetValue((int)RmUserFieldIndex.Lastname, value); }
		}

		/// <summary> The Mac property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."mac"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 17<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Mac
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Mac, true); }
			set	{ SetValue((int)RmUserFieldIndex.Mac, value); }
		}

		/// <summary> The Maccm property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."maccm"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 17<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Maccm
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Maccm, true); }
			set	{ SetValue((int)RmUserFieldIndex.Maccm, value); }
		}

		/// <summary> The Mobile property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."mobile"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Mobile
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Mobile, true); }
			set	{ SetValue((int)RmUserFieldIndex.Mobile, value); }
		}

		/// <summary> The Owner property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."owner"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 64<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Owner
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Owner, true); }
			set	{ SetValue((int)RmUserFieldIndex.Owner, value); }
		}

		/// <summary> The Password property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."password"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Password, true); }
			set	{ SetValue((int)RmUserFieldIndex.Password, value); }
		}

		/// <summary> The Phone property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."phone"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Phone
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Phone, true); }
			set	{ SetValue((int)RmUserFieldIndex.Phone, value); }
		}

		/// <summary> The Poolidcm property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."poolidcm"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Poolidcm
		{
			get { return (System.Int32)GetValue((int)RmUserFieldIndex.Poolidcm, true); }
			set	{ SetValue((int)RmUserFieldIndex.Poolidcm, value); }
		}

		/// <summary> The Poolidcpe property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."poolidcpe"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Poolidcpe
		{
			get { return (System.Int32)GetValue((int)RmUserFieldIndex.Poolidcpe, true); }
			set	{ SetValue((int)RmUserFieldIndex.Poolidcpe, value); }
		}

		/// <summary> The Pswactsmsnum property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."pswactsmsnum"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Pswactsmsnum
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Pswactsmsnum, true); }
			set	{ SetValue((int)RmUserFieldIndex.Pswactsmsnum, value); }
		}

		/// <summary> The Selfreg property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."selfreg"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Selfreg
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Selfreg, true); }
			set	{ SetValue((int)RmUserFieldIndex.Selfreg, value); }
		}

		/// <summary> The Srvid property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."srvid"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 11, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Srvid
		{
			get { return (System.Int32)GetValue((int)RmUserFieldIndex.Srvid, true); }
			set	{ SetValue((int)RmUserFieldIndex.Srvid, value); }
		}

		/// <summary> The State property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."state"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String State
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.State, true); }
			set	{ SetValue((int)RmUserFieldIndex.State, value); }
		}

		/// <summary> The Staticipcm property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."staticipcm"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Staticipcm
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Staticipcm, true); }
			set	{ SetValue((int)RmUserFieldIndex.Staticipcm, value); }
		}

		/// <summary> The Staticipcpe property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."staticipcpe"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Staticipcpe
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Staticipcpe, true); }
			set	{ SetValue((int)RmUserFieldIndex.Staticipcpe, value); }
		}

		/// <summary> The Taxid property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."taxid"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Taxid
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Taxid, true); }
			set	{ SetValue((int)RmUserFieldIndex.Taxid, value); }
		}

		/// <summary> The Uplimit property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."uplimit"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Uplimit
		{
			get { return (System.Int64)GetValue((int)RmUserFieldIndex.Uplimit, true); }
			set	{ SetValue((int)RmUserFieldIndex.Uplimit, value); }
		}

		/// <summary> The Uptimelimit property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."uptimelimit"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Uptimelimit
		{
			get { return (System.Int64)GetValue((int)RmUserFieldIndex.Uptimelimit, true); }
			set	{ SetValue((int)RmUserFieldIndex.Uptimelimit, value); }
		}

		/// <summary> The Usemacauth property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."usemacauth"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Usemacauth
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Usemacauth, true); }
			set	{ SetValue((int)RmUserFieldIndex.Usemacauth, value); }
		}

		/// <summary> The Username property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."username"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 64<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String Username
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Username, true); }
			set	{ SetValue((int)RmUserFieldIndex.Username, value); }
		}

		/// <summary> The Verified property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."verified"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Verified
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Verified, true); }
			set	{ SetValue((int)RmUserFieldIndex.Verified, value); }
		}

		/// <summary> The Verifycode property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."verifycode"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Verifycode
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Verifycode, true); }
			set	{ SetValue((int)RmUserFieldIndex.Verifycode, value); }
		}

		/// <summary> The Verifyfails property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."verifyfails"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Verifyfails
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Verifyfails, true); }
			set	{ SetValue((int)RmUserFieldIndex.Verifyfails, value); }
		}

		/// <summary> The Verifymobile property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."verifymobile"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Verifymobile
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Verifymobile, true); }
			set	{ SetValue((int)RmUserFieldIndex.Verifymobile, value); }
		}

		/// <summary> The Verifysentnum property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."verifysentnum"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Verifysentnum
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Verifysentnum, true); }
			set	{ SetValue((int)RmUserFieldIndex.Verifysentnum, value); }
		}

		/// <summary> The Warningsent property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."warningsent"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Warningsent
		{
			get { return (System.Int16)GetValue((int)RmUserFieldIndex.Warningsent, true); }
			set	{ SetValue((int)RmUserFieldIndex.Warningsent, value); }
		}

		/// <summary> The Zip property of the Entity RmUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "rm_users"."zip"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 8<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Zip
		{
			get { return (System.String)GetValue((int)RmUserFieldIndex.Zip, true); }
			set	{ SetValue((int)RmUserFieldIndex.Zip, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.EntityType.RmUserEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
