﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.FactoryClasses;
using WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data;

namespace WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity RadcheckEntity</summary>
	public partial class RadcheckFields
	{
		/// <summary>Creates a new RadcheckEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadcheckEntity.Enabled field instance</summary>
		public static EntityField2 Enabled
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Enabled);}
		}
		/// <summary>Creates a new RadcheckEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Id);}
		}
		/// <summary>Creates a new RadcheckEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Op);}
		}
		/// <summary>Creates a new RadcheckEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Username);}
		}
		/// <summary>Creates a new RadcheckEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Value);}
		}
		/// <summary>Creates a new RadcheckEntity.Virtualdeleted field instance</summary>
		public static EntityField2 Virtualdeleted
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadcheckFieldIndex.Virtualdeleted);}
		}
	}

	/// <summary>Field Creation Class for entity RadgroupreplyEntity</summary>
	public partial class RadgroupreplyFields
	{
		/// <summary>Creates a new RadgroupreplyEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Groupname field instance</summary>
		public static EntityField2 Groupname
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Groupname);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Id);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Op);}
		}
		/// <summary>Creates a new RadgroupreplyEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadgroupreplyFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity RadpostauthEntity</summary>
	public partial class RadpostauthFields
	{
		/// <summary>Creates a new RadpostauthEntity.Authdate field instance</summary>
		public static EntityField2 Authdate
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Authdate);}
		}
		/// <summary>Creates a new RadpostauthEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Id);}
		}
		/// <summary>Creates a new RadpostauthEntity.Nasipaddress field instance</summary>
		public static EntityField2 Nasipaddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Nasipaddress);}
		}
		/// <summary>Creates a new RadpostauthEntity.Pass field instance</summary>
		public static EntityField2 Pass
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Pass);}
		}
		/// <summary>Creates a new RadpostauthEntity.Reply field instance</summary>
		public static EntityField2 Reply
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Reply);}
		}
		/// <summary>Creates a new RadpostauthEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadpostauthFieldIndex.Username);}
		}
	}

	/// <summary>Field Creation Class for entity RadreplyEntity</summary>
	public partial class RadreplyFields
	{
		/// <summary>Creates a new RadreplyEntity.Attribute field instance</summary>
		public static EntityField2 Attribute
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Attribute);}
		}
		/// <summary>Creates a new RadreplyEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Id);}
		}
		/// <summary>Creates a new RadreplyEntity.Op field instance</summary>
		public static EntityField2 Op
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Op);}
		}
		/// <summary>Creates a new RadreplyEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Username);}
		}
		/// <summary>Creates a new RadreplyEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RadreplyFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity RmUserEntity</summary>
	public partial class RmUserFields
	{
		/// <summary>Creates a new RmUserEntity.Acctype field instance</summary>
		public static EntityField2 Acctype
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Acctype);}
		}
		/// <summary>Creates a new RmUserEntity.Actcode field instance</summary>
		public static EntityField2 Actcode
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Actcode);}
		}
		/// <summary>Creates a new RmUserEntity.Address field instance</summary>
		public static EntityField2 Address
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Address);}
		}
		/// <summary>Creates a new RmUserEntity.Alertemail field instance</summary>
		public static EntityField2 Alertemail
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Alertemail);}
		}
		/// <summary>Creates a new RmUserEntity.Alertsms field instance</summary>
		public static EntityField2 Alertsms
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Alertsms);}
		}
		/// <summary>Creates a new RmUserEntity.Cardfails field instance</summary>
		public static EntityField2 Cardfails
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Cardfails);}
		}
		/// <summary>Creates a new RmUserEntity.City field instance</summary>
		public static EntityField2 City
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.City);}
		}
		/// <summary>Creates a new RmUserEntity.Comblimit field instance</summary>
		public static EntityField2 Comblimit
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Comblimit);}
		}
		/// <summary>Creates a new RmUserEntity.Comment field instance</summary>
		public static EntityField2 Comment
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Comment);}
		}
		/// <summary>Creates a new RmUserEntity.Company field instance</summary>
		public static EntityField2 Company
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Company);}
		}
		/// <summary>Creates a new RmUserEntity.Contractid field instance</summary>
		public static EntityField2 Contractid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Contractid);}
		}
		/// <summary>Creates a new RmUserEntity.Contractvalid field instance</summary>
		public static EntityField2 Contractvalid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Contractvalid);}
		}
		/// <summary>Creates a new RmUserEntity.Country field instance</summary>
		public static EntityField2 Country
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Country);}
		}
		/// <summary>Creates a new RmUserEntity.Createdby field instance</summary>
		public static EntityField2 Createdby
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Createdby);}
		}
		/// <summary>Creates a new RmUserEntity.Createdon field instance</summary>
		public static EntityField2 Createdon
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Createdon);}
		}
		/// <summary>Creates a new RmUserEntity.Credits field instance</summary>
		public static EntityField2 Credits
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Credits);}
		}
		/// <summary>Creates a new RmUserEntity.Custattr field instance</summary>
		public static EntityField2 Custattr
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Custattr);}
		}
		/// <summary>Creates a new RmUserEntity.Downlimit field instance</summary>
		public static EntityField2 Downlimit
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Downlimit);}
		}
		/// <summary>Creates a new RmUserEntity.Email field instance</summary>
		public static EntityField2 Email
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Email);}
		}
		/// <summary>Creates a new RmUserEntity.Enableuser field instance</summary>
		public static EntityField2 Enableuser
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Enableuser);}
		}
		/// <summary>Creates a new RmUserEntity.Expiration field instance</summary>
		public static EntityField2 Expiration
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Expiration);}
		}
		/// <summary>Creates a new RmUserEntity.Firstname field instance</summary>
		public static EntityField2 Firstname
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Firstname);}
		}
		/// <summary>Creates a new RmUserEntity.Gpslat field instance</summary>
		public static EntityField2 Gpslat
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Gpslat);}
		}
		/// <summary>Creates a new RmUserEntity.Gpslong field instance</summary>
		public static EntityField2 Gpslong
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Gpslong);}
		}
		/// <summary>Creates a new RmUserEntity.Groupid field instance</summary>
		public static EntityField2 Groupid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Groupid);}
		}
		/// <summary>Creates a new RmUserEntity.Ipmodecm field instance</summary>
		public static EntityField2 Ipmodecm
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Ipmodecm);}
		}
		/// <summary>Creates a new RmUserEntity.Ipmodecpe field instance</summary>
		public static EntityField2 Ipmodecpe
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Ipmodecpe);}
		}
		/// <summary>Creates a new RmUserEntity.Lang field instance</summary>
		public static EntityField2 Lang
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Lang);}
		}
		/// <summary>Creates a new RmUserEntity.Lastlogoff field instance</summary>
		public static EntityField2 Lastlogoff
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Lastlogoff);}
		}
		/// <summary>Creates a new RmUserEntity.Lastname field instance</summary>
		public static EntityField2 Lastname
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Lastname);}
		}
		/// <summary>Creates a new RmUserEntity.Mac field instance</summary>
		public static EntityField2 Mac
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Mac);}
		}
		/// <summary>Creates a new RmUserEntity.Maccm field instance</summary>
		public static EntityField2 Maccm
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Maccm);}
		}
		/// <summary>Creates a new RmUserEntity.Mobile field instance</summary>
		public static EntityField2 Mobile
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Mobile);}
		}
		/// <summary>Creates a new RmUserEntity.Owner field instance</summary>
		public static EntityField2 Owner
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Owner);}
		}
		/// <summary>Creates a new RmUserEntity.Password field instance</summary>
		public static EntityField2 Password
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Password);}
		}
		/// <summary>Creates a new RmUserEntity.Phone field instance</summary>
		public static EntityField2 Phone
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Phone);}
		}
		/// <summary>Creates a new RmUserEntity.Poolidcm field instance</summary>
		public static EntityField2 Poolidcm
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Poolidcm);}
		}
		/// <summary>Creates a new RmUserEntity.Poolidcpe field instance</summary>
		public static EntityField2 Poolidcpe
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Poolidcpe);}
		}
		/// <summary>Creates a new RmUserEntity.Pswactsmsnum field instance</summary>
		public static EntityField2 Pswactsmsnum
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Pswactsmsnum);}
		}
		/// <summary>Creates a new RmUserEntity.Selfreg field instance</summary>
		public static EntityField2 Selfreg
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Selfreg);}
		}
		/// <summary>Creates a new RmUserEntity.Srvid field instance</summary>
		public static EntityField2 Srvid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Srvid);}
		}
		/// <summary>Creates a new RmUserEntity.State field instance</summary>
		public static EntityField2 State
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.State);}
		}
		/// <summary>Creates a new RmUserEntity.Staticipcm field instance</summary>
		public static EntityField2 Staticipcm
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Staticipcm);}
		}
		/// <summary>Creates a new RmUserEntity.Staticipcpe field instance</summary>
		public static EntityField2 Staticipcpe
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Staticipcpe);}
		}
		/// <summary>Creates a new RmUserEntity.Taxid field instance</summary>
		public static EntityField2 Taxid
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Taxid);}
		}
		/// <summary>Creates a new RmUserEntity.Uplimit field instance</summary>
		public static EntityField2 Uplimit
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Uplimit);}
		}
		/// <summary>Creates a new RmUserEntity.Uptimelimit field instance</summary>
		public static EntityField2 Uptimelimit
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Uptimelimit);}
		}
		/// <summary>Creates a new RmUserEntity.Usemacauth field instance</summary>
		public static EntityField2 Usemacauth
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Usemacauth);}
		}
		/// <summary>Creates a new RmUserEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Username);}
		}
		/// <summary>Creates a new RmUserEntity.Verified field instance</summary>
		public static EntityField2 Verified
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Verified);}
		}
		/// <summary>Creates a new RmUserEntity.Verifycode field instance</summary>
		public static EntityField2 Verifycode
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Verifycode);}
		}
		/// <summary>Creates a new RmUserEntity.Verifyfails field instance</summary>
		public static EntityField2 Verifyfails
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Verifyfails);}
		}
		/// <summary>Creates a new RmUserEntity.Verifymobile field instance</summary>
		public static EntityField2 Verifymobile
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Verifymobile);}
		}
		/// <summary>Creates a new RmUserEntity.Verifysentnum field instance</summary>
		public static EntityField2 Verifysentnum
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Verifysentnum);}
		}
		/// <summary>Creates a new RmUserEntity.Warningsent field instance</summary>
		public static EntityField2 Warningsent
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Warningsent);}
		}
		/// <summary>Creates a new RmUserEntity.Zip field instance</summary>
		public static EntityField2 Zip
		{
			get { return (EntityField2)EntityFieldFactory.Create(RmUserFieldIndex.Zip);}
		}
	}
	

}