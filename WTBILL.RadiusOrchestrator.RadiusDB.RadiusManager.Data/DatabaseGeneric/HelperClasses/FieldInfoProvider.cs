﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (5 + 0));
			InitRadcheckEntityInfos();
			InitRadgroupreplyEntityInfos();
			InitRadpostauthEntityInfos();
			InitRadreplyEntityInfos();
			InitRmUserEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits RadcheckEntity's FieldInfo objects</summary>
		private void InitRadcheckEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadcheckFieldIndex), "RadcheckEntity");
			this.AddElementFieldInfo("RadcheckEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Attribute, 64, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Enabled", typeof(System.Int16), false, false, false, false,  (int)RadcheckFieldIndex.Enabled, 0, 0, 1);
			this.AddElementFieldInfo("RadcheckEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadcheckFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadcheckEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Username, 64, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadcheckFieldIndex.Value, 253, 0, 0);
			this.AddElementFieldInfo("RadcheckEntity", "Virtualdeleted", typeof(System.Int16), false, false, false, false,  (int)RadcheckFieldIndex.Virtualdeleted, 0, 0, 1);
		}
		/// <summary>Inits RadgroupreplyEntity's FieldInfo objects</summary>
		private void InitRadgroupreplyEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadgroupreplyFieldIndex), "RadgroupreplyEntity");
			this.AddElementFieldInfo("RadgroupreplyEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Attribute, 64, 0, 0);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Groupname", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Groupname, 64, 0, 0);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadgroupreplyFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadgroupreplyEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadgroupreplyFieldIndex.Value, 253, 0, 0);
		}
		/// <summary>Inits RadpostauthEntity's FieldInfo objects</summary>
		private void InitRadpostauthEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadpostauthFieldIndex), "RadpostauthEntity");
			this.AddElementFieldInfo("RadpostauthEntity", "Authdate", typeof(System.DateTime), false, false, false, false,  (int)RadpostauthFieldIndex.Authdate, 0, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)RadpostauthFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadpostauthEntity", "Nasipaddress", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Nasipaddress, 15, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Pass", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Pass, 64, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Reply", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Reply, 32, 0, 0);
			this.AddElementFieldInfo("RadpostauthEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadpostauthFieldIndex.Username, 64, 0, 0);
		}
		/// <summary>Inits RadreplyEntity's FieldInfo objects</summary>
		private void InitRadreplyEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RadreplyFieldIndex), "RadreplyEntity");
			this.AddElementFieldInfo("RadreplyEntity", "Attribute", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Attribute, 64, 0, 0);
			this.AddElementFieldInfo("RadreplyEntity", "Id", typeof(System.Int64), true, false, true, false,  (int)RadreplyFieldIndex.Id, 0, 0, 11);
			this.AddElementFieldInfo("RadreplyEntity", "Op", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Op, 2, 0, 0);
			this.AddElementFieldInfo("RadreplyEntity", "Username", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Username, 64, 0, 0);
			this.AddElementFieldInfo("RadreplyEntity", "Value", typeof(System.String), false, false, false, false,  (int)RadreplyFieldIndex.Value, 253, 0, 0);
		}
		/// <summary>Inits RmUserEntity's FieldInfo objects</summary>
		private void InitRmUserEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RmUserFieldIndex), "RmUserEntity");
			this.AddElementFieldInfo("RmUserEntity", "Acctype", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Acctype, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Actcode", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Actcode, 60, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Address", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Address, 100, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Alertemail", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Alertemail, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Alertsms", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Alertsms, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Cardfails", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Cardfails, 0, 0, 4);
			this.AddElementFieldInfo("RmUserEntity", "City", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.City, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Comblimit", typeof(System.Int64), false, false, false, false,  (int)RmUserFieldIndex.Comblimit, 0, 0, 20);
			this.AddElementFieldInfo("RmUserEntity", "Comment", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Comment, 500, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Company", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Company, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Contractid", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Contractid, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Contractvalid", typeof(System.DateTime), false, false, false, false,  (int)RmUserFieldIndex.Contractvalid, 0, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Country", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Country, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Createdby", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Createdby, 64, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Createdon", typeof(System.DateTime), false, false, false, false,  (int)RmUserFieldIndex.Createdon, 0, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Credits", typeof(System.Decimal), false, false, false, false,  (int)RmUserFieldIndex.Credits, 0, 2, 20);
			this.AddElementFieldInfo("RmUserEntity", "Custattr", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Custattr, 255, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Downlimit", typeof(System.Int64), false, false, false, false,  (int)RmUserFieldIndex.Downlimit, 0, 0, 20);
			this.AddElementFieldInfo("RmUserEntity", "Email", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Email, 100, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Enableuser", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Enableuser, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Expiration", typeof(System.DateTime), false, false, false, false,  (int)RmUserFieldIndex.Expiration, 0, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Firstname", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Firstname, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Gpslat", typeof(System.Decimal), false, false, false, false,  (int)RmUserFieldIndex.Gpslat, 0, 14, 17);
			this.AddElementFieldInfo("RmUserEntity", "Gpslong", typeof(System.Decimal), false, false, false, false,  (int)RmUserFieldIndex.Gpslong, 0, 14, 17);
			this.AddElementFieldInfo("RmUserEntity", "Groupid", typeof(System.Int32), false, false, false, false,  (int)RmUserFieldIndex.Groupid, 0, 0, 11);
			this.AddElementFieldInfo("RmUserEntity", "Ipmodecm", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Ipmodecm, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Ipmodecpe", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Ipmodecpe, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Lang", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Lang, 30, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Lastlogoff", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RmUserFieldIndex.Lastlogoff, 0, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Lastname", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Lastname, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Mac", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Mac, 17, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Maccm", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Maccm, 17, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Mobile", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Mobile, 15, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Owner", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Owner, 64, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Password", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Password, 32, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Phone", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Phone, 15, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Poolidcm", typeof(System.Int32), false, false, false, false,  (int)RmUserFieldIndex.Poolidcm, 0, 0, 11);
			this.AddElementFieldInfo("RmUserEntity", "Poolidcpe", typeof(System.Int32), false, false, false, false,  (int)RmUserFieldIndex.Poolidcpe, 0, 0, 11);
			this.AddElementFieldInfo("RmUserEntity", "Pswactsmsnum", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Pswactsmsnum, 0, 0, 4);
			this.AddElementFieldInfo("RmUserEntity", "Selfreg", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Selfreg, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Srvid", typeof(System.Int32), false, false, false, false,  (int)RmUserFieldIndex.Srvid, 0, 0, 11);
			this.AddElementFieldInfo("RmUserEntity", "State", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.State, 50, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Staticipcm", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Staticipcm, 15, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Staticipcpe", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Staticipcpe, 15, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Taxid", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Taxid, 40, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Uplimit", typeof(System.Int64), false, false, false, false,  (int)RmUserFieldIndex.Uplimit, 0, 0, 20);
			this.AddElementFieldInfo("RmUserEntity", "Uptimelimit", typeof(System.Int64), false, false, false, false,  (int)RmUserFieldIndex.Uptimelimit, 0, 0, 20);
			this.AddElementFieldInfo("RmUserEntity", "Usemacauth", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Usemacauth, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Username", typeof(System.String), true, false, false, false,  (int)RmUserFieldIndex.Username, 64, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Verified", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Verified, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Verifycode", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Verifycode, 10, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Verifyfails", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Verifyfails, 0, 0, 4);
			this.AddElementFieldInfo("RmUserEntity", "Verifymobile", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Verifymobile, 15, 0, 0);
			this.AddElementFieldInfo("RmUserEntity", "Verifysentnum", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Verifysentnum, 0, 0, 4);
			this.AddElementFieldInfo("RmUserEntity", "Warningsent", typeof(System.Int16), false, false, false, false,  (int)RmUserFieldIndex.Warningsent, 0, 0, 1);
			this.AddElementFieldInfo("RmUserEntity", "Zip", typeof(System.String), false, false, false, false,  (int)RmUserFieldIndex.Zip, 8, 0, 0);
		}
		
	}
}




