﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radcheck.</summary>
	public enum RadcheckFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Enabled. </summary>
		Enabled,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Username. </summary>
		Username,
		///<summary>Value. </summary>
		Value,
		///<summary>Virtualdeleted. </summary>
		Virtualdeleted,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radgroupreply.</summary>
	public enum RadgroupreplyFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Groupname. </summary>
		Groupname,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radpostauth.</summary>
	public enum RadpostauthFieldIndex
	{
		///<summary>Authdate. </summary>
		Authdate,
		///<summary>Id. </summary>
		Id,
		///<summary>Nasipaddress. </summary>
		Nasipaddress,
		///<summary>Pass. </summary>
		Pass,
		///<summary>Reply. </summary>
		Reply,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Radreply.</summary>
	public enum RadreplyFieldIndex
	{
		///<summary>Attribute. </summary>
		Attribute,
		///<summary>Id. </summary>
		Id,
		///<summary>Op. </summary>
		Op,
		///<summary>Username. </summary>
		Username,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RmUser.</summary>
	public enum RmUserFieldIndex
	{
		///<summary>Acctype. </summary>
		Acctype,
		///<summary>Actcode. </summary>
		Actcode,
		///<summary>Address. </summary>
		Address,
		///<summary>Alertemail. </summary>
		Alertemail,
		///<summary>Alertsms. </summary>
		Alertsms,
		///<summary>Cardfails. </summary>
		Cardfails,
		///<summary>City. </summary>
		City,
		///<summary>Comblimit. </summary>
		Comblimit,
		///<summary>Comment. </summary>
		Comment,
		///<summary>Company. </summary>
		Company,
		///<summary>Contractid. </summary>
		Contractid,
		///<summary>Contractvalid. </summary>
		Contractvalid,
		///<summary>Country. </summary>
		Country,
		///<summary>Createdby. </summary>
		Createdby,
		///<summary>Createdon. </summary>
		Createdon,
		///<summary>Credits. </summary>
		Credits,
		///<summary>Custattr. </summary>
		Custattr,
		///<summary>Downlimit. </summary>
		Downlimit,
		///<summary>Email. </summary>
		Email,
		///<summary>Enableuser. </summary>
		Enableuser,
		///<summary>Expiration. </summary>
		Expiration,
		///<summary>Firstname. </summary>
		Firstname,
		///<summary>Gpslat. </summary>
		Gpslat,
		///<summary>Gpslong. </summary>
		Gpslong,
		///<summary>Groupid. </summary>
		Groupid,
		///<summary>Ipmodecm. </summary>
		Ipmodecm,
		///<summary>Ipmodecpe. </summary>
		Ipmodecpe,
		///<summary>Lang. </summary>
		Lang,
		///<summary>Lastlogoff. </summary>
		Lastlogoff,
		///<summary>Lastname. </summary>
		Lastname,
		///<summary>Mac. </summary>
		Mac,
		///<summary>Maccm. </summary>
		Maccm,
		///<summary>Mobile. </summary>
		Mobile,
		///<summary>Owner. </summary>
		Owner,
		///<summary>Password. </summary>
		Password,
		///<summary>Phone. </summary>
		Phone,
		///<summary>Poolidcm. </summary>
		Poolidcm,
		///<summary>Poolidcpe. </summary>
		Poolidcpe,
		///<summary>Pswactsmsnum. </summary>
		Pswactsmsnum,
		///<summary>Selfreg. </summary>
		Selfreg,
		///<summary>Srvid. </summary>
		Srvid,
		///<summary>State. </summary>
		State,
		///<summary>Staticipcm. </summary>
		Staticipcm,
		///<summary>Staticipcpe. </summary>
		Staticipcpe,
		///<summary>Taxid. </summary>
		Taxid,
		///<summary>Uplimit. </summary>
		Uplimit,
		///<summary>Uptimelimit. </summary>
		Uptimelimit,
		///<summary>Usemacauth. </summary>
		Usemacauth,
		///<summary>Username. </summary>
		Username,
		///<summary>Verified. </summary>
		Verified,
		///<summary>Verifycode. </summary>
		Verifycode,
		///<summary>Verifyfails. </summary>
		Verifyfails,
		///<summary>Verifymobile. </summary>
		Verifymobile,
		///<summary>Verifysentnum. </summary>
		Verifysentnum,
		///<summary>Warningsent. </summary>
		Warningsent,
		///<summary>Zip. </summary>
		Zip,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Radcheck</summary>
		RadcheckEntity,
		///<summary>Radgroupreply</summary>
		RadgroupreplyEntity,
		///<summary>Radpostauth</summary>
		RadpostauthEntity,
		///<summary>Radreply</summary>
		RadreplyEntity,
		///<summary>RmUser</summary>
		RmUserEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

