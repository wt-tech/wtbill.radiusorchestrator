﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace WTBILL.RadiusOrchestrator.RadiusDB.RadiusManager.Data.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(5);
			InitRadcheckEntityMappings();
			InitRadgroupreplyEntityMappings();
			InitRadpostauthEntityMappings();
			InitRadreplyEntityMappings();
			InitRmUserEntityMappings();
		}

		/// <summary>Inits RadcheckEntity's mappings</summary>
		private void InitRadcheckEntityMappings()
		{
			this.AddElementMapping("RadcheckEntity", @"radius", @"Default", "radcheck", 7, 0);
			this.AddElementFieldMapping("RadcheckEntity", "Attribute", "attribute", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadcheckEntity", "Enabled", "enabled", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("RadcheckEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 2);
			this.AddElementFieldMapping("RadcheckEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadcheckEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RadcheckEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("RadcheckEntity", "Virtualdeleted", "virtualdeleted", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 6);
		}

		/// <summary>Inits RadgroupreplyEntity's mappings</summary>
		private void InitRadgroupreplyEntityMappings()
		{
			this.AddElementMapping("RadgroupreplyEntity", @"radius", @"Default", "radgroupreply", 5, 0);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Attribute", "attribute", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Groupname", "groupname", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 2);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadgroupreplyEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits RadpostauthEntity's mappings</summary>
		private void InitRadpostauthEntityMappings()
		{
			this.AddElementMapping("RadpostauthEntity", @"radius", @"Default", "radpostauth", 6, 0);
			this.AddElementFieldMapping("RadpostauthEntity", "Authdate", "authdate", false, "TimeStamp", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("RadpostauthEntity", "Id", "id", false, "Int", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RadpostauthEntity", "Nasipaddress", "nasipaddress", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RadpostauthEntity", "Pass", "pass", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadpostauthEntity", "Reply", "reply", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RadpostauthEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 5);
		}

		/// <summary>Inits RadreplyEntity's mappings</summary>
		private void InitRadreplyEntityMappings()
		{
			this.AddElementMapping("RadreplyEntity", @"radius", @"Default", "radreply", 5, 0);
			this.AddElementFieldMapping("RadreplyEntity", "Attribute", "attribute", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("RadreplyEntity", "Id", "id", false, "BigInt", 0, 11, 0, true, "LAST_INSERT_ID()", null, typeof(System.Int64), 1);
			this.AddElementFieldMapping("RadreplyEntity", "Op", "op", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RadreplyEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RadreplyEntity", "Value", "value", false, "VarChar", 253, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits RmUserEntity's mappings</summary>
		private void InitRmUserEntityMappings()
		{
			this.AddElementMapping("RmUserEntity", @"radius", @"Default", "rm_users", 56, 0);
			this.AddElementFieldMapping("RmUserEntity", "Acctype", "acctype", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("RmUserEntity", "Actcode", "actcode", false, "VarChar", 60, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RmUserEntity", "Address", "address", false, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RmUserEntity", "Alertemail", "alertemail", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("RmUserEntity", "Alertsms", "alertsms", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 4);
			this.AddElementFieldMapping("RmUserEntity", "Cardfails", "cardfails", false, "SmallInt", 0, 4, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("RmUserEntity", "City", "city", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("RmUserEntity", "Comblimit", "comblimit", false, "BigInt", 0, 20, 0, false, "", null, typeof(System.Int64), 7);
			this.AddElementFieldMapping("RmUserEntity", "Comment", "comment", false, "VarChar", 500, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("RmUserEntity", "Company", "company", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("RmUserEntity", "Contractid", "contractid", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("RmUserEntity", "Contractvalid", "contractvalid", false, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("RmUserEntity", "Country", "country", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RmUserEntity", "Createdby", "createdby", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RmUserEntity", "Createdon", "createdon", false, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("RmUserEntity", "Credits", "credits", false, "Decimal", 0, 20, 2, false, "", null, typeof(System.Decimal), 15);
			this.AddElementFieldMapping("RmUserEntity", "Custattr", "custattr", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("RmUserEntity", "Downlimit", "downlimit", false, "BigInt", 0, 20, 0, false, "", null, typeof(System.Int64), 17);
			this.AddElementFieldMapping("RmUserEntity", "Email", "email", false, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("RmUserEntity", "Enableuser", "enableuser", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 19);
			this.AddElementFieldMapping("RmUserEntity", "Expiration", "expiration", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("RmUserEntity", "Firstname", "firstname", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("RmUserEntity", "Gpslat", "gpslat", false, "Decimal", 0, 17, 14, false, "", null, typeof(System.Decimal), 22);
			this.AddElementFieldMapping("RmUserEntity", "Gpslong", "gpslong", false, "Decimal", 0, 17, 14, false, "", null, typeof(System.Decimal), 23);
			this.AddElementFieldMapping("RmUserEntity", "Groupid", "groupid", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("RmUserEntity", "Ipmodecm", "ipmodecm", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 25);
			this.AddElementFieldMapping("RmUserEntity", "Ipmodecpe", "ipmodecpe", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 26);
			this.AddElementFieldMapping("RmUserEntity", "Lang", "lang", false, "VarChar", 30, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("RmUserEntity", "Lastlogoff", "lastlogoff", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("RmUserEntity", "Lastname", "lastname", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("RmUserEntity", "Mac", "mac", false, "VarChar", 17, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("RmUserEntity", "Maccm", "maccm", false, "VarChar", 17, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("RmUserEntity", "Mobile", "mobile", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("RmUserEntity", "Owner", "owner", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("RmUserEntity", "Password", "password", false, "VarChar", 32, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("RmUserEntity", "Phone", "phone", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("RmUserEntity", "Poolidcm", "poolidcm", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("RmUserEntity", "Poolidcpe", "poolidcpe", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("RmUserEntity", "Pswactsmsnum", "pswactsmsnum", false, "SmallInt", 0, 4, 0, false, "", null, typeof(System.Int16), 38);
			this.AddElementFieldMapping("RmUserEntity", "Selfreg", "selfreg", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 39);
			this.AddElementFieldMapping("RmUserEntity", "Srvid", "srvid", false, "Int", 0, 11, 0, false, "", null, typeof(System.Int32), 40);
			this.AddElementFieldMapping("RmUserEntity", "State", "state", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("RmUserEntity", "Staticipcm", "staticipcm", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("RmUserEntity", "Staticipcpe", "staticipcpe", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("RmUserEntity", "Taxid", "taxid", false, "VarChar", 40, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("RmUserEntity", "Uplimit", "uplimit", false, "BigInt", 0, 20, 0, false, "", null, typeof(System.Int64), 45);
			this.AddElementFieldMapping("RmUserEntity", "Uptimelimit", "uptimelimit", false, "BigInt", 0, 20, 0, false, "", null, typeof(System.Int64), 46);
			this.AddElementFieldMapping("RmUserEntity", "Usemacauth", "usemacauth", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 47);
			this.AddElementFieldMapping("RmUserEntity", "Username", "username", false, "VarChar", 64, 0, 0, false, "", null, typeof(System.String), 48);
			this.AddElementFieldMapping("RmUserEntity", "Verified", "verified", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 49);
			this.AddElementFieldMapping("RmUserEntity", "Verifycode", "verifycode", false, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 50);
			this.AddElementFieldMapping("RmUserEntity", "Verifyfails", "verifyfails", false, "SmallInt", 0, 4, 0, false, "", null, typeof(System.Int16), 51);
			this.AddElementFieldMapping("RmUserEntity", "Verifymobile", "verifymobile", false, "VarChar", 15, 0, 0, false, "", null, typeof(System.String), 52);
			this.AddElementFieldMapping("RmUserEntity", "Verifysentnum", "verifysentnum", false, "SmallInt", 0, 4, 0, false, "", null, typeof(System.Int16), 53);
			this.AddElementFieldMapping("RmUserEntity", "Warningsent", "warningsent", false, "SmallInt", 0, 1, 0, false, "", null, typeof(System.Int16), 54);
			this.AddElementFieldMapping("RmUserEntity", "Zip", "zip", false, "VarChar", 8, 0, 0, false, "", null, typeof(System.String), 55);
		}

	}
}
