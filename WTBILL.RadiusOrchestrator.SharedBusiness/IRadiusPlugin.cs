﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTBILL.RadiusOrchestrator.Data.EntityClasses;

namespace WTBILL.RadiusOrchestrator.SharedBusiness
{
    public interface IRadiusPlugin
    {
        //event EventHandler<UsernameCreatedEventArgs> OnUsernameCreated;
        //event EventHandler<PasswordCreatedEventArgs> OnPasswordCreated;
        RadOperationResult CreateUser(ServizioxcontrattoEntity sxc, int? idNuovoStatoSxc);
        RadOperationResult SuspendOverdueUser(ServizioxcontrattoEntity sxt, int? idNuovoStatoSxc);
        RadOperationResult ReactivateOverdueSuspended(ServizioxcontrattoEntity sxc);

        RadOperationResult SyncAttribute(AttributoradiuEntity attr);

        List<AttributoradiuEntity> GetAttributesToSync();

        List<ServizioxcontrattoEntity> GetUsersToCreate();
        List<ServizioxcontrattoEntity> GetOverdueUsersToSuspend();

        List<ServizioxcontrattoEntity> GetOverdueUsersToReactivate();

        /// <summary>
        /// utenti stagionali e annuali e simili
        /// </summary>
        /// <returns></returns>
        List<ServizioxcontrattoEntity> GetSeasonalUsersToSuspend();
        List<ServizioxcontrattoEntity> GetSeasonalUsersToReactivate();

        RadOperationResult SuspendSeasonalUser(ServizioxcontrattoEntity sxc);
        RadOperationResult ReactivateSeasonalSuspended(ServizioxcontrattoEntity sxc);


        List<ServizioxcontrattoEntity> GetUsersToErase();
        RadOperationResult EraseUser(ServizioxcontrattoEntity sxc);


        List<RadOperationResult> SyncCreateAllUsers();

        string GenerateUsername(ServizioxcontrattoEntity sxc);
        string GeneratePassword(ServizioxcontrattoEntity sxc);
    }
}
