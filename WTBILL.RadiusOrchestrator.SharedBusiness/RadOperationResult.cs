﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTBILL.RadiusOrchestrator.SharedBusiness
{
    public class RadOperationResult
    {
        public bool Success;
        public string Message;
        public List<object> ReturnParams;
    }
}
