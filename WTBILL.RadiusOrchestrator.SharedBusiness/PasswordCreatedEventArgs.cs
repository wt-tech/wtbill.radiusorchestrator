﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTBILL.RadiusOrchestrator.SharedBusiness
{
    public class PasswordCreatedEventArgs
    {
        public string Password { get; set; }
    }
}
