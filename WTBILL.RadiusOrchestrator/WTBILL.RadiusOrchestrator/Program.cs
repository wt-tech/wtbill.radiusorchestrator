﻿using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WTBILL.RadiusOrchestrator.Data.EntityClasses;
using WTBILL.RadiusOrchestrator.SharedBusiness;

namespace WTBILL.RadiusOrchestrator
{
    class Program
    {
       private static ILogger m_Logger;
      
        static void Main(string[] args)
        {
            //const string objectToInstantiate = "SampleProject.Domain.MyNewTestClass, MyTestProject";
           m_Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo.Console()
               .WriteTo.File("radius_orchestrator.log", rollingInterval: RollingInterval.Day)
               .CreateLogger();

            string pluginClassName = ConfigurationManager.AppSettings["RadiusPluginClassName"];
            try
            {
                if (args.Contains("syncAll"))
                {
                    ExecuteSyncCreatesAll(pluginClassName);

                }
                else
                {

                    ExecuteCreation(pluginClassName);
                    ExecuteSuspensions(pluginClassName);
                    ExecuteReactivations(pluginClassName);

                    ExecuteSeasonalSuspensions(pluginClassName);
                    ExecuteSeasonalReactivations(pluginClassName);

                    ExecuteErase(pluginClassName);

                    ExecuteSyncAttributes(pluginClassName);
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error("Errore: {err}  Stacktrace {stack}", ex.Message, ex.StackTrace);
            }

            if (Debugger.IsAttached ||( args.Length > 0 && args[0] == "pause"))
                Console.ReadLine();

        }

        private static void ExecuteErase(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<ServizioxcontrattoEntity> servizi = pg.GetUsersToErase();

            m_Logger.Information("Servizi disdettati da eliminare {nserv}", servizi.Count);

            RadOperationResult res;
            foreach (var sxc in servizi)
            {
                m_Logger.Information("sxc: {sxc} Cliente: {cliente} identificativo: {ident} ", sxc.IdservizioXcontratto, sxc.Contratto.Cliente.Idcliente, sxc.IdentificativoAccesso);
                res = pg.EraseUser(sxc);
                if (res.Success)
                    m_Logger.Information("sxc {sxc} risultato eliminazione OK {msg}", sxc.IdservizioXcontratto, res.Message);
                else
                    m_Logger.Error("sxc {sxc} risultato KO messaggio: {msg}", sxc.IdservizioXcontratto, res.Message);

            }

        }
        private static void ExecuteSeasonalReactivations(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<ServizioxcontrattoEntity> servizi = pg.GetSeasonalUsersToReactivate();

            m_Logger.Information("Servizi stagionali da riattivare {nserv}", servizi.Count);

            RadOperationResult res;
            foreach (var sxc in servizi)
            {
                m_Logger.Information("sxc: {sxc} Cliente: {cliente} identificativo: {ident} ", sxc.IdservizioXcontratto, sxc.Contratto.Cliente.Idcliente, sxc.IdentificativoAccesso);
                res = pg.ReactivateSeasonalSuspended(sxc);
                if (res.Success)
                    m_Logger.Information("sxc {sxc} risultato riattivazione OK {msg}", sxc.IdservizioXcontratto, res.Message);
                else
                    m_Logger.Error("sxc {sxc} risultato KO messaggio: {msg}", sxc.IdservizioXcontratto, res.Message);

            }

        }

        private static void ExecuteSeasonalSuspensions(string pluginClassName)
        {

            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<ServizioxcontrattoEntity> servizi = pg.GetSeasonalUsersToSuspend();

            m_Logger.Information("Servizi stagionali da sospendere {nserv}", servizi.Count);

            RadOperationResult res;
            foreach (var sxc in servizi)
            {
                m_Logger.Information("sxc: {sxc} Cliente: {cliente} identificativo: {ident}", sxc.IdservizioXcontratto, sxc.Contratto.Cliente.Idcliente, sxc.IdentificativoAccesso);
                res = pg.SuspendSeasonalUser(sxc);
                if (res.Success)
                    m_Logger.Information("sxc {sxc} risultato sospensione OK {msg}", sxc.IdservizioXcontratto, res.Message);
                else
                    m_Logger.Error("sxc {sxc} risultato KO messaggio: {msg}", sxc.IdservizioXcontratto, res.Message);

            }

        }


        private static void ExecuteSyncCreatesAll(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            
            m_Logger.Information("Inizio creazione utenti");

            RadOperationResult res;
            var results = pg.SyncCreateAllUsers();


            foreach (var r in results)
            {
                if (r.Success)
                    m_Logger.Information(r.Message);
                else
                    m_Logger.Error(r.Message);
                
            }

        }


        private static void ExecuteSyncAttributes(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<AttributoradiuEntity> attrs = pg.GetAttributesToSync();

            m_Logger.Information("Attributi da sincronizzare {nattr}", attrs.Count);

            RadOperationResult res;
            foreach (var a in attrs)
            {
                m_Logger.Information("Sxc: {sxc} Id-Attr: {id} attr: {attr} user: {user}", a.IdservizioXcontratto, a.IdattributoRadius, a.NomeAttributo, a.Servizioxcontratto.IdentificativoAccesso);
                res = pg.SyncAttribute(a);
                if (res.Success)
                    m_Logger.Information("Risultato operazione OK {msg}", res.Message);
                else
                    m_Logger.Error("Attr: {attr} risultato KO messaggio: {msg}", res.Message);

            }

        }

        private static void ExecuteCreation(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<ServizioxcontrattoEntity> servizi = pg.GetUsersToCreate();

            m_Logger.Information("Servizi da creare {nserv}", servizi.Count);

            RadOperationResult res;
            foreach (var sxc in servizi)
            {
                m_Logger.Information("sxc: {sxc} Cliente: {cliente}", sxc.IdservizioXcontratto, sxc.Contratto.Cliente.Idcliente);
                res = pg.CreateUser(sxc, 8);
                if (res.Success)
                    m_Logger.Information("sxc {sxc} risultato creazione OK {msg}", sxc.IdservizioXcontratto, res.Message);
                else
                    m_Logger.Error("sxc {sxc} risultato KO messaggio: {msg}", sxc.IdservizioXcontratto, res.Message);

            }

        }
        private static void ExecuteReactivations(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<ServizioxcontrattoEntity> servizi = pg.GetOverdueUsersToReactivate();

            m_Logger.Information("Servizi da riattivare {nserv}", servizi.Count);

            RadOperationResult res;
            foreach (var sxc in servizi)
            {
                m_Logger.Information("sxc: {sxc} Cliente: {cliente} identificativo: {ident} ", sxc.IdservizioXcontratto, sxc.Contratto.Cliente.Idcliente, sxc.IdentificativoAccesso);
                res = pg.ReactivateOverdueSuspended(sxc);
                if (res.Success)
                    m_Logger.Information("sxc {sxc} risultato riattivazione OK {msg}", sxc.IdservizioXcontratto, res.Message);
                else
                    m_Logger.Error("sxc {sxc} risultato KO messaggio: {msg}", sxc.IdservizioXcontratto, res.Message);

            }

        }

        private static void ExecuteSuspensions(string pluginClassName)
        {
            var objectType = Type.GetType(pluginClassName);
            IRadiusPlugin pg = (IRadiusPlugin)Activator.CreateInstance(objectType);
            List<ServizioxcontrattoEntity> servizi = pg.GetOverdueUsersToSuspend();
            
            m_Logger.Information("Servizi da sospendere {nserv}", servizi.Count);

            RadOperationResult res;
            foreach (var sxc in servizi)
            {
                m_Logger.Information("sxc: {sxc} Cliente: {cliente} identificativo: {ident}", sxc.IdservizioXcontratto, sxc.Contratto.Cliente.Idcliente, sxc.IdentificativoAccesso);
                res = pg.SuspendOverdueUser(sxc, null);
                if(res.Success)
                    m_Logger.Information("sxc {sxc} risultato sospensione OK {msg}", sxc.IdservizioXcontratto, res.Message);
                else
                    m_Logger.Error("sxc {sxc} risultato KO messaggio: {msg}",sxc.IdservizioXcontratto, res.Message);

            }
        }
    }
}
